import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import {JwtHelper} from "angular2-jwt";
import { Storage } from '@ionic/storage';
/*
  Generated class for the DoctorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DoctorProvider {
  jwtHelper = new JwtHelper();
  public base_url: any;
  data_url: any;

  constructor(public http: Http, private cache: CacheService, public storage: Storage, private siteProvider: SiteProvider) {
    console.log('Hello DoctorProvider Provider');
    this.storage.get("sess_user_token").then((token) => {
           // Decode the Token
           let decodedToken = this.jwtHelper.decodeToken(token);
           this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
           this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
        });
  }

  getSpecializations(isLoadNew, thisData) {
  		return new Promise(resolve => {
            this.siteProvider.getAccessTokenForPost().then(data => {

                 let url = this.data_url+"/search/doctorspecialties?q="+ thisData.searched_keyword;
                 url += "&page="+ thisData.current_page;
                 url += "&token="+ data['token'];
                  let version = "&v="+ this.siteProvider.getVersion();

                 let groupKey = "getSpecializations";
                 let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                 let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

                 response.subscribe((data) => {
                      resolve(data);
                }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
              });
        });
  }

  editDoctorSpecialty(formData) {
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.data_url+'/user/updatespecialty';

            let headers = new Headers();
               headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});

            this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
                resolve(data1);
            }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
        });
    });
  }

  getAllDoctors(isLoadNew, thisData){
      return new Promise(resolve => {
          this.siteProvider.getAccessTokenForPost().then(data => {

               let url = this.data_url+"/search/result?action=doctors&q="+ thisData.searched_keyword;
               if(thisData.applied_filters && thisData.applied_filters.city) {
                  url += "&city="+ thisData.applied_filters.city;
               }
               if(thisData.hospital) {
               url += "&hospital="+ thisData.hospital;
              }
               url += "&page="+ thisData.current_page;
               url += "&order=firstname&orderdirection=ASC";
               url += "&token="+ data['token'];
               let version = "&v="+ this.siteProvider.getVersion();

               let groupKey = "getAllDoctors";
               let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
               let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

               response.subscribe((data) => {
                    resolve(data);
               }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
            });
      });
   }

  getSchedules(isLoadNew, thisData) {
      return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {

               let url = this.data_url+"/user/doctortimeslot?";
               url += "id="+thisData.doc_id;
               url += "&hospital="+ thisData.hospital_id;
               url += "&day="+ thisData.selected_day;
               url += "&token="+ data['token'];
               this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                    resolve(data);
               }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, messagmessagee: this.siteProvider.generic_error_msg });
               });
            });
      });
  }

  setDoctorsSchedule(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
          //console.log(data['token']);
              let url = this.data_url+'/user/doctortimeslotsave';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });

        });
    });
  }

  addDoctorsHospital(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
          //console.log(data['token']);
              let url = this.data_url+'/user/doctorhospitalsave';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });

        });
    });
  }

  getAllFilteredDoctors(isLoadNew, thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {

             let url = this.data_url+"/search/result?action=doctors&specialty="+ thisData.filterData;
             url += "&page="+ thisData.current_page;
             url += "&token="+ data['token'];
             this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                  resolve(data);
             }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
          });
    });
  }

 getAllFilteredCityDoctors(isLoadNew, thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {

             let url = this.data_url+"/search/result?action=doctors&city="+ thisData.filterData;
             url += "&page="+ thisData.current_page;
             url += "&token="+ data['token'];
             this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                  resolve(data);
             }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
          });
    });
  }

  getDoctorsHospitalList(){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {
             let url = this.data_url+"/user/hospital";
             url += "?token="+ data['token'];
             this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                  resolve(data);
             }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
          });
    });
  }

  getDoctorsSchedule(){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {

             let url = this.data_url+"/user/schedule?";
             url += "token="+ data['token'];
             this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                  resolve(data);
             }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
          });
    });
  }

  verifyPatientTransaction(thisData){
    return new Promise(resolve => {
      console.log(thisData);
        this.siteProvider.getAccessToken().then(data => {

           let url = this.data_url+"/appointment/verification?";
           url += "id="+ thisData.code;
           url += "&token="+ data['token'];
           this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                resolve(data);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }

  removeDoctorsHospital(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
          //console.log(data['token']);
              let url = this.data_url+'/user/doctorhospitalremove';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });

        });
    });
  }

  deleteScheduleTimeSlot(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
          //console.log(data['token']);
              let url = this.data_url+'/user/doctortimeslotdelete';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });

        });
    });
  }

  setDoctorHospitalMain(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.data_url+'/hospital/setmainhospital';
            let formData = thisData;

            let headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
            });
        });
    });
  }

  getServiceType(){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {
            let url = this.data_url+"/servicetype"
            url += "?token="+ data['token'];
            let version = "&v="+ this.siteProvider.getVersion();

           let groupKey = "getServiceType";
           let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
           let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
           response.subscribe((data) => {
                resolve(data);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }

  getServices(isLoadNew, thisData) {
      return new Promise(resolve => {
            this.siteProvider.getAccessTokenForPost().then(data => {

                 let url = this.data_url+"/search/doctorservices?q="+ thisData.searched_keyword;
                 url += "&page="+ thisData.current_page;
                 url += "&service_type="+ thisData.service_type_id;
                 url += "&hospital_id="+ thisData.hospital_id;
                 url += "&token="+ data['token'];

                  let version = "&v="+ this.siteProvider.getVersion();

                 let groupKey = "getServices";
                 let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                 let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

                 response.subscribe((data) => {
                      resolve(data);
                }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
              });
        });
  }

  getUserServices(thisData) {
      return new Promise(resolve => {
            this.siteProvider.getAccessTokenForPost().then(data => {

                let url = this.data_url+"/user/doctorservices";
                url += "?token="+ data['token'];
                url += "&hospital_id="+ thisData.hospital_id;
                url += "&doctor_id="+ thisData.doctor_id;
                if(thisData.service_type_id){
                  url += "&service_type_id="+ thisData.service_type_id;
                }

                let version = "&v="+ this.siteProvider.getVersion();

                let groupKey = "getUserServices";
                let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

                request.subscribe((data) => {
                    resolve(data);
                }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                });
              });
      });
  }

  editDoctorServices(formData) {
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.data_url+'/user/updateservices';

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});

            this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
                resolve(data1);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }
}

import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import {JwtHelper} from "angular2-jwt";
import { Storage } from '@ionic/storage';

@Injectable()
export class ArticleProvider {
      jwtHelper = new JwtHelper();
      public base_url: any;
      data_url: any;
      accessToken = "";

      constructor(public http: Http, private cache: CacheService, public storage: Storage, private siteProvider: SiteProvider) {
         this.storage.get("sess_user_token").then((token) => {
           // Decode the Token
           let decodedToken = this.jwtHelper.decodeToken(token);
           this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
           this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
        });
      }

      getFeaturedArticles(isLoadNew){
        return new Promise(resolve => {
            this.siteProvider.getAccessTokenForPost().then(data => {
                this.accessToken = data['token'];
                 let url = this.data_url+"/article/?page=1";
                 url += "&token="+ data['token'];
                 let version = "&v="+ this.siteProvider.getVersion();

                 let groupKey = "getFeaturedArticles";
                 let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                 let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

                 response.subscribe((data) => {
                      resolve(data);
                 }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
              });
        });
     }

     getAllArticles(isLoadNew, thisData){
        return new Promise(resolve => {
            this.siteProvider.getAccessTokenForPost().then(data => {
                this.accessToken = data['token'];
                 let url = this.data_url+"/search/result?action=articles&q="+ thisData.searched_keyword;

                 if(thisData.featured) {
                    url += "&featured="+thisData.featured;
                 }

                 url += "&page="+ thisData.current_page;
                 url += "&token="+ data['token'];
                 let version = "&v="+ this.siteProvider.getVersion();

                 let groupKey = "getAllArticles";
                 let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                 let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
                 response.subscribe((data) => {
                    resolve(data);
                 }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
              });
        });
     }

     /* FROM ARJOY ==================================*/
     getAllRelatedArticles(isLoadNew, thisData){
        return new Promise(resolve => {
            this.siteProvider.getAccessTokenForPost().then(data => {
                this.accessToken = data['token'];
                 let url = this.data_url+"/article/category?category="+ thisData.related_article;
                 url += "&page="+ thisData.current_page;
                 url += "&token="+ data['token'];
                 let version = "&v="+ this.siteProvider.getVersion();
                 let groupKey = "getAllRelatedArticles";

                 let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                 let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

                 response.subscribe((data) => {
                      resolve(data);
                 }, err => {resolve({ error: 1, message: this.siteProvider.generic_error_msg }); });
              });
        });
     }
}

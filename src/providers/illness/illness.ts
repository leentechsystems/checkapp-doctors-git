import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { SiteProvider } from '../site/site';
import { CacheService } from 'ionic-cache';
import { Http, Headers, RequestOptions } from '@angular/http';
import {JwtHelper} from "angular2-jwt";
import { Storage } from '@ionic/storage';
/*
  Generated class for the IllnessProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IllnessProvider {
  jwtHelper = new JwtHelper();
  public base_url: any;
  data_url: any;
  accessToken:any = "";

  constructor(public http: Http, public storage: Storage, private cache: CacheService, private siteProvider: SiteProvider) {
    console.log('Hello IllnessProvider Provider');
    this.storage.get("sess_user_token").then((token) => {
         // Decode the Token
         let decodedToken = this.jwtHelper.decodeToken(token);
         this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
         this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
      });
  }

  getAllIllnes(){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.data_url+"/illness?token="+ data['token'];
            let version = "&v="+ this.siteProvider.getVersion();
             let groupKey = "getAllIllnes";

             let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
             let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
             response.subscribe((data) => {
                resolve(data);
            }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
        });
    });
  }
}

import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';
import {JwtHelper} from "angular2-jwt";

/*
  Generated class for the AmbulanceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AmbulanceProvider {
 jwtHelper = new JwtHelper();
  public base_url: any;
  data_url: any;
  accessToken = "";

  constructor(public http: Http, private cache: CacheService, private siteProvider: SiteProvider, public storage: Storage) {
    console.log('Hello AmbulanceProvider Provider');
    this.storage.get("sess_user_token").then((token) => {
         // Decode the Token
         let decodedToken = this.jwtHelper.decodeToken(token);
         this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
         this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
      });
  }

  getAllAmbulance(isLoadNew, thisData){
        return new Promise(resolve => {
            this.siteProvider.getAccessTokenForPost().then(data => {
                 let url = this.data_url+"/search/result?action=ambulance&q="+ thisData.searched_keyword;
                  if(thisData.applied_filters && thisData.applied_filters.city) {
                    url += "&city="+ thisData.applied_filters.city;
                 }
                 url += "&page="+ thisData.current_page;
                  url += "&order=name&orderdirection=ASC";
                 url += "&token="+ data['token'];
                 let version = "&v="+ this.siteProvider.getVersion();

                 let groupKey = "getAllAmbulance";
                 let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                 let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

                 response.subscribe((data) => {
                      resolve(data);
                 }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
              });
        });
     }

}

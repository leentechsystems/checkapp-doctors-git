import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import {JwtHelper} from "angular2-jwt";
import { Storage } from '@ionic/storage';

/*
  Generated class for the HospitalsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HospitalsProvider {

  jwtHelper = new JwtHelper();
  public base_url: any;
  data_url: any;

  constructor(public http: Http, private cache: CacheService, private siteProvider: SiteProvider, public storage: Storage) {
    console.log('Hello HospitalsProvider Provider');
      this.storage.get("sess_user_token").then((token) => {
        // Decode the Token
        let decodedToken = this.jwtHelper.decodeToken(token);
        this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
        this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
        console.log(this.data_url, this.base_url);
    });
  }

  getAllHospitalDoctors(isLoadNew, thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {
            let url = this.data_url+"/hospital/doctors?id="+ thisData.id;
            url += "&token="+ data['token'];
            let version = "&v="+ this.siteProvider.getVersion();

           let groupKey = "getAllHospitalDoctors"+thisData.id;
           let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
           let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
           response.subscribe((data) => {
                resolve(data);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }

  getServices(thisHospital){
    return new Promise(resolve => {
      this.siteProvider.getAccessTokenForPost().then(data => {
           let url = this.data_url+"/hospital/service?id="+ thisHospital;
           url += "&token="+ data['token'];
           let version = "&v="+ this.siteProvider.getVersion();

           let groupKey = "getServices";
           let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
           let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
           response.subscribe((data) => {
                resolve(data);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
      });
    });
  }

  getAllHospitals(isLoadNew, thisData){
    return new Promise(resolve => {
      this.siteProvider.getAccessTokenForPost().then(data => {

           let url = this.data_url+"/search/result?action=hospitals&q="+ thisData.searched_keyword;
           if(thisData.applied_filters && thisData.applied_filters.city) {
              url += "&city="+ thisData.applied_filters.city;
           }
           url += "&page="+ thisData.current_page;
           url += "&order=name&orderdirection=ASC";
           url += "&token="+ data['token'];
          let version = "&v="+ this.siteProvider.getVersion();
           let groupKey = "getAllHospitals";

           let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
           let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
           response.subscribe((data) => {
                resolve(data);
           }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
      });
    });
  }

  getAllUnselectHospitals(isLoadNew, thisData){
    return new Promise(resolve => {
      this.siteProvider.getAccessTokenForPost().then(data => {

           let url = this.data_url+"/search/result?action=hospitals&q="+ thisData.searched_keyword;
           if(thisData.applied_filters && thisData.applied_filters.city) {
              url += "&city="+ thisData.applied_filters.city;
           }
           url += "&page="+ thisData.current_page;
           url += "&order=name&orderdirection=ASC";
           url += "&token="+ data['token'];
           url += "&hospital="+ 'unselect';
           let version = "&v="+ this.siteProvider.getVersion();
           let groupKey = "getAllHospitals";

           let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
           let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
           response.subscribe((data) => {
                resolve(data);
           }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
      });
    });
  }

  getSpecificHospital(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {
            let url = this.data_url+"/hospital?id="+ thisData;
            url += "&token="+ data['token'];
            let version = "&v="+ this.siteProvider.getVersion();

           let groupKey = "getSpecificHospital"+thisData;
           let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
           let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
           response.subscribe((data) => {
                resolve(data);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }

  getHospitalServices(){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {
             //let url = this.data_url+"/hospital/service?id="+ thisData;
             //for Phase II
             let url = this.data_url+"/services"
             url += "?token="+ data['token'];
             this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                  resolve(data);
             }, err => {resolve({ error: 1, message: this.siteProvider.generic_error_msg }); });
          });
    });
  }

  getServiceBySeriveType(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {
            let url = this.data_url+"/hospital/servicebytype"
            url += "?token="+ data['token'];
            url += "&id="+ thisData['hospital_id'];
            url += "&service_type_id="+ thisData['service_type_id'];
            let version = "&v="+ this.siteProvider.getVersion();

           let groupKey = "getServiceBySeriveType";
           let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
           let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');
           response.subscribe((data) => {
                resolve(data);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }
}

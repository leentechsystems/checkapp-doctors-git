import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import {JwtHelper} from "angular2-jwt";
import { Storage } from '@ionic/storage';
/*
  Generated class for the UserProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UserProvider {
  jwtHelper = new JwtHelper();
  public base_url: any;
  data_url: any;

  constructor(public http: Http, private cache: CacheService, public storage: Storage, public siteProvider: SiteProvider) {
     this.storage.get("sess_user_token").then((token) => {
        if(token) {
         // Decode the Token
         console.log(token);
         let decodedToken = this.jwtHelper.decodeToken(token);
         this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
         this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
        }
    });
  }


  /* From AUBREY ================== */
  loginUser(thisData, data) {
	  return new Promise(resolve => {

      this.siteProvider.getGuestAccessToken().then(data1 => {
        let url = this.siteProvider.data_url+'/user/login';
        let formData = thisData;
        let cacheKey = "userLogin";

      	let headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        headers.append('Accept', 'application/json');
        headers.append('Authorization', 'Bearer '+data.token);

      	let options = new RequestOptions({headers: headers});
        let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json());
        request.subscribe(data1 => {
          this.cache.saveItem(cacheKey, data1);
        	resolve(data1);
        }, err => {
              //this.siteProvider.showToast(this.siteProvider.generic_error_msg);
          resolve({ error: 1, message: this.siteProvider.generic_error_msg });
        });
      });
    });
  }

  requestValidationCode(formData) {
    return new Promise(resolve => {
        this.siteProvider.getGuestAccessToken().then(data => {
           // console.log(data);
            let url = this.siteProvider.data_url+'/user/forgotpassword';

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);

            let options = new RequestOptions({headers: headers});
            this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
                resolve(data1);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg, false);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
            });
        });
    });
  }

  validateValidationCode(formData) {
    return new Promise(resolve => {
        let url = this.siteProvider.data_url+'/user/forgotpasswordcode';

        let headers = new Headers();
          headers.append('Content-Type', 'application/x-www-form-urlencoded');
          headers.append('Accept', 'application/json');
          headers.append('Authorization', 'Bearer '+ formData['token']);

        let options = new RequestOptions({headers: headers});
        this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
            resolve(data1);
        }, err => {
            this.siteProvider.showToast(this.siteProvider.generic_error_msg, false);
            resolve({ error: 1, message: this.siteProvider.generic_error_msg });
       });
    });
  }

  resetPasswordWithCode(formData) {
    return new Promise(resolve => {
        let url = this.siteProvider.data_url+'/user/forgotpasswordchange';

        let headers = new Headers();
          headers.append('Content-Type', 'application/x-www-form-urlencoded');
          headers.append('Accept', 'application/json');
          headers.append('Authorization', 'Bearer '+ formData['token']);

        let options = new RequestOptions({headers: headers});
        this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
            resolve(data1);
          }, err => {
          this.siteProvider.showToast(this.siteProvider.generic_error_msg);
          resolve({ error: 1, message: this.siteProvider.generic_error_msg });
       });
    });
  }

  resetPassword(formData) {
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.siteProvider.data_url+'/user/passwordchange';

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});

            this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
                resolve(data1);
             }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
        });
    });
  }

  editProfile(formData) {
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.siteProvider.data_url+'/user/update';

            let headers = new Headers();
              // headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});

            this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
                resolve(data1);
             }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
        });
    });
  }
  /* ================== End */


  /* From ARJOY ================== */
  checkMail(thisData, data) {
	  if(data.error !==0){
	      return new Promise(resolve => {
	          let url = this.siteProvider.data_url+'/user/mailavailability';
	          let formData = thisData;

	          let headers = new Headers();
	            headers.append('Content-Type', 'application/x-www-form-urlencoded');
	            headers.append('Accept', 'application/json');
	            headers.append('Authorization', 'Bearer '+data['token']);

	          let options = new RequestOptions({headers: headers});

	          this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
	              resolve(data1);
	           }, err => {
                  //this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
	      });
	    }
  }

  signup(regData, data){
    if(data.error !==0){
    return new Promise(resolve => {
          let url = this.siteProvider.data_url+'/user/signup';
          let formData = regData;

          let headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer '+data['token']);

          let options = new RequestOptions({headers: headers});

          this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe(data1 => {
              resolve(data1);
          }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
      });
    }
  }
  /* ================== End */


  /*From Rhaldnie*/
  getPatientProfile(patient_id){
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {

                 let url = this.siteProvider.data_url+"/user/get?id="+patient_id;
                 url += "&token="+ data['token'];
                 this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                    resolve(data);
                  }, err => {
                      this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                      resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                   });
              });
    });
  }

  getPreviousPatient(isLoadNew, thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {

                 let url = this.siteProvider.data_url+"/search/result?action="+thisData['action'];
                  url += "&token="+ data['token'];
                  url += "&page="+ thisData['current_page'];
                 if(thisData['applied_filters']){
                   //url += "&gender="+ thisData['applied_filters']['gender'];
                   url += "&range="+ thisData['applied_filters']['range'];
                 }else{
                   url += "&range=previous";
                 }

                 if(thisData['searched_keyword']){
                   url += "&q="+ thisData['searched_keyword'];
                 }
                 let version = "&v="+ this.siteProvider.getVersion();

                 let groupKey = "getPreviousPatient";
                 let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
                 let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

                 response.subscribe((data) => {
                    resolve(data);
                 }, err => {
                      this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                      resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                   });
              });
    });
  }
  /* ================== End */

  getMedicalReport(isLoadNew, thisData) {
    return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {
               let url = this.siteProvider.data_url+"/medical";
               url += "?type="+ thisData.type;
               url += "&page="+ thisData.current_page;
               url += "&token="+ data['token'];
               url += "&userid="+ thisData.user_id;
               if(thisData.transactionId){
                url += "&transactionId="+ thisData.transactionId;
               }

               let version = "&v="+ this.siteProvider.getVersion();

               let groupKey = "getMedicalReport";
               let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
               let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

               response.subscribe((data) => {
                    resolve(data);
               }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
            });
      });
  }
  /* From rhan*/
  getMedicalRecommendation(medical_report_id) {
    return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {
               let url = this.siteProvider.data_url+"/medical/recommendation";
               url += "?id="+ medical_report_id;
               url += "&token="+ data['token'];
               // url += "&userid=2";
               this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                    resolve(data);
               }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
            });
      });
  }

  getNotification(isLoadNew, thisData){
    return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {
               let url = this.siteProvider.data_url+"/notification";
               url += "?page="+ thisData.current_page;
               url += "&token="+ data['token'];
               if(thisData['action']){
                  url += "&action="+ thisData['action'];
               }
               let version = "&v="+ this.siteProvider.getVersion();

               let groupKey = "getNotification";
               let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
               let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

               // url += "&userid=2";
               response.subscribe((data) => {
                    resolve(data);
               }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
            });
      });
  }

  changeNotificationStatus(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {

            let url = this.siteProvider.data_url+'/notification/changestatus';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe((data1) => { resolve(data1); }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
        });
    });
  }

  sendSupportMessage(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {

            let url = this.siteProvider.data_url+'/sendmessage';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe((data1) => { resolve(data1); }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
        });
    });
  }

  saveBookmarks(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
          //console.log(data['token']);
              let url = this.siteProvider.data_url+'/user/userbookmarksave';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });

        });
    });
  }

  getBookmarks(isLoadNew, thisData){
    return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {
               let url = this.siteProvider.data_url+"/bookmarks";
               url += "?page="+ thisData.current_page;
               url += "&action="+ thisData.action;
               url += "&token="+ data['token'];
               let version = "&v="+ this.siteProvider.getVersion();

               let groupKey = "getBookmarks";
               let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
               let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

               // url += "&userid=2";
               response.subscribe((data) => {
                    resolve(data);
               }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
            });
      });
  }

  removeDeviceToken(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.siteProvider.data_url+'/user/removedevicetoken';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }

  /* ================== End */

  contactInformation(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
            let url = this.siteProvider.data_url+'/user/contact';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(data1); }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }
}

import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import { JwtHelper } from "angular2-jwt";
import { Storage } from '@ionic/storage';
/*
  Generated class for the AppointmentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AppointmentProvider = /** @class */ (function () {
    function AppointmentProvider(http, cache, storage, siteProvider) {
        var _this = this;
        this.http = http;
        this.cache = cache;
        this.storage = storage;
        this.siteProvider = siteProvider;
        this.jwtHelper = new JwtHelper();
        this.accessToken = "";
        console.log('Hello AppointmentProvider Provider');
        this.storage.get("sess_user_token").then(function (token) {
            // Decode the Token
            var decodedToken = _this.jwtHelper.decodeToken(token);
            _this.base_url = "http://" + decodedToken['iss'];
            _this.data_url = "http://" + decodedToken['iss'] + "/api";
            console.log(_this.data_url, _this.base_url);
        });
    }
    AppointmentProvider.prototype.setAppointment = function (thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/appointment';
                var formData = thisData;
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                var request = _this.http.post(url, formData, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); })
                    .subscribe(function (data1) { resolve(request); console.log("data", data1); }, function (err) { resolve({ error: 1, message: _this.siteProvider.generic_error_msg }); });
            });
        });
    };
    AppointmentProvider.prototype.getDoctorsAppointments = function (isLoadNew, thisData) {
        var _this = this;
        console.log(thisData);
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                _this.accessToken = data['token'];
                var url = _this.data_url + "/hmo/appointment?action=" + thisData.day;
                if (thisData.current_page) {
                    url += "&page=" + thisData.current_page;
                }
                else {
                    url += "&pagination=false";
                }
                url += "&token=" + data['token'];
                if (thisData.date) {
                    url += "&date=" + thisData.date;
                }
                if (thisData.category != undefined) {
                    url += "&category=" + thisData.category;
                }
                _this.http.get(url).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    AppointmentProvider.prototype.approveAppointment = function (thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/appointment/approve';
                var formData = thisData;
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                var request = _this.http.post(url, formData, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); })
                    .subscribe(function (data1) {
                    resolve(data1);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    AppointmentProvider.prototype.getAppointmentDetails = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                _this.accessToken = data['token'];
                var url = _this.data_url + "/appointment/details?id=" + id;
                url += "&token=" + data['token'];
                console.log(url);
                _this.http.get(url).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    AppointmentProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AppointmentProvider.ctorParameters = function () { return [
        { type: Http, },
        { type: CacheService, },
        { type: Storage, },
        { type: SiteProvider, },
    ]; };
    return AppointmentProvider;
}());
export { AppointmentProvider };
//# sourceMappingURL=appointment.js.map
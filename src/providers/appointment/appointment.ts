import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import {JwtHelper} from "angular2-jwt";
import { Storage } from '@ionic/storage';

/*
  Generated class for the AppointmentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AppointmentProvider {
  jwtHelper = new JwtHelper();
  public base_url: any;
  data_url: any;
  accessToken = "";

  constructor(public http: Http, private cache: CacheService, public storage: Storage, private siteProvider: SiteProvider) {
    console.log('Hello AppointmentProvider Provider');
     this.storage.get("sess_user_token").then((token) => {
         // Decode the Token
          let decodedToken = this.jwtHelper.decodeToken(token);
          this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
          this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
          console.log(this.data_url, this.base_url);
      });
  }

  setAppointment(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
              let url = this.data_url+'/appointment';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe(data1 => { resolve(request); console.log("data", data1); }, err => {resolve({ error: 1, message: this.siteProvider.generic_error_msg }); });

        });
    });
  }

  getDoctorsAppointments(isLoadNew, thisData){
    console.log(thisData);
    return new Promise(resolve => {
        this.siteProvider.getAccessToken().then(data => {
            this.accessToken = data['token'];

                 let url = this.data_url+"/hmo/appointment?action="+thisData.day;

                  if(thisData.current_page){
                      url += "&page="+ thisData.current_page;
                  } else {
                      url += "&pagination=false";
                  }

                 url += "&token="+ data['token'];

                 if(thisData.date){
                    url += "&date="+ thisData.date;
                 }

                 if(thisData.category != undefined){
                    url += "&category="+ thisData.category;
                 }

                 this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                    resolve(data);
                 }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
              });
    });
  }

  approveAppointment(thisData){
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {

            let url = this.data_url+'/appointment/approve';
            let formData = thisData;

            let headers = new Headers();
              headers.append('Content-Type', 'application/x-www-form-urlencoded');
              headers.append('Accept', 'application/json');
              headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});
            let request = this.http.post(url, formData, options).timeout(this.siteProvider.timeout).map(res => res.json())
            .subscribe((data1) => { resolve(data1);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
        });
    });
  }
  getAppointmentDetails(id) {
    return new Promise(resolve => {
      this.siteProvider.getAccessToken().then(data => {
        this.accessToken = data['token'];

            let url = this.data_url+"/appointment/details?id="+id;
            url += "&token="+ data['token'];
            console.log(url)
            this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
              resolve(data);
            }, err => {
                this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                resolve({ error: 1, message: this.siteProvider.generic_error_msg });
             });
        });
    });
  }
}

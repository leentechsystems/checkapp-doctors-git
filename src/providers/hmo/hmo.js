import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import { JwtHelper } from "angular2-jwt";
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';
import 'rxjs/add/observable/forkJoin';
import { FileTransfer } from '@ionic-native/file-transfer';
/*
  Generated class for the HmoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var HmoProvider = /** @class */ (function () {
    function HmoProvider(http, cache, siteProvider, storage, transfer) {
        var _this = this;
        this.http = http;
        this.cache = cache;
        this.siteProvider = siteProvider;
        this.storage = storage;
        this.transfer = transfer;
        this.jwtHelper = new JwtHelper();
        this.accessToken = "";
        console.log('Hello HmoProvider Provider');
        this.storage.get("sess_user_token").then(function (token) {
            // Decode the Token
            var decodedToken = _this.jwtHelper.decodeToken(token);
            _this.base_url = "http://" + decodedToken['iss'];
            _this.data_url = "http://" + decodedToken['iss'] + "/api";
        });
    }
    HmoProvider.prototype.getUserHmoAccount = function (loadNew, thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/hmo';
                var thisdata = JSON.stringify({
                    "userId": parseInt(thisData.id)
                });
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                _this.http.post(url, thisdata, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.applyNewAccount = function (thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/hmo/newaccount';
                var thisdata = JSON.stringify(thisData);
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                _this.http.post(url, thisdata, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.addReport = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var diagnosis, findings, hmoreport, transactionID, thisData;
            Observable.forkJoin([
                _this.storage.get('diagnosis'),
                _this.storage.get('findings'),
                _this.storage.get('hmoreport'),
                _this.storage.get('transaction_id'),
            ]).subscribe(function (res) {
                diagnosis = res[0];
                findings = res[1];
                hmoreport = res[2];
                transactionID = res[3];
                thisData = {
                    'medical_report_hmo_transactions_id': transactionID,
                    'diagnosis_illness_id': diagnosis.diagnosis,
                    'diagnosis_content': diagnosis.remarks,
                    'prescriptions_title': findings.findings,
                    'prescriptions_content': findings.remarks,
                    'hmo_report_title': hmoreport.findings,
                    'hmo_report_content': hmoreport.remarks,
                    'recommendation': hmoreport.recommendations
                };
                console.log('diagnosis', diagnosis);
                console.log('findings', findings);
                console.log('hmoreport', hmoreport);
                console.log(thisData);
                _this.siteProvider.getAccessTokenForPost().then(function (data) {
                    var url = _this.data_url + '/appointment/complete';
                    var thisdata = JSON.stringify(thisData);
                    var headers = new Headers();
                    headers.append('Content-Type', 'application/x-www-form-urlencoded');
                    headers.append('Accept', 'application/json');
                    headers.append('Authorization', 'Bearer ' + data['token']);
                    var options = new RequestOptions({ headers: headers });
                    _this.http.post(url, thisdata, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                        resolve(data);
                    }, function (err) {
                        _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                        resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                    });
                });
            });
        });
    };
    HmoProvider.prototype.uploadAttachments = function (attachments) {
        var _this = this;
        this.attachments = attachments;
        var x = 0;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                var url = _this.data_url + '/file/mobileupload?token=' + data['token'];
                var dataLength = (attachments.length - 1);
                var fileUploadStatus = [];
                var fileTransfer = _this.transfer.create();
                if (attachments.length == 0) {
                    resolve({ "message": "Success", "items": [] });
                }
                else {
                    attachments.forEach(function (element, index) {
                        var options = {
                            fileKey: 'file',
                            fileName: element.name,
                            chunkedMode: false,
                            mimeType: "image/jpeg",
                            headers: {}
                        };
                        fileTransfer.upload(element.path, url, options)
                            .then(function (res) {
                            _this.r = res;
                            fileUploadStatus[index] = JSON.parse(_this.r.response);
                            if (dataLength == index) {
                                resolve({ "message": "Success", "items": fileUploadStatus });
                            }
                        }, function (err) {
                            console.log(err);
                            if (x < 3) {
                                _this.uploadAttachments(_this.attachments);
                            }
                            x++;
                        });
                    });
                }
            });
        });
    };
    HmoProvider.prototype.saveReport = function (itemId, type, attachment) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessTokenForPost().then(function (data) {
                var url = _this.data_url + '/file/save';
                var headers = new Headers();
                headers.append('Content-Type', 'application/x-www-form-urlencoded');
                headers.append('Accept', 'application/json');
                headers.append('Authorization', 'Bearer ' + data['token']);
                var options = new RequestOptions({ headers: headers });
                var thisData = {
                    'attachment': attachment,
                    'attachment_type': type,
                    'item_id': itemId,
                };
                _this.http.post(url, thisData, options).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getAllTodayTransactions = function (isLoadNew, thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                var url = _this.data_url + "/hmo/transactions?";
                url += "page=" + thisData.current_page;
                url += "&token=" + data['token'];
                if (thisData['applied_filters']) {
                    if (thisData['applied_filters']['patient_id']) {
                        url += "&patient=" + thisData['applied_filters']['patient_id'];
                    }
                    if (thisData['applied_filters']['date_from'] !== undefined && thisData['applied_filters']['date_to'] !== undefined) {
                        url += "&date_from=" + thisData['applied_filters']['date_from'];
                        url += "&date_to=" + thisData['applied_filters']['date_to'];
                    }
                    if (thisData['applied_filters']['hospital_id']) {
                        url += "&hospital=" + thisData['applied_filters']['hospital_id'];
                    }
                }
                _this.http.get(url).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); }).subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.prototype.getAllPreviousTransactions = function (isLoadNew, thisData) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.siteProvider.getAccessToken().then(function (data) {
                var url = _this.data_url + "/hmo/transactions?";
                url += "page=" + thisData.current_page;
                url += "&token=" + data['token'];
                if (thisData['applied_filters']) {
                    if (thisData['applied_filters']['patient_id']) {
                        url += "&patient=" + thisData['applied_filters']['patient_id'];
                    }
                    if (thisData['applied_filters']['date_from'] !== undefined && thisData['applied_filters']['date_to'] !== undefined) {
                        url += "&date_from=" + thisData['applied_filters']['date_from'];
                        url += "&date_to=" + thisData['applied_filters']['date_to'];
                    }
                    if (thisData['applied_filters']['hospital_id']) {
                        url += "&hospital=" + thisData['applied_filters']['hospital_id'];
                    }
                }
                var version = "&v=" + _this.siteProvider.getVersion();
                var groupKey = "getAllPreviousTransactions";
                var request = _this.http.get(url + version).timeout(_this.siteProvider.timeout).map(function (res) { return res.json(); });
                var response = _this.cache.loadFromDelayedObservable(url, request, groupKey, _this.siteProvider.ttl, 'all');
                response.subscribe(function (data) {
                    resolve(data);
                }, function (err) {
                    _this.siteProvider.showToast(_this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: _this.siteProvider.generic_error_msg });
                });
            });
        });
    };
    HmoProvider.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    HmoProvider.ctorParameters = function () { return [
        { type: Http, },
        { type: CacheService, },
        { type: SiteProvider, },
        { type: Storage, },
        { type: FileTransfer, },
    ]; };
    return HmoProvider;
}());
export { HmoProvider };
//# sourceMappingURL=hmo.js.map
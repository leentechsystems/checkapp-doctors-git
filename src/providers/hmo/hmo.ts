import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/retry';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import { CacheService } from 'ionic-cache';
import { SiteProvider } from '../../providers/site/site';
import {JwtHelper} from "angular2-jwt";
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import 'rxjs/add/observable/forkJoin';
import { FileTransferObject, FileUploadOptions, FileTransfer } from '@ionic-native/file-transfer';

/*
  Generated class for the HmoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HmoProvider {

  jwtHelper = new JwtHelper();
  public base_url: any;
  data_url: any;
  accessToken = "";

  r:any;
  diagnosis: any;
  prescription: any;
  attachments: any;

  constructor(
    public http: Http,
    private cache: CacheService,
    public siteProvider: SiteProvider,
    public storage: Storage,
    public transfer: FileTransfer
  ) {
    console.log('Hello HmoProvider Provider');
    this.storage.get("sess_user_token").then((token) => {
           // Decode the Token
           let decodedToken = this.jwtHelper.decodeToken(token);
           this.base_url = this.siteProvider.protocol +"://"+ decodedToken['iss'];
           this.data_url = this.siteProvider.protocol +"://"+ decodedToken['iss']+"/api";
        });
  }

  getUserHmoAccount(loadNew, thisData) {
    return new Promise(resolve => {
        this.siteProvider.getAccessTokenForPost().then(data => {
          var url = this.data_url+'/hmo';
          var thisdata = JSON.stringify({
            "userId": parseInt(thisData.id)
          });

          let headers = new Headers();
          headers.append('Content-Type', 'application/x-www-form-urlencoded');
          headers.append('Accept', 'application/json');
          headers.append('Authorization', 'Bearer '+ data['token']);
        let options = new RequestOptions({headers: headers});

             this.http.post(url, thisdata, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                  resolve(data);
             }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
        });
    });
  }


  applyNewAccount(thisData) {
        return new Promise(resolve => {
          this.siteProvider.getAccessTokenForPost().then(data => {
            var url = this.data_url+'/hmo/newaccount';
            var thisdata = JSON.stringify(thisData);

            let headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});

               this.http.post(url, thisdata, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                    resolve(data);
               }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
          });
      });
  }

  addReport() {
    return new Promise(resolve => {
      let diagnosis, findings, hmoreport, transactionID, thisData;
      Observable.forkJoin([
        this.storage.get('diagnosis'),
        this.storage.get('findings'),
        this.storage.get('hmoreport'),
        this.storage.get('transaction_id'),
      ]).subscribe(res => {
        diagnosis = res[0];
        findings = res[1];
        hmoreport = res[2];
        transactionID = res[3];
        thisData = {
          'medical_report_hmo_transactions_id': transactionID,
          'diagnosis_illness_id': diagnosis.diagnosis,
          'diagnosis_content': diagnosis.remarks,
          'prescriptions_title': findings.findings,
          'prescriptions_content': findings.remarks,
          'hmo_report_title': hmoreport.findings,
          'hmo_report_content': hmoreport.remarks,
          'recommendation': hmoreport.recommendations
        }
        console.log('diagnosis', diagnosis);
        console.log('findings', findings);
        console.log('hmoreport', hmoreport);
        console.log(thisData);

        this.siteProvider.getAccessTokenForPost().then(data => {
          var url = this.data_url+'/appointment/complete';
          var thisdata = JSON.stringify(thisData);

          let headers = new Headers();
          headers.append('Content-Type', 'application/x-www-form-urlencoded');
          headers.append('Accept', 'application/json');
          headers.append('Authorization', 'Bearer '+ data['token']);
          let options = new RequestOptions({headers: headers});

              this.http.post(url, thisdata, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                  resolve(data);
              }, err => {
                    this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                    resolve({ error: 1, message: this.siteProvider.generic_error_msg });
                 });
        });
      });
    });
  }

  uploadAttachments(attachments) {

        this.attachments = attachments;
        let x = 0;
        return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {
            let url = this.data_url+'/file/mobileupload?token='+ data['token'];

            let dataLength = (attachments.length - 1);
            let fileUploadStatus = [];

            const fileTransfer: FileTransferObject = this.transfer.create();

            if(attachments.length == 0){

                resolve({"message":"Success", "items": []});

            }else{

                attachments.forEach((element, index) => {
                  let options: FileUploadOptions = {
                    fileKey: 'file',
                    fileName: element.name,
                    chunkedMode: false,
                    mimeType: "image/jpeg",
                    headers: {}
                  }
                  fileTransfer.upload(element.path, url, options)
                  .then((res) => {
                    this.r = res;
                    fileUploadStatus[index] = JSON.parse(this.r.response);
                    if (dataLength == index) {
                      resolve({"message":"Success", "items": fileUploadStatus});
                    }
                  }, (err) => {
                    console.log(err);

                      if(x < 3){
                          this.uploadAttachments(this.attachments);
                      }
                      x++;
                  });
                });

            }
          });
        });

  }

  saveReport(itemId, type, attachment) {
    return new Promise(resolve => {
          this.siteProvider.getAccessTokenForPost().then(data => {
            var url = this.data_url+'/file/save';
            let headers = new Headers();
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            headers.append('Accept', 'application/json');
            headers.append('Authorization', 'Bearer '+ data['token']);
            let options = new RequestOptions({headers: headers});
            let thisData = {
              'attachment': attachment,
              'attachment_type': type,
              'item_id': itemId,
            }
            this.http.post(url, thisData, options).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                resolve(data);
            }, err => {
              this.siteProvider.showToast(this.siteProvider.generic_error_msg);
              resolve({ error: 1, message: this.siteProvider.generic_error_msg });
           });
      });
    });
  }

   getAllTodayTransactions(isLoadNew, thisData) {
      return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {
               let url = this.data_url+"/hmo/transactions?";
               url += "page="+ thisData.current_page;
               url += "&token="+ data['token'];
                if(thisData['applied_filters']){
                  if(thisData['applied_filters']['patient_id']){
                   url += "&patient="+ thisData['applied_filters']['patient_id'];
                  }
                  if(thisData['applied_filters']['date_from'] !== undefined && thisData['applied_filters']['date_to'] !== undefined){
                   url += "&date_from="+ thisData['applied_filters']['date_from'];
                   url += "&date_to="+ thisData['applied_filters']['date_to'];
                  }
                  if(thisData['applied_filters']['hospital_id']){
                   url += "&hospital="+ thisData['applied_filters']['hospital_id'];
                  }
                }
               this.http.get(url).timeout(this.siteProvider.timeout).map(res => res.json()).subscribe((data) => {
                    resolve(data);
               }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
            });
      });
  }


  getAllPreviousTransactions(isLoadNew, thisData) {
      return new Promise(resolve => {
          this.siteProvider.getAccessToken().then(data => {
               let url = this.data_url+"/hmo/transactions?";
               url += "page="+ thisData.current_page;
               url += "&token="+ data['token'];
                if(thisData['applied_filters']){
                  if(thisData['applied_filters']['patient_id']){
                   url += "&patient="+ thisData['applied_filters']['patient_id'];
                  }
                  if(thisData['applied_filters']['date_from'] !== undefined && thisData['applied_filters']['date_to'] !== undefined){
                   url += "&date_from="+ thisData['applied_filters']['date_from'];
                   url += "&date_to="+ thisData['applied_filters']['date_to'];
                  }
                  if(thisData['applied_filters']['hospital_id']){
                   url += "&hospital="+ thisData['applied_filters']['hospital_id'];
                  }
                }
               let version = "&v="+ this.siteProvider.getVersion();
               let groupKey = "getAllPreviousTransactions";
               let request = this.http.get(url + version).timeout(this.siteProvider.timeout).map(res => res.json());
               let response = this.cache.loadFromDelayedObservable(url, request, groupKey, this.siteProvider.ttl, 'all');

               response.subscribe((data) => {
                    resolve(data);
                }, err => {
                  this.siteProvider.showToast(this.siteProvider.generic_error_msg);
                  resolve({ error: 1, message: this.siteProvider.generic_error_msg });
               });
            });
      });
  }

}

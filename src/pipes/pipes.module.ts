import { NgModule } from '@angular/core';
import { FormatDatePipe } from './format-date/format-date';
import { FormatDateAgoPipe } from '../pipes/format-date-ago/format-date-ago';
import { ShortenTextPipe } from './shorten-text/shorten-text';
import { FormatAccountnumberPipe } from './format-accountnumber/format-accountnumber';
import { KeysPipe } from './keys/keys';
import { CapitalizeTextPipe } from './capitalize-text/capitalize-text';

@NgModule({
	declarations: [
		FormatDatePipe,
		ShortenTextPipe,
		FormatAccountnumberPipe,
		FormatDateAgoPipe,
		KeysPipe,
		CapitalizeTextPipe
    ],
	imports: [],
	exports: [
		FormatDatePipe,
		ShortenTextPipe,
		FormatAccountnumberPipe,
		FormatDateAgoPipe,
		KeysPipe,
		CapitalizeTextPipe
	]
})

export class PipesModule {
	static forRoot(){
		return{
			ngModule: PipesModule,
			providers: []
		}
	}
}

import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the ShortenTextPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'shortenText',
})
export class ShortenTextPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
  	let val = value;
    val = value.substr(0, 100);

    if(value.length > 100) {
    	val += "...";
    }

    return val;
  }
}

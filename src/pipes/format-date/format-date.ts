import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FormatDataPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'formatDate',
})
export class FormatDatePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    	  	if(value && value !== "") {
			    //Count how many words were passed in
			    let pieces = value.split(" "); 

			    let wordCount = pieces[0].split("-");
			    let date_target_year = wordCount[0];
			    let date_target_month = wordCount[1];
			    let date_target_day = wordCount[2];
				let mlist = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

			    let date_target = mlist[parseInt(date_target_month) - 1] +" "+date_target_day+", "+date_target_year;
				return date_target;

			} else {
			   	return "";
			}
  }
}

import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  	name: 'formatDateAgo',
})

export class FormatDateAgoPipe implements PipeTransform {

  	transform(value, args) {

	  	if(value !== "") {

			var date = new Date(value);

			let date_send = new Date(date).getTime();
			let date_now = new Date().getTime();
			let date_target = date_now - date_send;

	        let wordDate = value.split(" ");
	        let wordCount = wordDate[0].split("-");

		    var month_name = function(dt){
				let mlist = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

			  	return mlist[dt.getMonth()];
			};


		    let target = month_name(new Date(value))+" "+wordCount[2]+", "+wordCount[0];
		  	let seconds = Math.floor(date_target/ 1000);
	        let minutes = Math.floor(date_target / (1000 * 60));
	        let hours = Math.floor(date_target / (1000 * 60 * 60));
	        let days = Math.floor(date_target / (1000 * 60 * 60 * 24));
			
	        if (seconds < 60) {

	            return "Just Now";

	        } else if (minutes < 2) {

	            return "a minute ago";

	        } else if (minutes < 60) {

	          	if(minutes > 1) {

	              	return minutes + " mins ago";

	          	} else {

	              	return minutes + " min ago";
	          	}

	        } else if (minutes == 60) {

	            return "1 hr ago";

	        } else if (hours < 24) {

	          	if(hours > 1) {

	              	return hours + " hrs ago";

	          	} else {

	              	return hours + " hr ago";

	          	}

	        } else if (days < 30) {

	          	if(days == 1) {

	              	return days + " day ago";

	          	} else if(days > 1 && days < 7){

	              	return days + " days ago";

	          	} else {

	          		let weeks = Math.floor(days / 7);

	          		if(weeks == 1){

	              		return weeks + " week ago";

	          		} else{

	              		return weeks + " weeks ago";

	          		}

	          	}

	        } else {

	            return target;
	        }

		} else {

		   	return "";

		}
  	}
}

import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FormatAccountnumberPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'formatAccountnumber',
})
export class FormatAccountnumberPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
  	let group1 = value.substr(0,4);
  	let group2 = value.substr(4,4);
  	let group3 = value.substr(8,4);
  	let group4 = value.substr(12,4);

    return group1 +"."+ group2 +"."+ group3 +"."+ group4;
  }
}

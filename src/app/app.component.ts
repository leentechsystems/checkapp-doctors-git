import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TabsPage } from '../pages/tabs/tabs';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { Storage } from '@ionic/storage';
import { CacheService } from "ionic-cache";
import { ImageLoaderConfig } from 'ionic-image-loader';
import { Badge } from '@ionic-native/badge';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;
  public google_api_key = "AIzaSyBs-lDYIShK6wLxjKXKf8nqYhxaDVbdM-E";

  constructor(platform: Platform, statusBar: StatusBar,
            splashScreen: SplashScreen, 
            private storage: Storage, 
            public cache: CacheService,
            private imageLoaderConfig: ImageLoaderConfig,
            public badge: Badge) {

    this.imageLoaderConfig.setBackgroundSize('cover');
    this.imageLoaderConfig.setCacheDirectoryName('checkapp-image-cache');
    this.imageLoaderConfig.setImageReturnType('base64');
    this.imageLoaderConfig.setFallbackFileNameCachedExtension('.jpg');
    this.imageLoaderConfig.enableSpinner(false);
    this.imageLoaderConfig.setFallbackUrl('assets/imgs/placeholder.png');
    this.imageLoaderConfig.enableFallbackAsPlaceholder(true);
    this.imageLoaderConfig.setMaximumCacheAge(7 * 24 * 60 * 60 * 1000); /*7 days*/
    this.imageLoaderConfig.enableDebugMode();
    this.imageLoaderConfig.setHeight('auto');

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      cache.setDefaultTTL(60 * 60 * 3); //set default cache TTL for 1 hour
      cache.setOfflineInvalidate(false); // Keep our cached results when device is offline!

      storage.ready().then(() => {
        storage.set('sess_google_api_key', this.google_api_key).then(key => { });

        storage.get('sess_user_login').then((val) => {
              if(!val) {
                this.rootPage = WalkthroughPage;
                this.badge.clear();
              } else {
                this.rootPage = TabsPage;
              }
        });
        
          statusBar.styleLightContent();
          statusBar.backgroundColorByHexString("#2d4963");
          splashScreen.hide();
      });
      statusBar.styleLightContent();
      statusBar.backgroundColorByHexString("#2d4963");
      splashScreen.hide();
    });
  }
}


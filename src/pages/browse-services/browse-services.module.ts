import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseServicesPage } from './browse-services';

@NgModule({
  declarations: [
    BrowseServicesPage,
  ],
  imports: [
    IonicPageModule.forChild(BrowseServicesPage),
  ],
})
export class BrowseServicesPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';

import { DoctorProvider } from '../../providers/doctor/doctor';

/**
 * Generated class for the BrowseServicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-browse-services',
	templateUrl: 'browse-services.html',
})
export class BrowseServicesPage {

	service_type: any;
	services: any;

	current_page = 1;
	total_services = 0;
	searched_keyword = "";
	doctor_services: any;
	categoriesChks:boolean[];
	tempSelectedChecks: any;
	service_counter: any;
	hospital_id: any;

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				public doctorProvider: DoctorProvider,
				public events: Events) {

		this.service_type = this.navParams.get('service_type');
		this.doctor_services = this.navParams.get('services');
		this.hospital_id = this.navParams.get('hospital_id'); 

		if(this.service_type){
			this.loadAllServices(false, true, false); 
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad BrowseServicesPage');
	}

	loadAllServices(refresher?, isLoadNew?, infiniteScroll?) {
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.current_page, /* separate each parameters bty comma*/
			"searched_keyword": this.searched_keyword,
			"service_type_id": this.service_type,
			"hospital_id": this.hospital_id
		};

		this.doctorProvider.getServices(isLoadNew, thisData).then(data => {
			if(data['result']) {
				this.total_services = data['result']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.services || isLoadNew === true) {
						this.services = []; 
					}
				}

				if(isLoadNew === true) {
					this.tempSelectedChecks = [];
				}else{
					this.tempSelectedChecks = this.categoriesChks;
				}

				data['result']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.services.push(cat);
					} 

					if(this.doctor_services){
						this.checkIfExistingSpeacilities(cat);
						if(this.service_counter != 0){
							this.tempSelectedChecks.push(true);
						}else{
							this.tempSelectedChecks.push(false);
						}
					}else{
						this.tempSelectedChecks.push(false);
					}

					if(infiniteScroll) {
						infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
					}
				});
				this.categoriesChks = this.tempSelectedChecks;
			}
		});
	}

	checkIfExistingSpeacilities(cat){
		this.service_counter = 0;
		this.doctor_services.forEach((srv, ids) => {
			if(cat.id == srv.service_id){
				this.service_counter ++;
			}
		});
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		if(this.services.length < this.total_services) {
			this.loadAllServices(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}

	searchKeyword(onCancel:any) {
		if(onCancel) {
			this.searched_keyword = "";
		}
		this.services = null;
		this.current_page = 1;
		this.loadAllServices(false, true, false); 
	}

	saveServices(){
		console.log('categoriesChks',this.categoriesChks);
		console.log('services',this.services);

		let selected_services = [];
		let temp_selected_services = [];
		let service_counter = 0;

		if(this.categoriesChks && this.services){
			console.log('fdsfsd');
			this.categoriesChks.forEach((cat, ids) => {
				if(cat == true){
					this.services.forEach((srv, idx) => {
						if(ids == idx){
							service_counter++;
							selected_services.push({
								'id': idx,
								'doctor_id': srv['description'],
								'hospital_id': srv['hospital_id'],
								'service_description': srv['description'],
								'service_id': srv['id'],
								'service_title': srv['title'],
								'hospital_service_id': srv['hospital_service_id']
							});

							temp_selected_services.push({
								"service_id": srv['id']
							});
						}
					});
				}
			});

			if(service_counter == 0){
				this.events.publish('add_doctor_services', {'data': null, 'total_count': service_counter}, Date.now());
			}else{
				this.events.publish('add_doctor_services', {'data': selected_services, 'total_count': service_counter}, Date.now());
			}

			this.navCtrl.pop();
			console.log(selected_services);
		}
	}
}

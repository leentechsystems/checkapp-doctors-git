import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseDoctorsPage } from './browse-doctors';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    BrowseDoctorsPage,
  ],
  imports: [
    IonicPageModule.forChild(BrowseDoctorsPage),
    IonicImageLoader,
  ],
})
export class BrowseDoctorsPageModule {}

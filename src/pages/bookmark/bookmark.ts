import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';
import { ProfileArticlePage } from '../../pages/profile-article/profile-article';
import { ProfileHospitalsPage } from '../../pages/profile-hospitals/profile-hospitals';
import { CallNumber } from '@ionic-native/call-number';
/**
 * Generated class for the BookmarkPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-bookmark',
	templateUrl: 'bookmark.html',
})
export class BookmarkPage {

	public favoriteSegment: any;
	segmentName = 'articles'; 

	all_articles: any;
	page_articles = 1;
	total_article_count = 0;  

	all_hospitals: any;
	page_hospitals = 1;
	total_hospitals_count = 0;  

	all_ambulance: any;
	page_ambulance = 1;
	total_ambulance_count = 0;  

	constructor(public navCtrl: NavController, 
				public navParams: NavParams, 
				public userProvider: UserProvider,
				public siteProvider: SiteProvider,
				public alertCtrl: AlertController, 
				private callNumber: CallNumber) {

		this.favoriteSegment = 'articles';
		this.loadAllData(false, true, false); 
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad BookmarkPage');
	}  

	reloadData(refresher?){
		console.log(this.segmentName);
		if(this.segmentName == 'articles'){
			this.all_articles = null;
			this.page_articles = 1;
			this.loadAllArticles(refresher, true, false);
		}else if(this.segmentName == 'hospitals'){
			this.all_hospitals = null;
			this.page_hospitals = 1; /* reset the current page to default */
			this.loadAllHospitals(refresher, true, false);
			
		}else if(this.segmentName == 'ambulance'){
			this.all_ambulance = null;
			this.page_ambulance = 1; /* reset the current page to default */
			this.loadAllAmbulance(refresher, true, false);
		}
	}

	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
		if(isLoadNew = true) { /* if refreshed */
			this.page_articles = 1; /* reset the current page to default */
			this.page_hospitals = 1; /* reset the current page to default */
			this.page_ambulance = 1; /* reset the current page to default */
		}

		this.loadAllArticles(refresher, isLoadNew, infiniteScroll); 
		this.loadAllHospitals(refresher, isLoadNew, infiniteScroll); 
		this.loadAllAmbulance(refresher, isLoadNew, infiniteScroll);
	}

	loadAllArticles(refresher?, isLoadNew?, infiniteScroll?) {
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.page_articles,
			"action": 'article',
		};

		this.userProvider.getBookmarks(isLoadNew, thisData).then(data => {
			if(data['error'] == 0) {
				this.total_article_count = data['result']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.all_articles || isLoadNew === true) {
						this.all_articles = []; 
					}
				}

				data['result']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.all_articles.push(cat);
					}
				});

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}

			if(refresher) {
				refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
			}
		});
	}

	loadAllHospitals(refresher?, isLoadNew?, infiniteScroll?) {
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.page_hospitals,
			"action": 'hospital',
		};

		this.userProvider.getBookmarks(isLoadNew, thisData).then(data => {
			if(data['error'] == 0) {
				this.total_hospitals_count = data['result']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.all_hospitals || isLoadNew === true) {
						this.all_hospitals = []; 
					}
				}

				data['result']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.all_hospitals.push(cat);
					}
				});

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}

			if(refresher) {
				refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
			}	
		});
	}

	loadAllAmbulance(refresher?, isLoadNew?, infiniteScroll?) {
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.page_ambulance,
			"action": 'ambulance',
		};

		this.userProvider.getBookmarks(isLoadNew, thisData).then(data => {
			if(data['error'] == 0) {
				this.total_ambulance_count = data['result']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.all_ambulance || isLoadNew === true) {
						this.all_ambulance = []; 
					}
				}

				data['result']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.all_ambulance.push(cat);
					}
				});

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}
			if(refresher) {
				refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
			}
		});
	}

	goToArticleInside(articleProfile) {
		this.navCtrl.push(ProfileArticlePage,{
			articleProfile : articleProfile
		});
	}

	gotoProfileHospital(hospital_profile) {
		this.navCtrl.push(ProfileHospitalsPage, {
			"hospital_profile": hospital_profile
		});
	}

	addtoBookmark(temp_bookmark, bookmark_index, bookmark_action, bookmark_title){

		let alert = this.alertCtrl.create({
			title: 'Remove Bookmark',
			cssClass: 'AcceptBookingAlert',
			buttons: [{
				text: 'No',
				role: 'cancel',
				handler: () => {
					console.log('Cancel clicked');

				}
			},{
				text: 'Yes',
				handler: () => {
					console.log('Open clicked');

					let thisData = {
						"item_id": temp_bookmark.id,
						"type": bookmark_action,
						"id": temp_bookmark.bookmark_id,
					};

					this.userProvider.saveBookmarks(thisData).then((data) => {
						if(data['error'] === 0){
							if(bookmark_action == 'article'){
								this.all_articles.forEach((cat, idx) => {
									if(bookmark_index == idx){
										this.all_articles.splice(bookmark_index, 1);
										this.total_article_count--;
									}
								});
							}
							else if(bookmark_action == 'hospital'){
								this.all_hospitals.forEach((cat, idx) => {
									if(bookmark_index == idx){
										this.all_hospitals.splice(bookmark_index, 1);
										this.total_hospitals_count--;
										console.log(this.total_hospitals_count);
									}
								});
							}
							else if(bookmark_action == 'ambulance'){
								this.all_ambulance.forEach((cat, idx) => {
									if(bookmark_index == idx){
										this.all_ambulance.splice(bookmark_index, 1);
										this.total_ambulance_count--;
										console.log(this.total_ambulance_count);
									}
								});
							}
						}
					}, err => {
						console.log(err);
					});
				}
			}]
		});

		alert.setMessage('Are you sure you want to remove <strong>'+ bookmark_title +'</strong> in bookmark list?');
		alert.present();
	}

	doInfiniteArticles(infiniteScroll:any) {
		this.page_articles += 1;
		if(this.all_articles.length < this.total_article_count) {
			this.loadAllArticles(false, false, infiniteScroll);
		}else {
			infiniteScroll.enable(false);
		}
	}

	doInfiniteHospitals(infiniteScroll:any) {
		this.page_hospitals += 1;
		if(this.all_hospitals.length < this.total_hospitals_count) {
			this.loadAllHospitals(false, false, infiniteScroll);
		}else {
			infiniteScroll.enable(false);
		}
	}

	doInfiniteAmbulance(infiniteScroll:any) {
		this.page_ambulance += 1;
		if(this.all_ambulance.length < this.total_ambulance_count) {
			this.loadAllAmbulance(false, false, infiniteScroll);
		}else {
			infiniteScroll.enable(false);
		}
	}

	currentSegmentPage(segmentName){
		console.log("segmentName",segmentName);
		this.segmentName = segmentName;
	}

	callNow(thisNumber) {
		if(thisNumber['contact']) {
			let tocall = "";
			if(thisNumber.contact.type === "mobile") {
				tocall += thisNumber.contact.country_code;
			} else {
				tocall += thisNumber.contact.area_code;
			}
			tocall += " "+ thisNumber.contact.number;
		 
			this.callNumber.callNumber(tocall, true)
			.then(res => console.log('Launched dialer!', res))
			.catch(err => console.log('Error launching dialer', err));
		}
	}
}

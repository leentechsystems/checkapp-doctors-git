import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookmarkPage } from './bookmark';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    BookmarkPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(BookmarkPage),
    IonicImageLoader,
  ],
})
export class BookmarkPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the AlertModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-alert-modal',
  templateUrl: 'alert-modal.html',
})
export class AlertModalPage {

  modal_type: any;

  store_platform: any;
  store_url: any;
  modal_data: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, 
  	public platform: Platform, public inAppBrowser: InAppBrowser) {
  	

    this.modal_type = this.navParams.get('modal_type');
    this.modal_data = this.navParams.get('modal_data') ? this.navParams.get('modal_data') : null;

    if(this.modal_data){
      console.log(this.modal_data);
      if (this.platform.is('ios')) {
          this.store_platform = "Apple Store";
          this.store_url = this.modal_data['appstore_url'];
      } else if (this.platform.is('android')) {
          this.store_platform = "Play Store";
          this.store_url = this.modal_data['playstore_url'];
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AlertModalPage');
  }

 	updateAction() {
	    this.viewCtrl.dismiss();
	    this.inAppBrowser.create(this.store_url, "_system", "location=true");
  	}

}
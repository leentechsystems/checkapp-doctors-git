import { Component } from '@angular/core';
import { IonicPage, NavController, App, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { EmailComposer } from '@ionic-native/email-composer';

import { BrowseArticlesPage } from './../browse-articles/browse-articles';
import { BrowseDoctorsPage } from './../browse-doctors/browse-doctors';
import { BrowseHospitalsPage } from './../browse-hospitals/browse-hospitals';
import { BrowseAmbulancePage } from './../browse-ambulance/browse-ambulance';
import { BrowseActivityLogsPage } from './../browse-activity-logs/browse-activity-logs';
import { BrowseHmoTransactionsPage } from './../browse-hmo-transactions/browse-hmo-transactions';
import { ProfileUserPage } from './../profile-user/profile-user';
import { TermsConditionPage } from './../terms-condition/terms-condition';
import { AboutPage } from './../about/about';
import { WalkthroughPage } from './../walkthrough/walkthrough';
import { ManageSchedulesPage } from './../manage-schedules/manage-schedules';
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';
import { ContactPage } from './../contact/contact';
import { FaqPage } from './../faq/faq';
import { BookmarkPage } from './../bookmark/bookmark';

/**
 * Generated class for the MorePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {

  termsConditionPage = TermsConditionPage;
  aboutPage = AboutPage;
  user_profile: any;
  photo: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, 
    private alertCtrl: AlertController, public appCtrl: App, private emailComposer: EmailComposer, 
    public siteProvider:SiteProvider, public loadingCtrl: LoadingController, public userProvider: UserProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MorePage');
  }

  goToBrowseArticles() {
    this.navCtrl.push(BrowseArticlesPage);
  }
  goToBrowseDoctors() {
    this.navCtrl.push(BrowseDoctorsPage);
  }
  goToBrowseHospitals() {
    this.navCtrl.push(BrowseHospitalsPage);
  }
  goToBrowseAmbulance() {
    this.navCtrl.push(BrowseAmbulancePage);
  }
  gotoActivityLogs() {
    this.navCtrl.push(BrowseActivityLogsPage);
  }  
  gotoTransactions() {
    this.navCtrl.push(BrowseHmoTransactionsPage);
  }
  gotoProfile() {
    this.navCtrl.push(ProfileUserPage, {
       "user_profile": this.user_profile
    });
  }
  gotoManageSchedules(){
    this.navCtrl.push(ManageSchedulesPage);
  }
  gotoBookmark() {
    this.navCtrl.push(BookmarkPage);
  }
  gotoAbout() {
    this.navCtrl.push(AboutPage);
  }
  gotoFaq() {
    this.navCtrl.push(FaqPage);
  }
  gotoTerms() {
    this.navCtrl.push(TermsConditionPage);
  }
  gotoContact(){
    this.navCtrl.push(ContactPage);
  } 
  logout() {
    let alert = this.alertCtrl.create({
      title: 'Log Out',
      message: 'Are you sure you want to log out?',
      cssClass: 'AcceptBookingAlert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Log Out',
          handler: () => {

            let loader = this.loadingCtrl.create();
            loader.present();

            this.storage.get("sess_device_token").then(token => {
              if(token) {

                let thisData = {
                  device_token: token
                };

                this.userProvider.removeDeviceToken(thisData).then(data => {
                  console.log(data);
                });
              } 
            });

            this.storage.remove('sess_user_login').then(() => {
                this.storage.remove('sess_user_hmo_account').then(() => {
                  this.storage.remove('sess_device_token');
                  this.appCtrl.getRootNav().setRoot(WalkthroughPage);
                  loader.dismiss();
                });
            });
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidEnter() {
     this.storage.get("sess_user_login").then((val) => {
          this.user_profile = val;
          if(val.attachments_filename && val.attachments_filepath) {
              this.photo = this.siteProvider.base_url + val.attachments_filepath +"/"+ val.attachments_filename;
          }
      });
  }

}

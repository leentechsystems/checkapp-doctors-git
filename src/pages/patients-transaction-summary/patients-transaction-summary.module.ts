import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsTransactionSummaryPage } from './patients-transaction-summary';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    PatientsTransactionSummaryPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(PatientsTransactionSummaryPage),
  ],
})
export class PatientsTransactionSummaryPageModule {}

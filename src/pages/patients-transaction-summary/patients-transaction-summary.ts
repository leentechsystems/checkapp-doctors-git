import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, ToastController } from 'ionic-angular';

import { HomePage } from './../home/home';
import { TabsPage } from './../tabs/tabs';
import { Storage } from '@ionic/storage';
import { HmoProvider } from '../../providers/hmo/hmo';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { RequestOptions } from '@angular/http';
import { SiteProvider } from '../../providers/site/site';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import 'rxjs/add/observable/forkJoin';
import { IllnessProvider } from '../../providers/illness/illness';
/**
 * Generated class for the PatientsTransactionSummaryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-patients-transaction-summary',
	templateUrl: 'patients-transaction-summary.html',
})
export class PatientsTransactionSummaryPage {
	transactionID: any;
	hmoTransactionID: any;
	r: any;
	reportDetails: any;
	diagnosisFiles: any;
	findingsFiles: any;
	diagattachment: Array<any> = [];
	fndgattachment: Array<any> = [];
	resdiag: any;
	resfndg: any;
	hospitalServices: any;

	diagDetails: any = {
		attachments: [],
	};
	fndgDetails: any = {
		attachments: [],
	};
	rprtDetails: any = {};

	ilnessList: any;
	res :any;
	loader: any;
	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
		public storage: Storage,
		public hmoProvider: HmoProvider,
		public appointment: AppointmentProvider,
		public hospitalsProvider: HospitalsProvider,
		public siteProvider: SiteProvider,
		public alert: AlertController,
		public illness: IllnessProvider,
		public loading: LoadingController,
		public toastCtrl: ToastController
	) {
		this.transactionID = navParams.get('transaction_id');
		console.log(this.transactionID);
		this.appointment.getAppointmentDetails(this.transactionID).then(data => {
			this.r = data;
			console.log(this.r);
			this.hmoTransactionID = this.r.appointments.hmo_transactions_id;
			this.storage.set('transaction_id', this.hmoTransactionID);
		});
		Observable.forkJoin([
			this.storage.get('diagnosis_attachment'), 
			this.storage.get('findings_attachment'), 
			this.storage.get('diagnosis'),
			this.storage.get('findings'),
			this.storage.get('hmoreport')
		]).subscribe(res => {
			// console.log(res[0]);
			// this.diagnosisFiles = res[0];
			// let diag = []
			// console.log(this.diagnosisFiles);
			// this.diagnosisFiles.forEach((element, index) => {
			//   console.log(index)
			//   this.diagattachment.push({"filename": element.filename, "filepath": element.filepath})
			// });

			// console.log(res[1]);
			// this.findingsFiles = res[1];
			// let fndg = []
			// this.findingsFiles.forEach((element, index) => {
			//   this.fndgattachment.push({"filename": element.filename, "filepath": element.filepath})
			// });

			this.diagDetails = res[2];
			this.fndgDetails = res[3];
			this.rprtDetails = res[4];
			console.log("diagDetails", this.diagDetails);
			console.log("fndgDetails", this.fndgDetails);
			console.log("rprtDetails", this.rprtDetails);
		})

		console.log("rhan", this.rprtDetails.recommendations);
		this.getAllIllness();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PatientsTransactionSummaryPage');
	}

	sendData() {
		this.loader = this.loading.create({
			content: 'Uploading Images...'
		});
		this.loader.present();
		Observable.forkJoin([
			this.hmoProvider.uploadAttachments(this.diagDetails.attachments),
			this.hmoProvider.uploadAttachments(this.fndgDetails.attachments),
		]).subscribe(res => {
			let diag, fndg;
			console.log(res[0]);
			diag = res[0];
			fndg = res[1];
			if(diag.message == "Success" && fndg.message == "Success") {
				this.loader.dismiss();
				this.diagnosisFiles = res[0];
				
				diag.items.forEach((element, index) => {
					console.log(index)
					this.diagattachment.push({"filename": element.files[0].filename, "filepath": element.files[0].filepath})
				});
				fndg.items.forEach((element, index) => {
					console.log(index)
					this.fndgattachment.push({"filename": element.files[0].filename, "filepath": element.files[0].filepath})
				});
				// Observable.forkJoin([
				//   this.storage.set('diagnosis_attachment', diag.items),
				//   this.storage.set('findings_attachment', fndg.items)
				// ]).subscribe(res => {
				this.gotoHome();
				// });
			}
		});
	}

	gotoHome(){
		console.log("diagnosis attachment", this.diagattachment);
		console.log("diagnosis attachment", this.fndgattachment);
		this.loader = this.loading.create({
			content: 'Sending data...'
		});
		this.loader.present();
		this.hmoProvider.addReport().then((data) => {
			console.log(data);
			this.r = data;
			Observable.forkJoin([
				this.hmoProvider.saveReport(this.r.diagnosis.id, 'diagnosis', this.diagattachment),
				this.hmoProvider.saveReport(this.r.prescription.id, 'prescriptions', this.fndgattachment),
			]).subscribe(res => {
				console.log(res);
				this.resdiag = res[0];
				this.resfndg = res[1];

				if(this.resdiag.message == "Success" && this.resfndg.message == "Success") {
					this.loader.dismiss();

					let alert = this.alert.create({
						title: "Send Prescription",
						message: "Your report has been successfully sent",
						enableBackdropDismiss: false,
						buttons: [
							{
								text: "OK",
								handler: () => {
									this.navCtrl.setRoot(TabsPage);
								}
							}
						]
					}).present();
				} else {

					this.loader.dismiss();
					this.presentToast("Something went wrong. Please try again.");
				}
			});
		})
	}

	getAllIllness() {
		this.illness.getAllIllnes().then(data => {
			this.res = data;
			this.ilnessList = this.res.cmspage;
			console.log(this.ilnessList); 
		});

	}

	parseToInt(x) {
		return parseInt(x);
	}


	presentToast(msg) {
		this.toastCtrl.create({
			message: msg,
			duration: 2000,
			position: "top"
		}).present();
	}

}

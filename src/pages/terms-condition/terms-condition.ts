import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the TermsConditionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terms-condition',
  templateUrl: 'terms-condition.html',
})
export class TermsConditionPage {
  content: any;

  constructor(public navCtrl: NavController,
  			     public navParams: NavParams,
              public siteProvider: SiteProvider, private storage: Storage ) {

  		this.storage.get("sess_termsandconditions").then(sess_termsandconditions => {
	        if(sess_termsandconditions) {
	            this.content = sess_termsandconditions;
	        } else {
	            this.siteProvider.getCmsPageContent("termsandconditions").then((data) => {
	                if(data['error'] === 0) {
	                  this.storage.set("sess_termsandconditions", data['cmspage']['content']).then(cms => {
	                    this.content = data['cmspage']['content'];
	                  });
	                }
	            });
	        }
	      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TermsConditionPage');
  }

}

import { Component } from '@angular/core';
import { App, NavController, NavParams, ModalController, LoadingController, AlertController, ToastController} from 'ionic-angular';

import { GlobalSearchPage } from './../global-search/global-search';
import { VerifyPage } from './../verify/verify';
import { AppointmentsScheduledPage } from './../appointments-scheduled/appointments-scheduled';
import { AppointmentsApprovalPage } from './../appointments-approval/appointments-approval';
import { AppointmentsPage } from './../appointments/appointments';
import { ViewModalPage } from './../view-modal/view-modal';
import { AcceptApprovalPage } from './../accept-approval/accept-approval';
import { BrowseHmoTransactionsPage } from './../browse-hmo-transactions/browse-hmo-transactions';

import { AppointmentProvider } from '../../providers/appointment/appointment';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { SiteProvider } from '../../providers/site/site';
import { Storage } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

@Component({
  	selector: 'page-home',
  	templateUrl: 'home.html'
})

export class HomePage {

  	userid = 0;
  	user_name = "";
  	user_prefix = "";

  	hour: any;
  	message = "";
  	searched_keyword = "";

  	current_page = 1;
  	today_appointments: any;
  	total_today_appointments = 0;
  	total_upcoming_appointments = 0;
  	total_pending_appointments = 0;
  	total_is_loaded: any;

  	scannedCode: any;
  	appointmentDetails: any;
  	approval_date : any;
	fabClicked: boolean = false;
	fabClickedCounter = 0;

  	constructor(public navCtrl: NavController, private storage: Storage,  public appointmentProvider: AppointmentProvider,
				public siteProvider: SiteProvider, public modalCtrl : ModalController, public app : App,
			  	public barcodeScanner: BarcodeScanner, public doctorProvider: DoctorProvider, public alertCtrl: AlertController,
			  	public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
  	}

  	ionViewDidLoad() {
		this.storage.get('sess_user_login').then((user) => {
			this.userid = user.id;
			this.user_prefix = user.prefix;
			this.user_name = user.firstname;
		});
  	}

  	ionViewDidEnter() {
	  	this.today_appointments = null;
	  	this.total_today_appointments = 0;
	  	this.total_upcoming_appointments = 0;
	  	this.total_pending_appointments = 0;
	  	this.messageDay();
	  	this.getTodayAppointment(true, false);
	  	this.getMainBanner();
  	}

  	messageDay(){
		let date = new Date();
		this.hour = date.getHours();

		if(this.hour >= 0 && this.hour <=11){
	  		this.message = "Good morning";
		}
		else if(this.hour >= 12 && this.hour <=17){
	  		this.message = "Good afternoon";
		}
		else{
	  		this.message = "Good evening";
		}
  	}

  	getMainBanner(){

		this.siteProvider.getMainBanner().then((data) => {
	  		console.log("data", data);
	  		this.total_upcoming_appointments = data['upcomming'];
	  		this.total_pending_appointments = data['pending'];
	  		this.total_is_loaded = data;
	  	}, err => {
	  		console.log(err);
		});
  	}

  	getTodayAppointment(isLoadNew?, infiniteScroll?){

		let thisData = {
		  	"current_page": this.current_page,
		  	"day": 'today',
		  	"date": '',
		  	"category": 'scheduled'
		};

	  	this.appointmentProvider.getDoctorsAppointments(isLoadNew, thisData).then(data => {
	  		if(data['error'] == 0){
                console.log('Check Me',data['appointments']);
			  	this.total_today_appointments = data['appointments']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
				  	if(!this.today_appointments || isLoadNew === true) {
					  	this.today_appointments = [];
					}
				}

				data['appointments']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {

						let wordDate = cat['appointment_date'].split(" ");
						let wordCount = wordDate[1].split(":");

						if(wordCount[0] < 12){
						  	cat['appointment_time'] = wordCount[0]+':'+wordCount[1]+' AM';

						}else{
						  	if(wordCount[0] == 12){
								cat['appointment_time'] = '12'+':'+wordCount[1]+' PM';
						  	}else{
								cat['appointment_time'] = (wordCount[0]-12) +':'+wordCount[1]+' PM';
						  	}
						}
						this.today_appointments.push(cat);
					}
				});

				if(infiniteScroll) {
				  infiniteScroll.complete();
				}
	  		}
	  	});
  	}

  	doInfinite(infiniteScroll:any) {
		this.current_page += 1;

		if(this.today_appointments.length < this.total_today_appointments) {
			this.getTodayAppointment(false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
  	}

  	gotoGlobalSearch(){
		this.navCtrl.push(GlobalSearchPage,{
	  		searched_keyword : this.searched_keyword
		});
  	}

  	gotoVerify(){
		this.navCtrl.push(VerifyPage);
  	}

  	gotoScheduled(){
		//this.navCtrl.push(AppointmentsScheduledPage);
		this.navCtrl.push(AppointmentsPage,{
			appointment: 'scheduled'
		});
  	}

  	gotoApproval(){
		//this.navCtrl.push(AppointmentsApprovalPage);
		this.navCtrl.push(AppointmentsPage,{
			appointment: 'approval'
		});
  	}

  	gotoViewModal(temp_patient_schedule){

		let doneModal = this.modalCtrl.create(ViewModalPage, {
		  	temp_patient_schedule: temp_patient_schedule
		});
		doneModal.onDidDismiss(data => {
			if(data){
		  		console.log(data);
			}
		});

		doneModal.present();
  	}

  	scanCode() {

    	this.barcodeScanner.scan({
          	"resultDisplayDuration" : 0,
      		/*'formats': 'QR_CODE',*/
			'prompt' : "Place a Qr Code in center of the square to scan it",

		}).then(barcodeData => {
			console.log(barcodeData);
			if (barcodeData.cancelled) {
			    console.log("User cancelled the action!");
			    console.log("User cancelled the action!");

			}else{
				barcodeData["text"] = JSON.parse(barcodeData.text);
				console.log(barcodeData.text['project']);
				console.log(barcodeData.text['user_id']);
				/*let temp_code = barcodeData.text.split("/");
	      		this.scannedCode = {
	      			'code' : temp_code[1],
	      			'patient_name': temp_code[2]
	      		};*/

	          	let loader = this.loadingCtrl.create({content: "Processing..."});
	          	loader.present();

				loader.dismiss();

	          	if(barcodeData.text['project'] == 'checkapp'){

		            let doneModal = this.modalCtrl.create(BrowseHmoTransactionsPage, {
		                "pageType": 'patient_transaction',
		                "patient_id":  barcodeData.text['user_id'],
		                "patient_name":  barcodeData.text['user_name'],
		            });

		            doneModal.onDidDismiss(data => {

		            });

		            doneModal.present();

	      		}else{
	      			this.presentToast('Sorry. Invalid format of Qr code.');
	      		}

/*	          	this.doctorProvider.verifyPatientTransaction(this.scannedCode).then((data) => {
	              	this.appointmentDetails = data['appointments'];
	                loader.dismiss();

	                if(this.appointmentDetails) {
	                	this.gotoModal();

	                } else {
	                    let alert = this.alertCtrl.create({
	                        title: 'Verify QR Transaction',
	                        message: 'Cannot find Transaction Number',
	                        cssClass: 'AcceptBookingAlert',
	                        buttons: [{
	                          	text: 'Okay',
	                          	role: 'cancel',
	                          	handler: () => {
	                              	console.log('Cancel clicked');
	                          	}
	                        }]
	                    });
	                    alert.present();
	                }

	            }, err => {
	              	console.log(err);
	          	});*/
			}

   		}, (err) => {
        	console.log('Error: ', err);
    	});

  	}

	gotoModal(){

/*    	let tempDate = this.appointmentDetails['appointment_date'].split(" ");
		this.approval_date = tempDate[0];

    	let thisData = {
      		"day": 'today',
      		"date": this.approval_date
    	};

	    this.appointmentProvider.getDoctorsAppointments(true, thisData).then((data) => {
	        console.log(data['appointments']['items']);

	        if(data['appointments']['items'].length !== 0){
	            data['appointments']['items'].forEach((cat, idx) => {

	                if(this.appointmentDetails['id'] == cat['id']){
	                   	this.appointmentDetails['attachments_filename'] = this.appointmentDetails['patient_user_profile_attachments_filename'];
	                    this.appointmentDetails['attachments_filepath'] = this.appointmentDetails['patient_user_profile_attachments_filename'];

	                  	let doneModal = this.modalCtrl.create(AcceptApprovalPage, {
	                      	"temp_patient_schedule": this.appointmentDetails,
	                      	"temp_date": this.approval_date,
	                      	"origin_page": "verify"
	                  	});

	                  	doneModal.onDidDismiss(data => {
	                  	});

	                  doneModal.present();
	                }
	            });

	        } else {

	            let alert = this.alertCtrl.create({
	                title: 'Verify QR Transaction',
	                message: 'Cannot find Transaction Number',
	                cssClass: 'AcceptBookingAlert',
	                buttons: [{
	                  	text: 'Okay',
	                  	role: 'cancel',
	                  	handler: () => {
	                      	console.log('Cancel clicked');
	                  	}
	                }]
	            });

	            alert.present();
	        }

	    }, err => {
	       console.log(err);
	    });*/
	}


  	presentToast(msg) {
      	this.toastCtrl.create({
	        message: msg,
	        duration: 2000,
	        position: "top"
    	}).present();
  	}

  	fabClick(){
  		console.log('fab clicked');
  		this.fabClickedCounter++;
  		console.log(this.fabClickedCounter);
  		if(this.fabClickedCounter % 2 == 0){
  			this.fabClicked = false;
  		}else{

  			this.fabClicked = true;
  		}
  	}
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AcceptApprovalPage } from './../accept-approval/accept-approval';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { DoctorProvider } from '../../providers/doctor/doctor';
/**
 * Generated class for the VerifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verify',
  templateUrl: 'verify.html',
})
export class VerifyPage {

	  public verificationCode: FormGroup;

    submitAttempts = false;
  	appointmentDetails: any;
  	approval_date : any;
    code: any;

  	constructor(public navCtrl: NavController, public navParams: NavParams,
  				public doctorProvider: DoctorProvider,
              	public alertCtrl: AlertController,
              	public loadingCtrl: LoadingController,
              	public formBuilder: FormBuilder,
            	  public appointmentProvider: AppointmentProvider,
              	public modalCtrl : ModalController,
                public toastCtrl: ToastController) {

	    this.verificationCode = formBuilder.group({
	        code: ['', Validators.compose([Validators.required])]
	    });
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad VerifyPage');
  	}


  	verifyAccount(){

        this.submitAttempts = true;

        if(!this.verificationCode.valid) {
            this.presentToast("Please fill-up all necessary fields");

        } else {
          	let loader = this.loadingCtrl.create({content: "Processing..."});
          	loader.present();
          	this.doctorProvider.verifyPatientTransaction(this.verificationCode.value).then((data) => {
                if(data['error'] == 0) {
                    this.appointmentDetails = data['appointments'];
                    if(this.appointmentDetails){
                        this.gotoModal();
                    }else{
                        let alert = this.alertCtrl.create({
                            title: 'Verify Transaction',
                            message: 'Cannot find your Transaction Number',
                            cssClass: 'AcceptBookingAlert',
                            buttons: [{
                                text: 'Okay',
                                role: 'cancel',
                                handler: () => {
                                    console.log('Cancel clicked');
                                }
                            }]
                        });
                        alert.present();
                    }
                }
                loader.dismiss();
            });
        }
  	}

	gotoModal(){

    let tempDate = this.appointmentDetails['appointment_date'].split(" ");
		this.approval_date = tempDate[0];

    let thisData = {
      "day": 'today',
      "date": this.approval_date

    };

    this.appointmentProvider.getAppointmentDetails(this.appointmentDetails['id']).then((data) => {
        console.log(data['appointments']);

        if(data['error']== 0){
            if(data['appointments']){
                this.appointmentDetails = data['appointments'];
                this.appointmentDetails['attachments_filename'] = this.appointmentDetails['patient_user_profile_attachments_filename'];
                this.appointmentDetails['attachments_filepath'] = this.appointmentDetails['patient_user_profile_attachments_filename'];
                let doneModal = this.modalCtrl.create(AcceptApprovalPage, {
                    "temp_patient_schedule": this.appointmentDetails,
                    "temp_date": this.approval_date,
                    "origin_page": "verify"
                });
                doneModal.onDidDismiss(data => {
                });
                doneModal.present();
            } else {
                let alert = this.alertCtrl.create({
                    title: 'Verify Transaction',
                    message: 'Cannot find your Transaction Number',
                    cssClass: 'AcceptBookingAlert',
                    buttons: [{
                      text: 'Okay',
                      role: 'cancel',
                      handler: () => {
                          console.log('Cancel clicked');
                      }
                    }]
                });
                alert.present();
            }
        }

    }, err => {
       console.log(err);
    });

	}



  presentToast(msg) {
      this.toastCtrl.create({
          message: msg,
          duration: 2000,
          position: "top"
    }).present();
  }

}

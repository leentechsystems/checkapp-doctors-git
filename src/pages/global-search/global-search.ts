import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BrowseArticlesPage } from '../../pages/browse-articles/browse-articles';
import { BrowseDoctorsPage } from '../../pages/browse-doctors/browse-doctors';
import { BrowseAmbulancePage } from '../../pages/browse-ambulance/browse-ambulance';
import { BrowseHospitalsPage } from '../../pages/browse-hospitals/browse-hospitals';

import { ProfileDoctorPage } from '../../pages/profile-doctor/profile-doctor';
import { ProfileHospitalsPage } from './../profile-hospitals/profile-hospitals';
import { ProfileArticlePage } from './../profile-article/profile-article';

import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number';

import { DoctorProvider } from '../../providers/doctor/doctor';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { AmbulanceProvider } from '../../providers/ambulance/ambulance';
import { ArticleProvider } from '../../providers/article/article';
import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the GlobalSearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-global-search',
  templateUrl: 'global-search.html',
})
export class GlobalSearchPage {

  featured_articles: any;
  all_articles: any;
  all_ambulance: any;
  all_hospitals: any;

  total_featured_count = 0;

  specialties: any;
  selected_specialties: any;
  applied_filters: any;

  accessToken:any = "";
  all_doctors: any;
  current_page = 1;

  total_count = 0; /* article */
  hospital_total_count = 0;
  doctor_total_count = 0;
  ambulance_total_count = 0;
  searched_keyword = "";

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public doctorProvider: DoctorProvider,
              public hospitalsProvider: HospitalsProvider,
              public ambulanceProvider: AmbulanceProvider,
              public articleProvider: ArticleProvider,
              private storage: Storage, private callNumber: CallNumber,
              public userProvider: UserProvider,
              public siteProvider: SiteProvider) {

        if(this.navParams.get('searched_keyword')){
          this.searched_keyword = this.navParams.get('searched_keyword');
        }

         this.loadAllData(false, true, false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GlobalSearchPage');
  }

  goToBrowseArticles() {
    this.navCtrl.push(BrowseArticlesPage, {
        "searched_keyword": this.searched_keyword
    });
  }
  goToBrowseDoctors() {
    this.navCtrl.push(BrowseDoctorsPage, {
        "searched_keyword": this.searched_keyword
    });
  }
  goToBrowseHospitals() {
    this.navCtrl.push(BrowseHospitalsPage, {
        "searched_keyword": this.searched_keyword
    });
  }
  goToBrowseAmbulance() {
    this.navCtrl.push(BrowseAmbulancePage, {
        "searched_keyword": this.searched_keyword
    });
  }
  gotoProfileHospital(hospital_profile) {
    this.navCtrl.push(ProfileHospitalsPage, {
        "hospital_profile": hospital_profile
    });
  }

  searchNews(onCancel:any) {
     if(onCancel) {
          this.searched_keyword = "";
      }
      this.featured_articles = null;
      this.all_articles = null;
      this.all_ambulance = null;
      this.all_hospitals = null;
      this.all_doctors = null;

      this.loadAllData(false, true, false);
  }

  onTyping(){
    this.searched_keyword = "";
  }

  goToArticleInside(articleProfile) {
    this.navCtrl.push(ProfileArticlePage,{
        articleProfile : articleProfile
    });
  }
  goToDoctorProfile(doctorProfile) {
    this.navCtrl.push(ProfileDoctorPage,{
        doctorProfile : doctorProfile
    });
  }

  loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
      if(isLoadNew = true) { /* if refreshed */
          this.current_page = 1; /* reset the current page to default */
      }

      /* - load functions responsible on getting and processing the data
      - we'll pass refresher so we will stop the refresher event after processing the data NOT promptly
      - we'll pass infiniteScroll so we will stop the infiniteScroll event after processing the data NOT promptly */

      this.loadFeatureArticles(isLoadNew);
      this.loadAllArticles(refresher, isLoadNew, infiniteScroll);
      this.loadAllAmbulance(refresher, isLoadNew, infiniteScroll);
      this.loadAllDoctors(refresher, isLoadNew, infiniteScroll);
      this.loadAllHospitals(refresher, isLoadNew, infiniteScroll);
  }

  loadFeatureArticles(isLoadNew?) {
      this.articleProvider.getFeaturedArticles(isLoadNew).then((data) => {
        if(data['error'] == 0){
          this.featured_articles = data['articles'];
          this.total_featured_count = data['articles']['total_count'];
        }
      });
  }

  loadAllArticles(refresher?, isLoadNew?, infiniteScroll?) {
    /* getting of data from the provider, we'll passing set of data the function is requiring */
      let thisData = {
        "current_page": this.current_page, /* separate each parameters bty comma*/
        "searched_keyword": this.searched_keyword
      };

      this.articleProvider.getAllArticles(isLoadNew, thisData).then(data => {
        if(data['error'] == 0){

          this.total_count = data['result']['total_count'];

          /* converting into array if variable is empty */
          /* emptying the variable when refreshed */
          if(!this.all_articles || isLoadNew === true) {
             this.all_articles = [];
          }


          data['result']['items'].forEach((cat, idx) => {
              if(cat.id !== undefined) {
                  this.all_articles.push(cat);
              }
          });
        }

        if(refresher) {
          refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
        }
      });
  }

  loadAllAmbulance(refresher?, isLoadNew?, infiniteScroll?) {

      let thisData = {
        "current_page": this.current_page,
        "searched_keyword": this.searched_keyword
      };

       this.ambulanceProvider.getAllAmbulance(isLoadNew, thisData).then(data => {
        if(data['error'] == 0){

          this.ambulance_total_count = data['result']['total_count'];

          if(!this.all_ambulance || isLoadNew === true) {
            this.all_ambulance = [];
          }


          data['result']['items'].forEach((cat, idx) => {
            if(cat.id !== undefined) {
                this.all_ambulance.push(cat);
            }
          });
        }

        if(refresher) {
          refresher.complete();
        }
    });
  }

  loadAllDoctors(refresher?, isLoadNew?, infiniteScroll?) {

      let thisData = {
        "current_page": this.current_page,
        "searched_keyword": this.searched_keyword,
        "applied_filters": null,
        "selected_specialties": ""
      };

       this.doctorProvider.getAllDoctors(isLoadNew, thisData).then(data => {
        if(data['error'] == 0){

          this.doctor_total_count = data['result']['total_count'];

          if(!this.all_doctors || isLoadNew === true) {
              this.all_doctors = [];
          }

          data['result']['items'].forEach((cat, idx) => {
              if(cat.id !== undefined) {
                  this.all_doctors.push(cat);
              }
          });
        }

        if(refresher) {
          refresher.complete();
        }
    });
  }

  loadAllHospitals(refresher?, isLoadNew?, infiniteScroll?) {

      let thisData = {
        "current_page": this.current_page,
        "searched_keyword": this.searched_keyword
      };

       this.hospitalsProvider.getAllHospitals(isLoadNew, thisData).then(data => {
        if(data['error'] == 0){

          this.hospital_total_count = data['result']['total_count'];

            if(!this.all_hospitals || isLoadNew === true) {
                 this.all_hospitals = [];
              }

          data['result']['items'].forEach((cat, idx) => {
                if(cat.id !== undefined) {
                    this.all_hospitals.push(cat);
                }
          });
        }

        if(refresher) {
          refresher.complete();
        }
    });
 }

  callNow(thisNumber) {
      if(thisNumber.contact) {
          let tocall = "";
          if(thisNumber.contact.type === "mobile") {
            tocall += thisNumber.country_code;
          } else {
            tocall += thisNumber.contact.area_code;
          }
          tocall += " "+ thisNumber.contact.number;

          this.callNumber.callNumber(tocall, true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
      }
  }

  addtoBookmark(temp_bookmark, bookmark_index, bookmark_action){
    console.log('temp_bookmark',temp_bookmark);
    console.log('bookmark_action',bookmark_action);

    let thisData = {
      "item_id": temp_bookmark.id,
      "type": bookmark_action,
    };

    if(temp_bookmark.bookmark_id){
      thisData['id'] = temp_bookmark.bookmark_id;
    }

    if(bookmark_action == 'article'){
      this.all_articles.forEach((cat, idx) => {
        if(bookmark_index == idx){
          if(temp_bookmark.bookmark_id){
            cat.bookmark_id = null;
          }else{
            cat.bookmark_id = 1;
          }
        }
      });
    }
    else if(bookmark_action == 'hospital'){
      this.all_hospitals.forEach((cat, idx) => {
        if(bookmark_index == idx){
          if(temp_bookmark.bookmark_id){
            cat.bookmark_id = null;
          }else{
            cat.bookmark_id = 1;
          }
        }
      });
    }
    else if(bookmark_action == 'ambulance'){
      this.all_ambulance.forEach((cat, idx) => {
        if(bookmark_index == idx){
          if(temp_bookmark.bookmark_id){
            cat.bookmark_id = null;
          }else{
            cat.bookmark_id = 1;
          }
        }
      });
    }

    this.userProvider.saveBookmarks(thisData).then((data) => {

      if(data['error'] === 0){

          if(bookmark_action == 'article'){
            this.all_articles.forEach((cat, idx) => {
              if(bookmark_index == idx){
                if(thisData['id']){
                  cat.bookmark_id = null;
                }else{
                  cat.bookmark_id = data['result']['id'];
                }
              }
            });
          }
          else if(bookmark_action == 'hospital'){
            this.all_hospitals.forEach((cat, idx) => {
              if(bookmark_index == idx){
                if(thisData['id']){
                  cat.bookmark_id = null;
                }else{
                  cat.bookmark_id = data['result']['id'];
                }
              }
            });
          }
          else if(bookmark_action == 'ambulance'){
            this.all_ambulance.forEach((cat, idx) => {
              if(bookmark_index == idx){
                if(thisData['id']){
                  cat.bookmark_id = null;
                }else{
                  cat.bookmark_id = data['result']['id'];
                }
              }
            });
        }
      }

    }, err => {

      console.log(err);

    });
  }

}

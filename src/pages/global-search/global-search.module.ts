import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GlobalSearchPage } from './global-search';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    GlobalSearchPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(GlobalSearchPage),
    IonicImageLoader,
  ],
})
export class GlobalSearchPageModule {}

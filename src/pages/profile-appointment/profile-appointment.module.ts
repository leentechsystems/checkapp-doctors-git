import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileAppointmentPage } from './profile-appointment';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ProfileAppointmentPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(ProfileAppointmentPage),
    IonicImageLoader,
  ],
})
export class ProfileAppointmentPageModule {}

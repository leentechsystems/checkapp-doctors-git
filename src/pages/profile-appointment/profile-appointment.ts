import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { ProfileDoctorPage } from '../../pages/profile-doctor/profile-doctor';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { PatientsProfilePage } from './../patients-profile/patients-profile';
import { UserProvider } from '../../providers/user/user';
import { PatientsDiagnosticsPage } from './../patients-diagnostics/patients-diagnostics';
import { ProfileMedicalRecordsPage } from './../profile-medical-records/profile-medical-records';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the ProfileAppointmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-appointment',
  templateUrl: 'profile-appointment.html',
})
export class ProfileAppointmentPage {

  patient_schedule = this.navParams.get('appointment_details');
  patient_profile: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
              public appointmentProvider: AppointmentProvider, public photoViewer: PhotoViewer,
              public loadingCtrl: LoadingController, public userProvider: UserProvider, public siteProvider: SiteProvider) {
    console.log(this.patient_schedule);

    if(this.patient_schedule){
        let wordDate = new Date(this.patient_schedule['appointment_date']).toString();
        console.log(wordDate);
        let wordCount = wordDate.split(" ");
        console.log(wordCount[0]);
        if(wordCount[0] == 'Sun'){
            this.patient_schedule['appointment_week'] = 'Sunday';

        }
        else if(wordCount[0] == 'Mon'){
            this.patient_schedule['appointment_week'] = 'Monday';

        }
        else if(wordCount[0] == 'Tue'){
            this.patient_schedule['appointment_week'] = 'Thuesday';

        }
        else if(wordCount[0] == 'Wed'){
            this.patient_schedule['appointment_week'] = 'Wednesday';

        }
        else if(wordCount[0] == 'Thu'){
            this.patient_schedule['appointment_week'] = 'Thursday';

        }
        else if(wordCount[0] == 'Fri'){
            this.patient_schedule['appointment_week'] = 'Friday';

        }
        else if(wordCount[0] == 'Sat'){
            this.patient_schedule['appointment_week'] = 'Saturday';

        }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileAppointmentPage');
  }

  escapeModal() {
      this.viewCtrl.dismiss();
  }

  viewProfile() {
    //this.viewCtrl.dismiss();

    this.userProvider.getPatientProfile(this.patient_schedule['patient_user_id']).then((data) => {
      this.patient_profile = data['user'];

      this.patient_schedule['hospital_address'] = this.patient_schedule['hospital_address_address'];
      this.patient_schedule['hospital_city'] = this.patient_schedule['hospital_address_city'];
      this.patient_schedule['hospital_region'] = this.patient_schedule['hospital_address_region'];
      this.patient_schedule['hospital_zip'] = this.patient_schedule['hospital_address_zip'];
      this.patient_schedule['hospital_country'] = this.patient_schedule['hospital_address_country'];

      this.patient_schedule['prefix'] = this.patient_schedule['doctor_prefix'];
      this.patient_schedule['suffix'] = this.patient_schedule['doctor_suffix'];
      this.patient_schedule['firstname'] = this.patient_schedule['doctor_firtname'];
      this.patient_schedule['lastname'] = this.patient_schedule['doctor_lastname'];
      this.patient_schedule['medical_degree'] = this.patient_schedule['doctor_medical_degree'];
      this.patient_schedule['id'] = parseInt(this.patient_schedule['doctor_id']);
      this.patient_schedule['hospital_latitude'] = this.patient_schedule['hospital_address_lat'];
      this.patient_schedule['hospital_longtitude'] = this.patient_schedule['hospital_address_lng'];

  /*    this.navCtrl.push(ProfileDoctorPage, {
        "hospital_profile": this.patient_schedule
      });*/

      //this.patient_schedule['appointment_status'] = this.patient_schedule['status'];

      console.log("temp_patient_appointment", this.patient_schedule);

      this.navCtrl.push(PatientsProfilePage, {
        "temp_patient_profile": this.patient_profile,
        "temp_patient_appointment" : this.patient_schedule,
        "origin_page": "appointments"
      });

    }, err => {
        console.log(err);
    });
  }

  viewPhoto(attachment){
    console.log(attachment);
    this.photoViewer.show(attachment);
  }

  actionAppointment(thisAction){

    let thisData = {
        "id": this.patient_schedule['id'],
        "action": thisAction
    };

    let loader = this.loadingCtrl.create({content: "Processing..."});
    loader.present();

    this.appointmentProvider.approveAppointment(thisData).then((data) => {
          loader.dismiss();
          this.viewCtrl.dismiss(data['results']);

      }, err => {
          console.log(err);
    });
  }

  gotoDiagnostics(){
    let params = {
      'transaction_id': this.patient_schedule.appointment_id,
      'patient_schedule': this.patient_schedule
    }
    this.navCtrl.push(PatientsDiagnosticsPage, params);
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  gotoProfileMedicalRecord() {
    let thisData = {
      'type': "consultation",
      'user_id': this.patient_schedule.patient_user_id,
      'transactionId': this.patient_schedule.appointment_hmo_transactions_id,
    };

    this.userProvider.getMedicalReport(true, thisData).then(data => {
      if(data['error'] === 0) {

        this.navCtrl.push(ProfileMedicalRecordsPage, {
            "report": data['result']['items'][0],
        });
      }
    });
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';
import { IonicSelectableComponent } from 'ionic-selectable';

/**
 * Generated class for the FormFilterAmbulancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-form-filter-ambulance',
  templateUrl: 'form-filter-ambulance.html',
})
export class FormFilterAmbulancePage {
 public filterForm: FormGroup;
filter_selected: any;
city: any;
regions: any;
region: any;
cities: any;
is_change_region = false;
is_submit = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    public viewCtrl: ViewController, public storage: Storage, public siteProvider: SiteProvider) {
  		this.filterForm = formBuilder.group({
	        city: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
      });

      this.storage.get("sess_regions").then(sess_regions => {
        console.log(sess_regions);
          if(sess_regions) {
              this.regions = sess_regions;

          } else {
              this.siteProvider.getRegions().then(data => {
                console.log(data);
                  if(data['error'] === 0) {
                    this.regions =  data['provinces'];
                  }
              });
          }
      });

  }

   applyFilter(thisAction) {
    this.is_submit = true;

    this.filter_selected = this.filterForm.value;
    if(thisAction === "apply") {
        if(this.city) {
          this.filter_selected['city'] = this.city['citymunDesc'];
          this.viewCtrl.dismiss(this.filter_selected);
        }
    } else {
        this.filter_selected['city'] = "";
        this.viewCtrl.dismiss(this.filter_selected);
    }
  }

  escapeModal() {
    this.viewCtrl.dismiss();
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  regionChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.region = event.value;
    this.city = ''
    this.getCity();
  }

  cityChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormFilterAmbulancePage');
  }
  getCity() {
    console.log(this.region);
    this.is_change_region = true;
    this.siteProvider.getCitiesByRegions(this.region.provCode).then(city => {
        this.cities = city['city'];
         this.is_change_region = false;
    });
  }
}

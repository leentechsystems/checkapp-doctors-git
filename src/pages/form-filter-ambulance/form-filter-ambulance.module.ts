import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormFilterAmbulancePage } from './form-filter-ambulance';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  declarations: [
    FormFilterAmbulancePage,
  ],
  imports: [
    IonicSelectableModule,
    IonicPageModule.forChild(FormFilterAmbulancePage),
  ],
})
export class FormFilterAmbulancePageModule {}

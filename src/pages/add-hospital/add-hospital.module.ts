import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddHospitalPage } from './add-hospital';

@NgModule({
  declarations: [
    AddHospitalPage,
  ],
  imports: [
    IonicPageModule.forChild(AddHospitalPage),
  ],
})
export class AddHospitalPageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController, Events } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { ProfileHospitalsPage } from './../profile-hospitals/profile-hospitals';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
/**
 * Generated class for the AddHospitalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-add-hospital',
	templateUrl: 'add-hospital.html',
})
export class AddHospitalPage {

	accessToken:any = "";
	all_hospitals: any;
	current_page = 1;
	total_count = 0;
	searched_keyword = (this.navParams.get('searched_keyword')) ? this.navParams.get('searched_keyword') : "";
	hospital_data = (this.navParams.get('hospital_data')) ? this.navParams.get('hospital_data') : null;
	my_total_hospital = (this.navParams.get('my_total_hospital')) ? this.navParams.get('my_total_hospital') : null;

	specialties: any;
	selected_specialties: any;
	current_location: any;
	counter: any;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public viewCtrl: ViewController,
				public modalCtrl: ModalController,
				public hospitalsProvider: HospitalsProvider,
				private storage: Storage,
				public events: Events) {

		this.storage.get("sess_current_location").then(sess_current_location => {
			if(sess_current_location) {
				this.current_location = sess_current_location;
			}
			this.loadAllData(false, true, false);
		});

		events.subscribe('add_hospital', (hospital_data, time) => {
			this.hospital_data.push(hospital_data);
		 	this.all_hospitals = [];
		 	this.current_page = 1;
			this.loadAllData(false, true, false);
		});
		console.log('my_total_hospital', this.my_total_hospital);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AddHospitalPage');
	}

	goBack() {
		this.viewCtrl.dismiss();
	}

	stopPropagation(e) {
	  e.stopPropagation();
	}

	gotoProfileHospital(hospital_profile) {
		this.navCtrl.push(ProfileHospitalsPage, {
			"hospital_profile": hospital_profile,
			"page_action": 'add_hospital',
			"my_total_hospital": this.my_total_hospital
		});
	}

	reloadData(refresher?){
		this.all_hospitals = null;
		this.current_page = 1;
		this.loadAllData(refresher, true, false);
	}

	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {

		console.log(this.current_page);
		let origin_coordinates = {};

		if(this.current_location) {
			origin_coordinates = {
				"lat": this.current_location['lat'],
				"lng": this.current_location['lng'],
			};
		} else {
			origin_coordinates = {
				"lat": 0,
				"lng": 0,
			};
		}

		let thisData = {
			"current_page": this.current_page,
			"searched_keyword": this.searched_keyword,
			"origin_coordinates": origin_coordinates
		};

		this.hospitalsProvider.getAllUnselectHospitals(isLoadNew, thisData).then(data => {
			console.log('data',data);
			if(data['error'] == 0){

				this.total_count = data['result']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					if(!this.all_hospitals || isLoadNew === true) {
						this.all_hospitals = [];
					}
				}

				data['result']['items'].forEach((cat, id) => {
					if(cat.id !== undefined) {
						this.checkIfExistingHospital(cat)
						if(this.counter == 0){
							this.all_hospitals.push(cat);
						}
					}
				});
				console.log(this.all_hospitals);

				if(infiniteScroll) {
					infiniteScroll.complete();
				}
			}

			if(refresher) {
				refresher.complete();
			}
		});
	}

	checkIfExistingHospital(cat){
		if(cat){
			this.counter = 0;
			this.hospital_data.forEach((hosp, idx) => {
				if(cat.id === hosp.id) {
					this.counter ++;
				}
			});
		}
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		console.log(this.all_hospitals.length +'<'+ this.total_count);
		if(this.all_hospitals.length < this.total_count) {
			this.loadAllData(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}

	searchNews(onCancel:any) {
		if(onCancel) {
			this.searched_keyword = "";
		}
		this.all_hospitals = null;
		this.current_page = 1;
		this.loadAllData(false, true, false);
	}

	onTyping(){
		this.searched_keyword = "";
	}
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsProfilePage } from './patients-profile';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    PatientsProfilePage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(PatientsProfilePage),
    IonicImageLoader,
  ],
})
export class PatientsProfilePageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PatientsDiagnosticsPage } from './../patients-diagnostics/patients-diagnostics';
import { BrowseMedicalRecordsPage } from '../../pages/browse-medical-records/browse-medical-records';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the PatientsProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-patients-profile',
	templateUrl: 'patients-profile.html',
})
export class PatientsProfilePage {

	patient_profile = this.navParams.get('temp_patient_profile');
	patient_appointment = this.navParams.get('temp_patient_appointment');
	patient_contact: any;
	patient_hmo: any;
	transactionID: any;
	origin_page = (this.navParams.get('origin_page')) ? this.navParams.get('origin_page') : null;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public siteProvider: SiteProvider) {

		console.log("patient_profile", this.patient_profile);
		console.log("patient_appointment", this.patient_appointment);
		console.log('origin_page',this.origin_page);

		if(this.patient_profile){
			this.patient_contact = this.patient_profile['contact'];
			this.patient_hmo = this.patient_profile['hmo'];
			this.transactionID = this.patient_appointment['id'];
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PatientsProfilePage');
	}

	gotoDiagnostics(){
		if(this.patient_appointment['appointment_status']) {
			let params = { 'transaction_id': this.transactionID,
				'patient_schedule': this.patient_appointment
			}
			this.navCtrl.push(PatientsDiagnosticsPage, params);
		}
	}

	goToBrowseMedicalRecords() {
		this.navCtrl.push(BrowseMedicalRecordsPage,{
			userProfile : this.patient_profile
		});
	}
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ToastController, Events} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { DoctorProvider } from '../../providers/doctor/doctor';
/**
 * Generated class for the AddSchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-schedule',
  templateUrl: 'add-schedule.html',
})
export class AddSchedulePage {

	userData: any;
	days = [];
    schedule_days: Array<{title: string, id: any}>;
	public schedule = {
	   timeStarts: '07:00',
	   timeEnds: '19:00',
	   timeStartsParam: 'AM',
	   timeEndsParam: 'PM',
	}

	public doctorsSchedule: FormGroup;
  	hospitals: any;
  	isEnabled: any;
  	//hospitalList: any;
  	timeslot_change = false;
  	action_title: any;
  	action_message: any;

	time_from: any;
	time_to: any;
	timeStartsHourParam: any;
	timeEndsHourParam: any;
    public time_slot : Array<{timeStarts: string, timeEnds: string, timeStartsParam: string, timeEndsParam: string}>;
    final_time_slot = [];
  	submitAttempts : boolean = false;
  	temp_doctorsSchedule = (this.navParams.get('temp_doctorsSchedule')) ? this.navParams.get('temp_doctorsSchedule') : null;
  	hospitalList = (this.navParams.get('doctorsHospital')) ? this.navParams.get('doctorsHospital') : null;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
  				public loadingCtrl: LoadingController, public doctorProvider: DoctorProvider, private storage: Storage,
  				public alertCtrl: AlertController, public toastCtrl: ToastController, public events: Events) {
  		
        this.time_slot = [];   
        this.schedule_days = [
            { title: 'Su', id: '1'},
            { title: 'Mo', id: '2'},
            { title: 'Tu', id: '3'},
            { title: 'We', id: '4'},
            { title: 'Th', id: '5'},
            { title: 'Fr', id: '6'},
            { title: 'Sa', id: '7'},
        ];   

    	if(this.temp_doctorsSchedule){
    		console.log('temp_doctorsSchedule',this.temp_doctorsSchedule);
	    	this.hospitals = this.temp_doctorsSchedule['doctor_schedule_hospital_id'];
		    this.doctorsSchedule = formBuilder.group({
		        hospitals: ['this.hospitals', Validators.required]
		    });

	    	this.days.push(this.temp_doctorsSchedule['doctor_schedule_day_id']);
	    	console.log(this.days);

	    }else{
			this.isEnabled = false;
  			this.days = [];
	    	this.time_slot.push(this.schedule);

		    this.doctorsSchedule = formBuilder.group({
		        hospitals: ['', Validators.required]
		    });

	    }

        this.storage.get('sess_user_login').then((val) => {
            this.userData = val;
        });
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad AddSchedulePage');	 
	   	//this.getDoctorsHospitals();   
  	}  	

  	ionViewDidEnter(){

    	if(this.temp_doctorsSchedule){
    		console.log('temp_doctorsSchedule',this.temp_doctorsSchedule);
    		this.isEnabled = true;
		    this.temp_doctorsSchedule['schedules'].forEach((time, tidx) => {

	    		/*if(time['time_from_unit'] == 'PM'){
	    			time['time_from'] = (parseFloat(time['time_from']) + 12) +':00';
	    		}else{
	    			if(parseFloat(time['time_from']) < 10){
	    				time['time_from'] = '0'+time['time_from']+':00'; 
	    			}
	    			if(parseFloat(time['time_from']) == 12){
	    				time['time_from'] = '00:00'; 
	    			}
	    		}   		

	    		if(time['time_to_unit'] == 'PM'){
	    			time['time_to'] = (parseFloat(time['time_to']) + 12) +':00';
	    		}else{
	    			if(parseFloat(time['time_to']) < 10){
	    				time['time_to'] = '0'+time['time_to']+':00'; 
	    			}
	    			if(parseFloat(time['time_to']) == 12){
	    				time['time_to'] = '00:00'; 
	    			}
	    		}*/

				let temp_timeslot = {
				   timeStarts: time['time_slot']['time_from'],
				   timeEnds: time['time_slot']['time_to'],
				   timeStartsParam: time['time_slot']['time_from_parameter'],
				   timeEndsParam: time['time_slot']['time_to_parameter'],
				}	
		    	this.time_slot.push(temp_timeslot);
	    	});
	    	console.log(this.time_slot);
		}
  	}

  	timeslotChange(){
  		this.timeslot_change = true;
  	}

  	addTimeSlot(){
  		let time_slot = {
		   'timeStarts': '07:00',
		   'timeEnds': '19:00',
		   'timeStartsParam': 'AM',
		   'timeEndsParam': 'PM',
  		}
  		this.time_slot.push(time_slot);
  	}

/*  	this.last_sum;
  	this.temp_checkTimeSlot = [];
  	this.temp_checkTimeSlot.push(parseInt(this.time_slot[0]['timeStarts']));
  	this.temp_checkTimeSlot.push(parseInt(this.time_slot[this.time_slot.length - 1]['timeEnds']));
  	this.time_slot.forEach((time, sidx) => {
  		this.last_sum = parseInt(time['timeStarts']) + 1;
  		if(this.last_sum < parseInt(time['timeEnds'])){
  			this.temp_checkTimeSlot.push(this.last_sum);
  		}
  	});*/

  	removeTimeSlot(timeSlotIndex){
  		this.time_slot.splice(timeSlotIndex, 1);
  	}

  	getDoctorsHospitals(){

	    this.doctorProvider.getDoctorsHospitalList().then(data => {
	    	if(data['error'] == 0){
	    		this.hospitalList = data['hospitals']['items']; 
	    	}
	    });
  	}

	applyDay(tempDay){
		if(this.days.indexOf(tempDay) > -1){
			this.days.forEach((cat, idx) => {
				if(cat == tempDay){
					this.days.splice(idx, 1);
				}
			});
		}else{
			this.days.push(tempDay);
		}
	}

    checkIfDays(action) {

        if(this.days.indexOf(action) > -1){
            return "yes";
        } else {
            return "none";
        }
    }

	saveSchedule(){

		console.log('time slot', this.time_slot.length);
		console.log('days', this.days.length);
		console.log('doctorsSchedule', this.doctorsSchedule.valid);
        this.submitAttempts = true;

        if(!this.doctorsSchedule.valid || (this.days.length == 0 || this.time_slot.length == 0)) {
            this.presentToast("Please fill-up all required fields");

        }else{

			this.final_time_slot = [];

	    	this.time_slot.forEach((cat, idx) => {

		        let timeStarts = cat['timeStarts'].split(":");
				let timeEnds = cat['timeEnds'].split(":");

				/*if(parseInt(cat['timeStarts']) < 10){
		        	let timeStartsHour = timeStarts[0].split("0");
					this.time_from = timeStartsHour[1];

				}else{
					this.time_from = timeStarts[0];
				}*/

				/*if(parseInt(cat['timeEnds']) < 10){
		        	let timeEndsHour = timeEnds[0].split("0");
					this.time_to = timeEndsHour[1];

				}else{
					this.time_to = timeEnds[0];
				}*/

		        /*if(this.time_from < 12){
		            this.timeStartsHourParam = 'AM';

		        }else{
		            this.timeStartsHourParam = 'PM';
		        }

		        if(this.time_to < 12){
		           	this.timeEndsHourParam = 'AM';

		        }else{
		            this.timeEndsHourParam = 'PM';
		        }*/

		       	let timeslot = {
			        "time_from": cat['timeStarts'],
			        "time_from_unit": cat['timeStartsParam'],
			        "time_to": cat['timeEnds'],
			        "time_to_unit": cat['timeEndsParam']
		    	};	    

		        this.final_time_slot.push(timeslot);

	    	});
		        
		    let thisData = {
			    "day_id": this.days,
			    "user_id": this.userData['id'],
			    "time_slot": this.final_time_slot,
			    "status": '1',
			    "hospital_id": this.hospitals,
			    "max_patient": '0',
			    "id": ''
		   	};

		    let loader = this.loadingCtrl.create({content: "Processing..."});
		    loader.present();

		    this.doctorProvider.setDoctorsSchedule(thisData).then((data) => {
		    	if(data['error'] == 0){

					if(this.temp_doctorsSchedule){
						this.action_title = 'Successfully Update your Schedule';
						this.action_message = 'Your schedule is successfully updated. Press Okay to see you Schedules';

					}else{
						this.action_title = 'Successfully Addded your Schedule';
						this.action_message = 'Your schedule is added in your Schedule List. Press Okay to see you Schedules';
					}

		            let alert = this.alertCtrl.create({
		                title: this.action_title,
		                message: this.action_message,
		                cssClass: 'AcceptBookingAlert',
		             	buttons: [{
		                    text: 'Okay',
		                    role: 'cancel',
		                    handler: () => {
		                       	console.log('Cancel clicked');
		                       	this.events.publish('add_schedule', 'true', Date.now());
				    		    this.navCtrl.pop();
		                   	}
		                }]
		            });
		            alert.present();
		    	}else{
		    		this.presentToast(data['message']);
		    	}

		      	loader.dismiss();

		      	}, err => {

		      	console.log(err);

		    	});

        }
	}

	presentToast(msg) {
	    this.toastCtrl.create({
	        message: msg,
	        duration: 2000,
	        position: "top"
		}).present();
	}

	leavePage(){

		if(this.timeslot_change == true){

		   	let alert = this.alertCtrl.create({
		        title: '',
		        message: 'Are you sure you want to leave the page without saving?',
		        cssClass: 'AcceptBookingAlert',
		        buttons: [{
		            text: 'Leave',
		            role: 'cancel',
		            handler: () => {
		                console.log('Leave clicked');
		    			this.navCtrl.pop();
		            }
		        },
		        {
		            text: 'Stay',
		            role: 'cancel',
		            handler: () => {
		                console.log('Stay clicked');
		            }
		        }]
		    });
		    alert.present();
		}else {

		    this.navCtrl.pop();
		}
	}

}

  
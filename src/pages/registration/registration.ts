import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, ToastController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TabsPage } from '../../pages/tabs/tabs';
import { Storage } from '@ionic/storage';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook } from '@ionic-native/facebook';
import { FCM } from '@ionic-native/fcm';

import { EmailValidator } from './../../validators/email';
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the RegistrationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  	selector: 'page-registration',
  	templateUrl: 'registration.html',
})
export class RegistrationPage {
 	public registrationForm: FormGroup;
	passwordType = "password";
	 passwordIcon = "ios-eye";
 	passwordStatus = "hide";

 	submitAttempt: boolean = false;
 	loginData: any;
 	is_submit: boolean = false;
	deviceToken: any;

 	email = "";
	users: any;
	profile_url: any;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, public loadingCtrl:LoadingController,
  			  	private storage: Storage, private events: Events, public userProvider: UserProvider, public siteProvider: SiteProvider,
  			 	public toastCtrl: ToastController, public emailValidator: EmailValidator, private alertCtrl: AlertController,
  			 	public googlePlus: GooglePlus, public facebook: Facebook, public fcm: FCM) {

  		this.registrationForm = formBuilder.group({
  			firstname: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*') ])],
  			lastname: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*') ])],
	        email: ['', Validators.compose([Validators.required, EmailValidator.emailValidator])],
	        password: ['', Validators.compose([Validators.minLength(10), Validators.required])],
			request_type: ['doctor_app', Validators.required],
			user_role_id: ['7', Validators.required],
	    });


  	}

  	ionViewDidEnter(){
  		this.users = [];
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad RegistrationPage');
		this.fcm.getToken().then(token => {
			if(token){
		  		this.deviceToken = token;
				this.storage.set("sess_device_token", token).then((token) => { 
					console.log('device_token',token);
				});	
			}
		})
		//this.deviceToken = 1234;
  	}

  	togglePassword() {
		if(this.passwordStatus == "hide") {
			this.passwordStatus = "show";
			this.passwordType = "text";
			this.passwordIcon = "ios-eye-off-outline";
		} else {
			this.passwordStatus = "hide";
			this.passwordType = "password";
			this.passwordIcon = "ios-eye";
		}
 	}
 	showToast(msg) {
	    const toast = this.toastCtrl.create({
	      	message: msg,
	      	showCloseButton: false,
    	  	duration: 3000,
	      	position: 'top'
	    });
	    toast.present();
	}

	signUpNow() {
	 	this.is_submit = true;

		if(!this.registrationForm.valid){
			this.showToast("Please check all input fields");

		} else {
		    let loader = this.loadingCtrl.create();
			loader.present();

			this.siteProvider.getGuestAccessToken().then(data => {

				this.registrationForm.value['token'] = this.deviceToken;
			    this.userProvider.checkMail(this.registrationForm.value, data).then(here => {

					if(here['error'] === 1){
			    		loader.dismissAll();
		    			this.showToast(here['message']);

		    		} else {
			    		loader.dismissAll();
				    	this.userProvider.signup(this.registrationForm.value, data).then(here => {
				    		if(here['error'] === 0){
				    			let alert = this.alertCtrl.create({
			    			      	title: 'Successful Registration',
			    			      	message: 'Your account has been created successfully',
			    			      	cssClass: 'AcceptBookingAlert',
			    			      	buttons: [{
			    			          	text: "Okay",
			    			          	handler: () => {
			    			              	this.storage.set('sess_user_login', here['user']).then((val) => {
				    			              	this.storage.set("sess_user_token", here['token']).then((token) => {
								        	  		this.navCtrl.setRoot(TabsPage); 
								        	  	});
											}, err => {
						 						this.showToast("Something went wrong. Please try again.");
						 					});
			    			          	}
			    			      	}]
			    			    });
			    			    alert.present(); 
							}
				        });
			    	}
		    	});
		    });
		}	
	}

	loginFacebook(){

		this.facebook.login(['public_profile', 'email'])
			.then(res => {
			    if(res.status === "connected") {
			        this.getUserDetail(res.authResponse.userID);

			   	} else {
			       	console.log("etoa res una",res);
			   	}
			})
	    .catch(e => {
	      	console.log("error", e);
			this.showToast('Failed to connect in Facebook. Network is unreachable');
	    });
	}

	getUserDetail(userid) {

	  	this.facebook.api("/"+userid+"/?fields=id,email,name,first_name,last_name,picture.width(720).height(720)",["public_profile"])
	    	.then(res => {
	      		console.log("etoaaa profile",res);

		    	var userData = {
			        'firstname': res.first_name,
			        'lastname': res.last_name,
			        'email': res.email,
			        //'profile_photo': res.picture.data.url,
			        'fb_user_id': res.id,
					'request_type': 'doctor_app',
					'user_role_id': '7',
					'token' : this.deviceToken,
		    	};

		    	let loader = this.loadingCtrl.create({
			        content: ""
			    });
			    loader.present();

				this.siteProvider.getGuestAccessToken().then(data => {

				    this.userProvider.checkMail(userData, data).then(here => {

						if(here['error'] === 1){
			    			loader.dismissAll();
			    			this.showToast(here['message']);

			    		} else {

					    	this.userProvider.signup(userData, data).then(here => {

			    				loader.dismissAll();
					    		console.log('facebook_photo',res.picture.data.url);

							    if(here['error'] === 0){
							    	this.storage.set("sess_user_token", here['token']);
	      							this.users = here['user'];

									if(res.picture.data.url){

						    			this.profile_url = res.picture.data.url;
							          	let pp_pieces = this.profile_url.split("=");
							          	console.log('picture123',pp_pieces);
							          	let new_pp = {
							            	"name": pp_pieces[pp_pieces.length - 1]+'.jpg',
							            	"path": this.profile_url
							          	};

							          	let thisPhoto = [];
							          	thisPhoto.push(new_pp);
										this.uploadProfilePhoto(thisPhoto, here['user']['id']);
									}

							    	let alert = this.alertCtrl.create({
						    			title: 'Successful Registration',
						    			message: 'Your account has been created successfully',
						    			cssClass: 'AcceptBookingAlert',
						    			buttons: [{
						    			    text: "Okay",
						    			    handler: () => {
									    		this.storage.set('sess_user_login', this.users).then((val) => {
													}, err => {
												 		this.showToast("Something went wrong. Please try again.");
												});
						    			        this.navCtrl.setRoot(TabsPage); 
						    			    }
						    			}]
						    		});
						    		alert.present(); 
								}
					        });
				    	}
			    	});
			    });
	    	})

	    .catch(e => {
	      	console.log("error", e);
			this.showToast('Failed to connect in Facebook. Network is unreachable');
	    });
	}

	loginGoogle(){

		this.googlePlus.login({}).then(res => {

			console.log("googlePlusProfile", res);

			//let google_name = res.displayName.split(" ");

			var userData = {
			    'firstname': res.givenName,
			    'lastname': res.familyName,
			    'email': res.email,
			    'google_user_id': res.userId,
				'request_type': 'doctor_app',
				'user_role_id': '7',
				'token' : this.deviceToken,
				//'profile_photo': res.imageUrl,
				//'location': res.placesLived
			};

			console.log("google profile",userData);

			let loader = this.loadingCtrl.create({
			    content: ""
		    });

			loader.present();

			this.siteProvider.getGuestAccessToken().then(data => {

				this.userProvider.checkMail(userData, data).then(here => {

					if(here['error'] === 1){
			    		loader.dismissAll();
			    		this.showToast(here['message']);

			    	} else {

					    this.userProvider.signup(userData, data).then(here => {
			                loader.dismiss();

					    	console.log('google_photo', res.imageUrl);

						    if(here['error'] === 0){
							    this.storage.set("sess_user_token", here['token']);
	      						this.users = here['user'];

						    	if(res.imageUrl){

						    		this.profile_url = res.imageUrl;
							        let pp_pieces = this.profile_url.split("/");
							        console.log('picture123',pp_pieces);
							        let new_pp = {
							            "name": pp_pieces[pp_pieces.length - 1]+'.jpg',
							           	"path": this.profile_url
							        };

							        let thisPhoto = [];
							        thisPhoto.push(new_pp);
									this.uploadProfilePhoto(thisPhoto, here['user']['id']);
						    	}

						    	let alert = this.alertCtrl.create({
					    			title: 'Successful Registration',
					    			message: 'Your account has been created successfully',
					    			cssClass: 'AcceptBookingAlert',
					    			buttons: [{
					    			    text: "Okay",
					    			    handler: () => {
									    	this.storage.set('sess_user_login', this.users).then((val) => {
												}, err => {
											 		this.showToast("Something went wrong. Please try again.");
											});
						    			    this.navCtrl.setRoot(TabsPage); 
					    			    }
					    			}]
					    		});
					    		alert.present(); 
							}
					    });
				   	}
			   	});
			});
	    })
	    
	    .catch(err => {
	    	console.log(err);
			this.showToast('Failed to connect in GooglePlus. Network is unreachable');
	    });
	}

	uploadProfilePhoto(thisPhoto, userid){
		console.log(userid);
		this.siteProvider.downloadPhoto(thisPhoto).then(res1 => {
			console.log(res1);
	        if(res1['error'] === 0) {
				this.siteProvider.uploadPhoto(res1['items']).then(res => {
					console.log(res);
		            if(res['error'] === 0) {
		                let attachments = [];
		                res['items'].forEach((items, iidx) => {
		                    attachments.push({"filename": items.files[0].filename, "filepath": items.files[0].filepath})
		               	});

		                let attachmentsData = {
		                   	"attachment_type": "user",
		                    "item_id": userid,
		                    "attachment": attachments
		                };

		                this.siteProvider.saveAttachment(attachmentsData).then(saveAttachmentResult => {
		                    if(saveAttachmentResult['error'] === 0) {
						    	this.users['attachments_id'] = saveAttachmentResult['attachments'][0]['id'];
						    	this.users['attachments_filename'] = saveAttachmentResult['attachments'][0]['filename'];
						    	this.users['attachments_filepath'] = saveAttachmentResult['attachments'][0]['filepath'];
		                        console.log("saveAttachmentResult", saveAttachmentResult);
		                    } 
		                });   
		            }
		        });
			}
		});	
	}
}

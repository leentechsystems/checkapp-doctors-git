import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { DoctorProvider } from '../../providers/doctor/doctor';
/**
 * Generated class for the FormFilterPatientsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-form-filter-patients',
	templateUrl: 'form-filter-patients.html',
})
export class FormFilterPatientsPage {
	public filterTransaction: FormGroup;
	//gender: any;
	range: any;
	filter_selected: any;

	filterData = this.navParams.get('data') ? this.navParams.get('data') : null ;
	filter_type = this.navParams.get('filter_type') ? this.navParams.get('filter_type') : null ;

	hospitals: any;
	today: any;
  	today_year: any;
  	today_month: any;
  	today_day: any;
  	current_date: any;

  	hospitalList:any;
  	public event: any;
  	submitAttempts : boolean = false;

	constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
				public viewCtrl: ViewController, public doctorProvider: DoctorProvider) {

		console.log(this.filter_type);

		if(this.filter_type == 'patients'){
			if(this.filterData){
				//this.gender = this.filterData['gender'];
				this.range = this.filterData['range'];
			}else{
				//this.gender = 'female';
				this.range = 'previous';
			}
		}else{

	      	this.today = new Date();
	      	this.today_month = this.today.getMonth() + 1;
	      	this.today_day = this.today.getDate();
	      	this.today_year = this.today.getFullYear();

	      	if(this.today_month < 10){
	        	this.today_month = '0'+this.today_month;
	      	}
	      	if(this.today_day < 10){
	        	this.today_day = '0'+this.today_day;
	      	}

	      	this.current_date = this.today_year +"-"+this.today_month+"-"+this.today_day;

	   		this.getDoctorsHospitals();
			if(this.filterData){
		      	this.event = {
				    date_to: this.filterData['date_to'],
				    date_from: this.filterData['date_from'],
		  		};

	    		this.hospitals = this.filterData['hospital_id'];
				this.filterTransaction = formBuilder.group({
					hospitals: ['this.hospitals']
				});
			}else{

		      	this.event = {
				    date_to: this.current_date,
				    date_from: this.current_date,
		  		};

				this.filterTransaction = formBuilder.group({
					hospitals : [''],
				});
			}
		}
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad FormFilterPatientsPage');
	}

/*  	filterGender(gender){
		this.gender = gender;
	}  	*/

	getDoctorsHospitals(){
	    this.doctorProvider.getDoctorsHospitalList().then(data => {
	    	this.hospitalList = data['hospitals']['items'];
	    });
  	}

	escapeModal() {
	  this.viewCtrl.dismiss();
	}

	stopPropagation(e) {
	  e.stopPropagation();
	}

	applyFilter(thisAction) {

		if(thisAction == 'apply'){
			if(this.filter_type == 'patients'){
				this.filter_selected = {
					//gender: this.gender,
					range: this.range
				};
			}else{
				if(this.hospitals){
					this.filter_selected = {
						date_from: this.event.date_from,
						date_to: this.event.date_to,
						hospital_id: this.hospitals,
					};
				}else{
					this.filter_selected = {
						date_from: this.event.date_from,
						date_to: this.event.date_to,
					};
				}
			}
			this.viewCtrl.dismiss(this.filter_selected);

		}else{
			this.viewCtrl.dismiss(thisAction);
		}
	}
}

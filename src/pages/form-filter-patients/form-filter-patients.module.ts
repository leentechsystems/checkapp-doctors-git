import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormFilterPatientsPage } from './form-filter-patients';

@NgModule({
  declarations: [
    FormFilterPatientsPage,
  ],
  imports: [
    IonicPageModule.forChild(FormFilterPatientsPage),
  ],
})
export class FormFilterPatientsPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientScheduledProfilePage } from './patient-scheduled-profile';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    PatientScheduledProfilePage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(PatientScheduledProfilePage),
  ],
})
export class PatientScheduledProfilePageModule {}

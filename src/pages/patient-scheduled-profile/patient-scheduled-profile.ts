import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { PatientsDiagnosticsPage } from './../patients-diagnostics/patients-diagnostics';
import { BrowseMedicalRecordsPage } from '../../pages/browse-medical-records/browse-medical-records';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the PatientScheduledProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-patient-scheduled-profile',
	templateUrl: 'patient-scheduled-profile.html',
})

export class PatientScheduledProfilePage {

	patient_profile = this.navParams.get('temp_patient_profile');
	patient_schedule = this.navParams.get('patient_schedule');
	patient_contact: any;
	patient_hmo: any;
	transactionID: any;
	constructor(public navCtrl: NavController, public navParams: NavParams, public siteProvider: SiteProvider,
		public alertCtrl: AlertController,) {

		this.patient_contact = this.patient_profile['contact'];
		this.patient_hmo = this.patient_profile['hmo'];
		this.transactionID = navParams.get('transaction_num')
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad PatientScheduledProfilePage');
	}

	gotoDiagnostics(){
		if(this.patient_schedule['status'] === '4') {
			let params = { 
				'transaction_id': this.transactionID,
				'patient_schedule': this.patient_schedule
			}
			this.navCtrl.push(PatientsDiagnosticsPage, params);

		} else {
			let alert = this.alertCtrl.create({
				title: 'Send Prescription',
				message: 'Cannot send prescription as of now. Please contact the gatekeeper',
				cssClass: 'AcceptBookingAlert',
				buttons: [{
					text: 'Okay',
					role: 'cancel',
					handler: () => {
							console.log('Cancel clicked');
					}
				}]
			});

			alert.present();
		}
	}

	goToBrowseMedicalRecords() {
		this.navCtrl.push(BrowseMedicalRecordsPage,{
				userProfile : this.patient_profile
		});
	}
}

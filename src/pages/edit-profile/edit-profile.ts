import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { ModalInfoPage } from '../../pages/modal-info/modal-info'

/**
 * Generated class for the EditProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  	public profileForm: FormGroup;
  	medical_degrees: any;
  	prefixes: any;
  	suffixes: any;
  	user_profile = this.navParams.get('user_profile');
  	editing_section = this.navParams.get('editing_section');
  	is_submit = false;

  	constructor(public navCtrl: NavController,
  				public navParams: NavParams,
  				public formBuilder: FormBuilder,
				public toastCtrl: ToastController,
				public userProvider: UserProvider,
				public alertCtrl: AlertController,
				public storage: Storage,
				public loadingCtrl: LoadingController,
				public modalCtrl: ModalController) {

		this.medical_degrees = ["B. N.","B.S.N.","M.N.","M.N.A.","D.D.S.","D.M.D.","D.O.","D.P.T.","D.S.N.","D.Sc.PT","M.D.","O.D.","Pharm.D."];
	   	this.prefixes = ["Ms.", "Mr.", "Mrs.", "Dr."];

	  	console.log("editing_section", this.editing_section);
	  	console.log("user_profile", this.user_profile);
        console.log("Licenece Number", this.user_profile['license_number']);

		this.profileForm = formBuilder.group({
		  	license_number: [this.user_profile['license_number'], Validators.compose([Validators.minLength(7), Validators.maxLength(7)])],
		  	email: [this.user_profile['email'], Validators.required],
		  	firstname: [this.user_profile['firstname'], Validators.required],
		  	lastname: [this.user_profile['lastname'], Validators.required],
		  	midlename: [this.user_profile['midlename']],
		  	mothers_maiden_name: [this.user_profile['mothers_maiden_name']],
		  	medical_degree: [this.user_profile['medical_degree']],
		  	date_of_birth: [this.user_profile['date_of_birth']],
		  	gender: [this.user_profile['gender']],
		  	biography: [this.user_profile['biography']],
		  	country: ['PH'],
		  	region: [this.user_profile['address_region']],
		  	city: [this.user_profile['address_city']],
		  	address: [this.user_profile['address_address']],
		  	zip: [this.user_profile['address_zip']],
		  	prefix: [this.user_profile['prefix']],
		  	suffix: [this.user_profile['suffix']],
		  	address_id: [this.user_profile['address_id'], Validators.required],
		  	id: [this.user_profile['id'], Validators.required],
		  	status: [this.user_profile['id'], Validators.required],
	  	});
  	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad EditProfilePage');
  	}

  	saveChanges() {
	  	this.is_submit = true;
	  	console.log(this.profileForm);

	 	if(!this.profileForm.valid) {
			this.presentToast("Please fill-up all necessary fields");

	 	} else {
			let loader = this.loadingCtrl.create();
			loader.present();

	  		let formData = this.profileForm.value;
	  		formData['date_of_birth'] = this.profileForm.value['date_of_birth'];

			this.userProvider.editProfile(formData).then(res => {
				console.log('res',res);
				if(res['error'] === 0) {
					let new_sess_user_login = this.user_profile;
					new_sess_user_login['license_number'] = this.profileForm.value['license_number'];
					new_sess_user_login['prefix'] = this.profileForm.value['prefix'];
					new_sess_user_login['firstname'] = this.profileForm.value['firstname'];
					new_sess_user_login['midlename'] = this.profileForm.value['midlename'];
					new_sess_user_login['mothers_maiden_name'] = this.profileForm.value['mothers_maiden_name'];
					new_sess_user_login['lastname'] = this.profileForm.value['lastname'];
					new_sess_user_login['suffix'] = this.profileForm.value['suffix'];
					new_sess_user_login['medical_degree'] = this.profileForm.value['medical_degree'];
					new_sess_user_login['gender'] = this.profileForm.value['gender'];
					new_sess_user_login['date_of_birth'] = this.profileForm.value['date_of_birth'];
					new_sess_user_login['biography'] = this.profileForm.value['biography'];

					this.storage.set("sess_user_login", new_sess_user_login).then(sess_user_login => {
						let alert = this.alertCtrl.create({
							title: 'Edit Profile',
							message: 'Your profile has been updated successfully!',
							cssClass: 'AcceptBookingAlert',
							buttons: [{
								text: 'Back to Profile',
								role: 'cancel',
								handler: () => {
									this.navCtrl.pop();
									console.log('Cancel clicked');
								}
							}]
						});
						alert.present();
					});
				}
				loader.dismiss();
			});
	 	}
 	}

  	presentToast(msg) {
	  	this.toastCtrl.create({
		  	message: msg,
		  	duration: 2000,
		  	position: "top"
	  	}).present();
  	}

  	addContact(contact=null, contact_idx=null){
  		console.log(contact);
		let contactInfoModal = this.modalCtrl.create(ModalInfoPage,{
	  		modal_type : 'contact_info',
	  		modal_contact : contact,
	  		contact_idx: contact_idx
		});

		contactInfoModal.onDidDismiss(data => {
			if(data){
				this.user_profile = data;
			}
		});

		contactInfoModal.present();
  	}
}

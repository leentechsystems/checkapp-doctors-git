import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, App, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';
import { WalkthroughPage } from './../walkthrough/walkthrough'; /* 04-20-2018 */

/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {

   old_password: any;
   new_password: any;

   passwordType = "password";
   passwordIcon = "ios-eye";
   passwordStatus = "hide";

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, 
    public toastCtrl: ToastController, public appCtrl: App,  public userProvider: UserProvider, public siteProvider: SiteProvider, private alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }

  presentToast(msg) {
      this.toastCtrl.create({
          message: msg,
          duration: 2000,
          position: "top"
      }).present();
  }

  resetPassword() {
      if(!this.old_password || !this.new_password) {
          this.presentToast("Please fill-up all necessary fields");

      } else {
          if(this.new_password.length > 10) {

                  let loader = this.loadingCtrl.create();
                  loader.present();

                  let formData = {
                      "old_password": this.old_password,
                      "new_password": this.new_password,
                  };

                  this.userProvider.resetPassword(formData).then(res => {
                      if(res['error'] === 0) {
                          let alert = this.alertCtrl.create({
                            title: 'Success',
                            message: 'Your password has been changed successfully.',
                            buttons: [{
                                text: "Okay",
                                handler: () => {
                                    this.storage.remove('sess_user_login').then(() => {
                                        this.storage.remove('sess_user_hmo_account').then(() => {
                                          this.appCtrl.getRootNav().setRoot(WalkthroughPage);
                                          loader.dismiss();
                                        });
                                    });
                                }
                            }]
                          });
                          alert.present();

                      } else {
                          let msg = "";
                          if(res['message'] === "Invalid password."){
                              msg = "Invalid old password";
                          } else {
                              msg = res['message'];
                          }
                          this.presentToast(msg);
                          loader.dismiss();
                      }
                  });
   
          } else {
              this.presentToast("Password must be atleast ten (10) characters");
          }
      }
  }

  togglePassword() {
    if(this.passwordStatus == "hide") {
      this.passwordStatus = "show";
      this.passwordType = "text";
      this.passwordIcon = "ios-eye-off-outline";
    } else {
      this.passwordStatus = "hide";
      this.passwordType = "password";
      this.passwordIcon = "ios-eye";
    }
  }

}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the ProfileMedicalRecordsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-medical-records',
  templateUrl: 'profile-medical-records.html',
})
export class ProfileMedicalRecordsPage {
   records_tabs = "diagnosis";
   report = (this.navParams.get('report')) ?  this.navParams.get('report') : "";
   recommendations: any;
   total_recommendation = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public userProvider: UserProvider, public siteProvider: SiteProvider) {
  		console.log("report", this.report);
      this.medicalRecommendation();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileMedicalRecordsPage');
  }

  /* from rhan */
  medicalRecommendation(){
    this.userProvider.getMedicalRecommendation(this.report['hmo_transactions_id']).then(data => {
      console.log(this.recommendations);
      if(data['error'] == 0){
        this.recommendations = data['result']['items'];
        this.total_recommendation = data['result']['total_count'];
      }
    });
  } 

  /* end */
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileMedicalRecordsPage } from './profile-medical-records';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ProfileMedicalRecordsPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(ProfileMedicalRecordsPage),
    IonicImageLoader,
  ],
})
export class ProfileMedicalRecordsPageModule {}

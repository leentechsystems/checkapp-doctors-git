import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
 
  accessToken:any = "";
  aboutData: any;

  constructor(public navCtrl: NavController,
  			     public navParams: NavParams,
              public siteProvider: SiteProvider, private storage: Storage ) {

  		 this.storage.get("sess_about").then(sess_about => {
        if(sess_about) {
            this.aboutData = sess_about;
        } else {
            this.siteProvider.getCmsPageContent("about").then((data) => {
                if(data['error'] === 0) {
                  this.storage.set("sess_about", data['cmspage']['content']).then(cms => {
                    this.aboutData = data['cmspage']['content'];
                  });
                }
            });
        }
      });

  }
 
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events, LoadingController, AlertController, ToastController } from 'ionic-angular';

import { DoctorProvider } from '../../providers/doctor/doctor';
import { BrowseServicesPage } from './../browse-services/browse-services';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the AddServicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-services',
  templateUrl: 'add-services.html',
})
export class AddServicesPage {

 	total_service_type = 0;
 	serviceType: any;
 	serviceTypeId: any;
 	serviceTypeName: any;
 	hospitalInfo = (this.navParams.get('hospital')) ? this.navParams.get('hospital') : null;
 	doctor_services_change:boolean = false;
 	doctor_services: any;
 	total_doctor_services = 0;
 	userProfile: any;

	selectServiceType = {
	    title: 'Service Type',
	    subTitle: '',
	};

  	constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events,
  		public modalCtrl: ModalController, public doctorProvider: DoctorProvider, public loadingCtrl: LoadingController,
  		public alertCtrl: AlertController, public toastCtrl: ToastController, public storage: Storage) {

		events.subscribe('add_doctor_services', (services, time) => {
	    	this.doctor_services_change = true;
	  		this.doctor_services = services['data'];
	  		this.total_doctor_services = services['total_count'];
	  	});

	    this.getServiceType();
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad AddServicesPage');
	    this.storage.get("sess_user_login").then((val) => {
	      this.userProfile = val;
	      console.log('this.userProfile',this.userProfile.id);
	    });
  	}

  	getServiceType(){

	    this.doctorProvider.getServiceType().then(data => {
	    	console.log(data);
	    	if(data['error'] == 0){
	    		this.serviceType = data['data']['data'];
	    		this.total_service_type = data['data']['total_count'];
	    	}
	    });
  	}

	getServices(){
		if(this.serviceTypeId && this.hospitalInfo){

			this.serviceType.forEach((srv) => {
				if(this.serviceTypeId == srv.id){
					this.serviceTypeName = srv.name;
				}
			});

			let thisData = {
				'hospital_id': this.hospitalInfo.id,
				'service_type_id': this.serviceTypeId,
				'doctor_id': this.userProfile.id
			};

		    this.doctorProvider.getUserServices(thisData).then(data => {
		    	if(data['error'] == 0){
					this.doctor_services = data['services']['services'];
		    		this.total_doctor_services = data['services']['total_count'];
		    	}
		    });
		}

	}

	leavePage(){
		if(this.doctor_services_change){

		   	let alert = this.alertCtrl.create({
		        title: '',
		        message: 'Are you sure you want to leave the page without saving?',
		        cssClass: 'AcceptBookingAlert',
		        buttons: [{
		            text: 'Leave',
		            role: 'cancel',
		            handler: () => {
		                console.log('Leave clicked');
		    			this.navCtrl.pop();
		            }
		        },
		        {
		            text: 'Stay',
		            role: 'cancel',
		            handler: () => {
		                console.log('Stay clicked');
		            }
		        }]
		    });
		    alert.present();
		}else {

		    this.navCtrl.pop();
		}
	}

	gotoAddService(){
		if(this.serviceTypeId && this.hospitalInfo){
	        this.navCtrl.push(BrowseServicesPage, {
	            'message_type': "add_services",
	            'services' : this.doctor_services,
	            'service_type' : this.serviceTypeId,
	            'hospital_id' : this.hospitalInfo.id
	        });

      	}else{
			console.log('no service for service type');
      	}
	}

	saveServices(){

	    if(this.doctor_services_change){

	      	let loader = this.loadingCtrl.create();
	      	loader.present();

	      	let thisData = {
	        	'services' : this.doctor_services,
	        	'hospital_id' : this.hospitalInfo.id,
				'service_type_id': this.serviceTypeId
	      	}

	      	if(this.total_doctor_services == 0){
	      		thisData['remove_all_service'] = true;
	      	}

	      	this.doctorProvider.editDoctorServices(thisData).then(res => {
	          	if(res['error'] === 0) {
	          		this.events.publish('added_doctor_services', true, Date.now());
                  	let alert = this.alertCtrl.create({
                      	cssClass: 'AcceptBookingAlert',
                      	buttons: [{
                        	text: 'Okay',
                        	role: 'cancel',
                        	handler: () => {
                            	this.navCtrl.pop();
                            	console.log('Cancel clicked');
                        	}
                      	}]
                  	}); 

                    alert.setTitle('Save Services');
                    alert.setMessage('Your services for <strong>'+ this.hospitalInfo.name +'</strong> has been save successfully');
                  	alert.present();

	          	} else {
	              	this.presentToast("Something went wrong. Please try again.");
	          	}
	        	loader.dismiss();
	      	});
	  	}
	}

  	presentToast(msg) {
	    this.toastCtrl.create({
	        message: msg,
	        duration: 2000,
	        position: "top"
	    }).present();
  	}	
}

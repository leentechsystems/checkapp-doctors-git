import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ForgotPasswordPage } from '../../pages/forgot-password/forgot-password';
import { RegistrationPage } from '../../pages/registration/registration';
import { TabsPage } from '../../pages/tabs/tabs';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';
import { Network } from '@ionic-native/network';
import { FCM } from '@ionic-native/fcm';
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook } from '@ionic-native/facebook';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  	selector: 'page-login',
  	templateUrl: 'login.html',
})
export class LoginPage {

	public loginForm: FormGroup;
	passwordType = "password";
	passwordIcon = "ios-eye";
	passwordStatus = "hide";
	is_submit: boolean = false;
	deviceToken: any;

	users: any;
  	profile_url : any;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
				private storage: Storage, public toastCtrl: ToastController, public siteProvider: SiteProvider,
				public userProvider: UserProvider, private network: Network, public loadingCtrl: LoadingController,
			 	public fcm: FCM, public googlePlus: GooglePlus, public facebook: Facebook) {

	  	this.loginForm = formBuilder.group({
		  	email: ['', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
		  	password: ['', Validators.compose([Validators.minLength(10), Validators.required])],
			request_type: ['doctor_app', Validators.required],
	  	});
  	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad LoginPage');
		// this.fcm.getToken().then(token => {
		// 	if(token){
		//   		this.deviceToken = token;
		// 		this.storage.set("sess_device_token", token).then((token) => {
		// 			console.log('device_token',token);
		// 		});
		// 	}
		// })
		//this.deviceToken = 1234;
  	}


  	ionViewDidEnter() {
	  	this.network.onConnect().subscribe(data => {
			console.log(data);
			this.displayNetworkUpdate(data.type);
	  	}, error => console.error(error));

	 	this.network.onDisconnect().subscribe(data => {
			console.log(data);
			this.displayNetworkUpdate(data.type);
	  	}, error => console.error(error));
  	}

  	displayNetworkUpdate(connectionState: string) {
	  	if(connectionState === "offline") {
		  	this.toastCtrl.create({
			  	message: "You are offline",
			  	duration: 3000,
			  	position: "top"
		  	}).present();

	  	} else {
		  	this.toastCtrl.create({
			  	message: "You are now connected",
			  	position: "top",
			  	duration: 3000,
			  	cssClass: "success"
		  	}).present();
	  	}
  	}

  	togglePassword() {
		if(this.passwordStatus == "hide") {
	  		this.passwordStatus = "show";
	  		this.passwordType = "text";
	  		this.passwordIcon = "ios-eye-off-outline";
		} else {
	  		this.passwordStatus = "hide";
	  		this.passwordType = "password";
	  		this.passwordIcon = "ios-eye";
		}
 	}

 	goToRegistration() {
  		this.navCtrl.push(RegistrationPage);
 	}

 	goToForgotPassword() {
 	 	this.navCtrl.push(ForgotPasswordPage);
 	}

 	loginNow() {
	  	this.is_submit = true;
	  	if(this.loginForm.valid) {
		  	let loader = this.loadingCtrl.create();
		  	loader.present();

		  	this.siteProvider.getGuestAccessToken().then(data => {
				this.loginForm.value['request_type'] = 'doctor_app';
				this.loginForm.value['token'] = this.deviceToken;
				this.userProvider.loginUser(this.loginForm.value, data).then((res) => {
				  	if(res['error'] === 1) {
						loader.dismiss();
						this.presentToast(res['message']);
				 	} else {
						this.storage.set("sess_user_login", res['user']).then((log) => {
					  		this.storage.set("sess_user_token", res['token']).then((token) => {
						 		loader.dismiss();
						 		this.storage.remove("sess_forgot_pass_email");
						 		this.navCtrl.setRoot(TabsPage);
					  		});
						}, err => {
						  	loader.dismiss();
						  	this.presentToast(res['message']);
						});
				  	}
				});
		  	});
	  	}
 	}

  	presentToast(msg) {
	  	this.toastCtrl.create({
		  	message: msg,
		  	duration: 2000,
		  	position: "top"
	  	}).present();

	}

	loginFacebook(){

		this.facebook.login(['public_profile', 'email'])
			.then(res => {
			    if(res.status === "connected") {
			        this.getUserDetail(res.authResponse.userID);

			   	} else {
			       	console.log("etoa res una",res);
			   	}
			})
	    .catch(err => {
	    	console.log("There is an error " + JSON.stringify(err));
			this.presentToast('Failed to connect in Facebook. Network is unreachable');
	    });
	}

	getUserDetail(userid) {

	  	this.facebook.api("/"+userid+"/?fields=id,email,name,first_name,last_name,picture.width(720).height(720),gender",["public_profile"])
	    	.then(res => {
	      		console.log("etoaaa profile",res);
	      		this.users = res;

	        	console.log("etoaaa",this.users);

		    	var userData = {
			        'firstname': res.first_name,
			        'lastname': res.last_name,
			        'email': res.email,
			        //'profile_photo': res.picture.data.url,
			        'fb_user_id': res.id,
					'request_type': 'doctor_app',
					'token' : this.deviceToken,
		    	};

		    	let loader = this.loadingCtrl.create({
			        content: ""
			    });
			    loader.present();

				this.siteProvider.getGuestAccessToken().then(data => {
					console.log("entered here");

					this.userProvider.loginUser(userData, data).then((res1) => {
						console.log("continued here");
						loader.dismiss();

						if(res1['error'] === 1 && res1['message'] !== "Invalid request") {
							this.presentToast(res1['message']);

						} else {

						    console.log('facebook_photo',res.picture.data.url);
							this.storage.set("sess_user_token", res1['token']);
		      				this.users = res1['user'];

							if(res.picture.data.url){

							    this.profile_url = res.picture.data.url;
								let pp_pieces = this.profile_url.split("=");
								console.log('picture123',pp_pieces);
								let new_pp = {
								    "name": pp_pieces[pp_pieces.length - 1]+'.jpg',
								    "path": this.profile_url
								};

								let thisPhoto = [];
								thisPhoto.push(new_pp);
								this.uploadProfilePhoto(thisPhoto, res1['user']['id']);
							}

							this.storage.set("sess_user_login", this.users).then((log) => {
								this.storage.remove("sess_forgot_pass_email");
								this.navCtrl.setRoot(TabsPage);
							}, err => {
								loader.dismiss();
								this.presentToast(res1['message']);
							});
						}
					});
				});
	    	})

	    .catch(err => {
	    	console.log("There is an error " + JSON.stringify(err));
			this.presentToast('Failed to connect in Facebook. Network is unreachable');
	    });
	}

	loginGoogle(){

		this.googlePlus.login({}).then(res => {

			console.log("googlePlusProfile", res);
			let google_name = res.displayName.split(" ");

			var userData = {
			    'firstname': google_name[0],
			    'lastname': google_name[1],
			    'email': res.email,
			    'google_user_id': res.userId,
				'request_type': 'doctor_app',
				'token' : this.deviceToken,
				//'profile_photo': res.imageUrl,
				//'location': res.placesLived
			};

			console.log("google profile",userData);

			let loader = this.loadingCtrl.create({
			    content: ""
		    });
			loader.present();

			this.siteProvider.getGuestAccessToken().then(data => {
				console.log("entered here");

				this.userProvider.loginUser(userData, data).then((res1) => {
					console.log("continued here");
					loader.dismiss();

					console.log('google_photo', res.imageUrl);

					this.storage.set("sess_user_token", res1['token']);
	      			this.users = res1['user'];

					if(res1['error'] === 1 && res1['message'] !== "Invalid request") {
						this.presentToast(res1['message']);

					} else {

						if(res.imageUrl){

							this.profile_url = res.imageUrl;
							let pp_pieces = this.profile_url.split("/");
							console.log('picture123',pp_pieces);
							let new_pp = {
								"name": pp_pieces[pp_pieces.length - 1]+'.jpg',
								"path": this.profile_url
							};

							let thisPhoto = [];
							thisPhoto.push(new_pp);
							this.uploadProfilePhoto(thisPhoto, res1['user']['id']);
						}

						this.storage.set("sess_user_login", res1['user']).then((log) => {
							this.storage.remove("sess_forgot_pass_email");
							this.navCtrl.setRoot(TabsPage);
						}, err => {
							this.presentToast(res1['message']);
						});
					}
				});
			});
	    })

	    .catch(err => {
	    	console.log("There is an error " + JSON.stringify(err));
			this.presentToast('Failed to connect in GooglePlus. Network is unreachable');
	    });
	}

	uploadProfilePhoto(thisPhoto, userid){
		console.log(userid);
		this.siteProvider.downloadPhoto(thisPhoto).then(res1 => {
			console.log(res1);
	        if(res1['error'] === 0) {
				this.siteProvider.uploadPhoto(res1['items']).then(res => {
					console.log(res);
		            if(res['error'] === 0) {
		                let attachments = [];
		                res['items'].forEach((items, iidx) => {
		                    attachments.push({"filename": items.files[0].filename, "filepath": items.files[0].filepath})
		               	});

		                let attachmentsData = {
		                   	"attachment_type": "user",
		                    "item_id": userid,
		                    "attachment": attachments
		                };

		                this.siteProvider.saveAttachment(attachmentsData).then(saveAttachmentResult => {
		                    if(saveAttachmentResult['error'] === 0) {
						    	this.users['attachments_id'] = saveAttachmentResult['attachments'][0]['id'];
						    	this.users['attachments_filename'] = saveAttachmentResult['attachments'][0]['filename'];
						    	this.users['attachments_filepath'] = saveAttachmentResult['attachments'][0]['filepath'];
						    	this.storage.set("sess_user_login", this.users);
		                        console.log(this.users);
		                        console.log("saveAttachmentResult", saveAttachmentResult);
		                    }
		                });
		            }
		        });
			}
		});
	}
}

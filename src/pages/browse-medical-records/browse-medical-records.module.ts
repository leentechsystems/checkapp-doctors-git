import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseMedicalRecordsPage } from './browse-medical-records';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    BrowseMedicalRecordsPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(BrowseMedicalRecordsPage),
  ],
})
export class BrowseMedicalRecordsPageModule {}

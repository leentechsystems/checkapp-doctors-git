import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ToastController, Content, ModalController } from 'ionic-angular';

import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';
import { AppointmentsPage } from '../../pages/appointments/appointments';
import { TabsPage } from './../tabs/tabs';
import { AppointmentsScheduledPage } from './../appointments-scheduled/appointments-scheduled';
import { AppointmentsApprovalPage } from './../appointments-approval/appointments-approval';
import { ProfileUserPage } from './../profile-user/profile-user';
import { FCM } from '@ionic-native/fcm';

/**
 * Generated class for the NotificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-notification',
	templateUrl: 'notification.html',
})
export class NotificationPage {
	@ViewChild(Content) content: Content;

	notifications: any;
	current_page = 1;
	total_notifications = 0;
	new_notification: any;
	scrollAmount = 0;

	constructor(public navCtrl: NavController, public navParams: NavParams, public userProvider: UserProvider, public siteProvider: SiteProvider,
				public events: Events, public fcm: FCM, public toastCtrl: ToastController, public modalCtrl: ModalController) {

        // events.subscribe('total_unread_notification:created', (action, time) => {
        //   console.log(action);
        //   this.ionViewDidEnter();
        // });

	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad NotificationPage');
	}

    ionViewDidEnter() {
        this.notifications = null;
        this.current_page = 1;
        this.getNotification(false, true, false);
    }

    reloadData(refresher?){
      	this.notifications = null;
      	this.current_page = 1;
      	this.getNotification(refresher, true, false);
    }

	getNotification(refresher?, isLoadNew?, infiniteScroll?){
		let thisData = {
			"current_page": this.current_page
		};

		this.userProvider.getNotification(isLoadNew, thisData).then(data => {
			if(data['error'] === 0) {
				this.total_notifications = data['result']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					if(!this.notifications || isLoadNew === true) {
						this.notifications = [];
					}
				}

				data['result']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.notifications.push(cat);
					}
				});

				if(infiniteScroll) {
					infiniteScroll.complete();
				}
			}

        	if(refresher) {
          		refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
        	}
		});
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		if(this.notifications.length < this.total_notifications) {
			this.getNotification(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}

	gotoAppointmentCalendar(this_index, notification_id, this_status, temp_notification) {
		console.log('sdada');
		console.log('temp_notification',temp_notification);
		if(this_status == '2'){
			let thisData = {
				"id": notification_id,
				"status": '1'
			};

			this.userProvider.changeNotificationStatus(thisData).then(data => {
				if(data['error'] == 0){
					if(data['message'] == 'Success.'){
						this.events.publish('total_unread_notification:created', 'summation', Date.now());
					}

					if(this.notifications){
						this.notifications.forEach((pGig, idx) => {
							if(idx == this_index){
								pGig['status'] = '1';
							}
						});
					}
				}
			});
		}

		if(temp_notification['type'] == 'hmo_transactions'){
			if(temp_notification['transaction']['appointment_appointment_date']){
				let wordDate = temp_notification['transaction']['appointment_appointment_date'].split(" ");

				if(temp_notification['attachment']){
					temp_notification['transaction']['patient_user_profile_attachments_filename'] = temp_notification['attachment']['filename'];
					temp_notification['transaction']['patient_user_profile_attachments_filepath'] = temp_notification['attachment']['filepath'];
				}

				if(temp_notification['action'] == 'set_appointment' || temp_notification['action'] == 'approve_appointment' || temp_notification['action'] == 'arrived' || temp_notification['action'] == 'complete' ){
					if((temp_notification['transaction']['status'] == '1' || temp_notification['transaction']['status'] == '4' || temp_notification['transaction']['status'] == '3') && (temp_notification['transaction']['appointment_status'] == '1' || temp_notification['transaction']['appointment_status'] == '4' || temp_notification['transaction']['appointment_status'] == '3')){

						let doneModal = this.modalCtrl.create(AppointmentsScheduledPage,{
							"temp_appointment": temp_notification['transaction'],
							"temp_date": wordDate[0],
							"temp_hospital_id": temp_notification['transaction']['hospital_id'],
							"from_notif": 'true'
						});

						doneModal.onDidDismiss(data => {
							if(data){

							}
						});

						doneModal.present();

					}else if((temp_notification['transaction']['status'] == '2' || temp_notification['transaction']['status'] == '0') && (temp_notification['transaction']['appointment_status'] == '4' || temp_notification['transaction']['appointment_status'] == '2'  || temp_notification['transaction']['appointment_status'] == '0' || temp_notification['transaction']['appointment_status'] == '1')){
						let doneModal = this.modalCtrl.create(AppointmentsApprovalPage,{
							"temp_appointment": temp_notification['transaction'],
							"temp_date": wordDate[0],
							"temp_hospital_id": temp_notification['transaction']['hospital_id'],
							"from_notif": 'true'
						});

						doneModal.onDidDismiss(data => {
							temp_notification['transaction'] = temp_notification['transaction'];
						});

						doneModal.present();
					}
				}
			}
		}else if(temp_notification['type'] == 'user'){
			if(temp_notification['action'] == 'prc_verified'){

				let doneModal = this.modalCtrl.create(ProfileUserPage);

				doneModal.onDidDismiss(data => {

				});

				doneModal.present();
			}
		}
	}
}

import { HmoProvider } from './../../providers/hmo/hmo';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { PatientsPrescriptionsPage } from './../patients-prescriptions/patients-prescriptions';


import { IllnessProvider } from '../../providers/illness/illness';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { normalizeURL } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { IonicSelectableComponent } from "ionic-selectable";

/**
 * Generated class for the PatientsDiagnosticsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-patients-diagnostics",
  templateUrl: "patients-diagnostics.html"
})
export class PatientsDiagnosticsPage {
  ilnessList: any;
  res: any;
  imageURI: Array<any> = [];
  diagnosis: any = 0;
  remarks: string = "";
  noDiagnosis: boolean = false;
  noRemarks: boolean = false;
  noAttachments: boolean = false;
  transactionID: any;
  d: any;
  patient_schedule = this.navParams.get("patient_schedule");

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public illness: IllnessProvider,
    public transfer: FileTransfer,
    public camera: Camera,
    public imagePicker: ImagePicker,
    public storage: Storage,
    public hmo: HmoProvider,
    public loader: LoadingController
  ) {
    this.transactionID = navParams.get("transaction_id");
    console.log("transaction_id", this.transactionID);
    console.log(this.patient_schedule);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PatientsDiagnosticsPage");
    this.getAllIllness();
  }

  gotoPrescriptions() {
    // if(this.diagnosis == 0) {
    //   this.noDiagnosis = true;
    // } else {
    //   this.noDiagnosis = false;
    // }
    // if(this.remarks == '') {
    //   this.noRemarks = true;
    // } else {
    //   this.noRemarks = false;
    // }
    // if(this.imageURI.length == 0) {
    //   this.noAttachments = true;
    // } else {
    //   this.noAttachments = false;
    // }
    // if(this.diagnosis != 0 && this.remarks != '' && this.imageURI.length > 0) {
    let params = {
      transaction_id: this.transactionID,
      patient_schedule: this.patient_schedule
    };
    this.storage.set("diagnosis", {
      diagnosis: this.diagnosis.id,
      remarks: this.remarks,
      attachments: this.imageURI,
      type: "diagnosis"
    });
    this.navCtrl.push(PatientsPrescriptionsPage, params);

    // }
  }

  diagnoseChange(event: {
    component: IonicSelectableComponent;
    value: any;
  }) {
    console.log(this.diagnosis);
   }

  getAllIllness() {
    this.illness.getAllIllnes().then(data => {
      this.res = data;
      this.ilnessList = this.res.cmspage;
      console.log(this.ilnessList);
    });
  }
  chooseImage() {
    // const options: CameraOptions = {
    //   quality: 100,
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //   correctOrientation: true,
    //   mediaType: this.camera.MediaType.PICTURE,
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   this.imageURI.push(imageData);
    //   console.log(this.imageURI);
    // },
    // (err) => {
    //   console.log(err);
    // }).then((path) => {

    // });
    let max;
    if (this.imageURI.length == 0) {
      max = 3;
    } else {
      let len = this.imageURI.length;
      max = 3 - len;
    }
    const options: ImagePickerOptions = {
      quality: 100,
      maximumImagesCount: max
    };
    this.imagePicker.getPictures(options).then(
      results => {
        // this.imageURI = results;

        if (results !== "OK") {
          for (var i = 0; i < results.length; i++) {
            // console.log('Image URI: ' + results[i]);
            this.imageURI.push({
              path: results[i],
              name: results[i].substr(results[i].lastIndexOf("/") + 1)
            });
          }
        }
        console.log(this.imageURI);
      },
      err => {}
    );
  }

  removeImage(i) {
    this.imageURI.splice(i, 1);
  }
  padLeft(nr, n, str = "") {
    return Array(n - String(nr).length + 1).join(str || "0") + nr;
  }
}

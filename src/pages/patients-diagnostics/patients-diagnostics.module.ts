import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsDiagnosticsPage } from './patients-diagnostics';
import { IonicSelectableModule } from "ionic-selectable";

@NgModule({
  declarations: [
    PatientsDiagnosticsPage,
  ],
  imports: [
    IonicSelectableModule,
    IonicPageModule.forChild(PatientsDiagnosticsPage),
  ],
})
export class PatientsDiagnosticsPageModule {}

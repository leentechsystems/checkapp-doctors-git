import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileDoctorPage } from './profile-doctor';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    ProfileDoctorPage,
  ],
  imports: [
    PipesModule,
    IonicPageModule.forChild(ProfileDoctorPage),
    IonicImageLoader,
  ],
})
export class ProfileDoctorPageModule {}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, ModalController, AlertController, LoadingController, NavParams, Platform } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { DoctorProvider } from '../../providers/doctor/doctor';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { SiteProvider } from '../../providers/site/site';
import { PromptModalPage } from '../../pages/prompt-modal/prompt-modal';
import { AddServicesPage } from '../../pages/add-services/add-services';

import { GoogleMap, GoogleMaps } from '@ionic-native/google-maps';

/**
 * Generated class for the ProfileDoctorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 declare var google;

@IonicPage()
@Component({
	selector: 'page-profile-doctor',
	templateUrl: 'profile-doctor.html',
})
export class ProfileDoctorPage {

	hospital_tabs: string = '';
	hospital_id: any;
	doc_id: any;
	all_scheds: any;
	hospital_location: any;
	hospital_services: any;
	total_hospital_services = 0;
	total_count = 0;
	days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
	storeScheds: any;
	total_storeScheds = 0;
	current_page = 1;
	marker: any;
	contactNumber: any;
	contacts: any;
	selected_service: any;
	map_style = "";

	x = 0;

	doctorProfile = this.navParams.get('doctorProfile');
	userProfile: any;

	@ViewChild('map') mapElement: ElementRef;
	map: GoogleMap;

	main_hospital: any;
	more_hospital = [];
	hospitals: any;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public doctorProvider: DoctorProvider,
				private storage: Storage,
				public modalCtrl: ModalController,
				public alertCtrl: AlertController,
				public siteProvider: SiteProvider,
				public loadingCtrl: LoadingController,
				public platform: Platform,
				private googleMaps: GoogleMaps,
				public hospitalsProvider: HospitalsProvider) {

		if (this.hospital_tabs == ''){
			this.hospital_tabs = 'hospital';
			this.map_style = "display: block;";
		}

		this.doc_id = this.doctorProfile['user_id'];
		this.hospital_id = this.doctorProfile['hospital_doctors_hospital_id'];
		this.hospitals = this.doctorProfile['hospital'];

		let no_main = 'no';
		this.hospitals.forEach((hosp, idx) => {
			if(hosp){
				if(hosp['main'] == '1'){
					no_main = 'yes';
					this.main_hospital = hosp;
					this.hospital_id = hosp['hospital_id'];
					this.more_hospital.push(hosp);
				}else{
					if(hosp['hospital_id'] != '0'){
						this.more_hospital.push(hosp);
					}
				}
			}
		});

		if(no_main == 'no'){
			this.main_hospital = [];
			this.more_hospital = [];
			this.hospitals.forEach((hosp, idx) => {
				if(idx == 0){
					this.main_hospital = hosp;
					if(this.hospitals.length != 1){
						this.more_hospital.push(hosp);
					}
				}else{
					this.more_hospital.push(hosp);
				}
			});
		}

		console.log('this.doctorProfile',this.doctorProfile);

		this.loadAllData(false, true, false);
		this.getHospitalServices();
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProfileDoctorPage');
		if(this.main_hospital.hospital_latitude && this.main_hospital.hospital_longtitude) {
			 this.initializeMap();
		}
		this.storage.get("sess_user_login").then((val) => {
			this.userProfile = val;
			console.log('this.userProfile',this.userProfile.id);
		});
	}

	reloadData(){
		this.storeScheds = null;
		this.total_storeScheds = 0;
		this.loadAllData(false, true, false);
		this.getHospitalServices();

		console.log('ionViewDidLoad ProfileDoctorPage');
		if(this.main_hospital.hospital_latitude && this.main_hospital.hospital_longtitude) {
			this.initializeMap();
		}
	}


 	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {

		let thisData = {
			"hospital_id": this.hospital_id,
			"doc_id": this.doc_id,
			"selected_day": ""
		};

		 this.doctorProvider.getSchedules(isLoadNew, thisData).then(data => {
			this.storeScheds = [];
			if(data['error'] === 0) {
				this.total_storeScheds = data['total_count'];
				this.all_scheds = data['timeslot'];
				console.log(this.all_scheds);
				if(this.all_scheds){
					this.days.forEach((days, didx) => {
						if(this.total_storeScheds > 0) {
							if(!this.storeScheds) {
								this.storeScheds = [];
							}
							if(this.all_scheds[days] !== undefined){
								let newSched = {
									"day" : days,
									"timeslots" : this.all_scheds[days]
								};
								this.storeScheds.push(newSched);
							}
							this.x++;
						}
					});
					console.log('this.storeScheds',this.storeScheds);
				}
			}

			if(refresher) {
				refresher.complete();
			}
		});
	}

	initializeMap() {
		let element = this.mapElement.nativeElement;
		this.map = new google.maps.Map(this.mapElement.nativeElement, {
			zoom: 12,
			center: {lat: parseFloat(this.main_hospital['hospital_latitude']), lng: parseFloat(this.main_hospital['hospital_longtitude'])},
			mapTypeControl: false,
			streetViewControl: false
		});

		this.marker = new google.maps.Marker({
			map: this.map,
			animation: google.maps.Animation.DROP,
			position: {lat: parseFloat(this.main_hospital['hospital_latitude']), lng: parseFloat(this.main_hospital['hospital_longtitude'])},
		});
	}

	getHospitalServices() {

		let thisData = {
			'hospital_id': this.hospital_id,
			'doctor_id': this.doctorProfile['user_id']
		};

		this.doctorProvider.getUserServices(thisData).then(services => {
			if(services['error'] === 0) {
				this.total_hospital_services = services['services']['service_count'];
				this.hospital_services = services['services']['data'];
				this.total_count = services['services']['total_count'];
				console.log(this.hospital_services);
			}
		}, err => {
			console.log("getServices", err);
		});
	}

	changeMapVisibility() {
		if(this.hospital_tabs === 'hospital') {
			this.map_style = "display: block;";
		} else {
			this.map_style = "display: none;";
		}
	}

	viewHospital(hospital, thisIndex){

		console.log(hospital);
		let loading = this.loadingCtrl.create({
			spinner: 'crescent',
			duration: 1000
		});

		this.doctorProfile['hospital_address'] = hospital.hospital_address;
		this.doctorProfile['hospital_address_id'] = hospital.hospital_address_id;
		this.doctorProfile['hospital_attachments_filename'] = hospital.hospital_attachments_filename;
		this.doctorProfile['hospital_attachments_filepath'] = hospital.hospital_attachments_filepath;
		this.doctorProfile['hospital_city'] = hospital.hospital_city;
		this.doctorProfile['hospital_country'] = hospital.hospital_country;
		this.doctorProfile['hospital_description'] = hospital.hospital_description;
		this.doctorProfile['hospital_doctors_hospital_id'] = hospital.hospital_id;
		this.doctorProfile['hospital_doctors_main'] = hospital.main;
		this.doctorProfile['hospital_email'] = hospital.hospital_email;
		this.doctorProfile['hospital_latitude'] = hospital.hospital_latitude;
		this.doctorProfile['hospital_longtitude'] = hospital.hospital_longtitude;
		this.doctorProfile['hospital_name'] = hospital.hospital_name;
		this.doctorProfile['hospital_region'] = hospital.hospital_region;
		this.doctorProfile['hospital_zip'] = hospital.hospital_zip;

		loading.onDidDismiss(() => {
			this.hospital_id = hospital.hospital_id;
			this.main_hospital = hospital;
			this.reloadData();
		});

		loading.present();

	}

	seeAllServices(serviceKey, services){
		let doneModal = this.modalCtrl.create(PromptModalPage, {
			message_type: "see_all_services",
			service_type: serviceKey,
			services: services,
		});
		doneModal.onDidDismiss(data => {

		});

		doneModal.present();
	}

	addSevices(){
		if(this.main_hospital){
			this.main_hospital['id'] = this.main_hospital['hospital_id'];
			this.main_hospital['name'] = this.main_hospital['hospital_name'];
			this.navCtrl.push(AddServicesPage, {
				'hospital': this.main_hospital
			});
		}
	}
}

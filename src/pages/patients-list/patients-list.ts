import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PatientsProfilePage } from './../patients-profile/patients-profile';
import { FormFilterPatientsPage } from './../form-filter-patients/form-filter-patients';
import { ModalController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the PatientsListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patients-list',
  templateUrl: 'patients-list.html',
})
export class PatientsListPage {

    patients: any;
    total_patients: 0;
    searched_keyword: any;
    current_page = 1;
    applied_filters: any;
    searched_submit: any;
    search_hide = false;
    temp_patient_appointment: any;


    constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
                public userProvider: UserProvider, public siteProvider: SiteProvider) {
    
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad PatientsListPage');
    }

    ionViewWillEnter(){
        this.applied_filters= null;
        this.patients = null;
        this.current_page = 1;
        this.loadAllData(false, true, false);
    }

    reloadData(refresher?){
        this.patients = null;
        this.current_page = 1;
        this.loadAllData(refresher, true, false);
    }

    loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
    
        console.log(this.current_page);

        let thisData = {
            "action": 'patient',
            "searched_keyword": this.searched_keyword,
            "current_page": this.current_page,
            "applied_filters": this.applied_filters
        };

        if(this.searched_submit == false){ 
            this.searched_submit = false;
        }else{
            this.searched_submit = true;
        }

        this.userProvider.getPreviousPatient(isLoadNew, thisData).then(data => {
            if(data['error'] == 0){
                this.total_patients = data['result']['total_count'];

                if(!infiniteScroll && infiniteScroll === false) {
                    if(!this.patients || isLoadNew === true) {
                        this.patients = []; 
                    }
                }

                data['result']['items'].forEach((cat, idx) => {
                    if(cat.id !== undefined) {
                        this.patients.push(cat);
                    }
                });
                
                if(infiniteScroll) {
                    infiniteScroll.complete();
                }
            }

            if(refresher) {
                refresher.complete();
            }
        });
    }
  
    doInfinite(infiniteScroll:any) {
        this.current_page += 1;
        if(this.patients.length < this.total_patients) {
            this.loadAllData(false, false, infiniteScroll);
        } else {
            infiniteScroll.enable(false);
        }
    }

    gotoPatientProfile(temp_patient_profile){
        console.log('temp_patient_profile',temp_patient_profile);
        this.userProvider.getPatientProfile(temp_patient_profile['user_id']).then((data) => {
            console.log('data',data);
            if(data['error'] == 0){
                temp_patient_profile = data['user'];
            }

            this.patients.forEach((cat, idx) => {
                if(cat.user_id == temp_patient_profile['user_id']) {
                    cat['hmo_transactions_status'] = cat['appointment_status'];
                    this.temp_patient_appointment = cat;
                }
            });

            this.navCtrl.push(PatientsProfilePage,{
                "temp_patient_profile": temp_patient_profile,
                "temp_patient_appointment" : this.temp_patient_appointment
            });

        }, err => {

        });

    }

    gotofilter(){
        let filterModal = this.modalCtrl.create(FormFilterPatientsPage,{
            filter_type: 'patients',
            data: this.applied_filters
        });

        filterModal.onDidDismiss(data => { 
            if(data){
                console.log(data);
                this.patients = null;
                this.current_page = 1;
                if(data == 'reset'){
                    this.applied_filters = null;
                    this.loadAllData(false,true,false);
                }else{
                    this.applied_filters = {
                        range: data.range,
                        //gender: data.gender
                    };
                    this.loadAllData(false,true,false);
                }
            }
        });

        filterModal.present();
    }  

    searchPatientss(onCancel:any) {
        if(onCancel) {
            this.searched_keyword = "";
        }

        this.searched_submit = false;
        this.search_hide = true;
        this.patients = null;      
        this.loadAllData(false, true, false); 
    } 
}

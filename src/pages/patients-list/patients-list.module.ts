import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsListPage } from './patients-list';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    PatientsListPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(PatientsListPage),
    IonicImageLoader,
  ],
})
export class PatientsListPageModule {}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsPrescriptionsPage } from './patients-prescriptions';

@NgModule({
  declarations: [
    PatientsPrescriptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(PatientsPrescriptionsPage),
  ],
})
export class PatientsPrescriptionsPageModule {}

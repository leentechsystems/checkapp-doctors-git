import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

import { PatientsHmoreportPage } from './../patients-hmoreport/patients-hmoreport';

import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { normalizeURL } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HmoProvider } from '../../providers/hmo/hmo';
/**
 * Generated class for the PatientsPrescriptionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-patients-prescriptions',
  templateUrl: 'patients-prescriptions.html',
})
export class PatientsPrescriptionsPage {
  res :any;
  imageURI : Array<any> = [];
  findings: any;
  remarks: string = '';
  noFindings: boolean = false;
  noRemarks: boolean = false;
  noAttachments: boolean = false;
  transactionID: any;
  diagnosis: any;
  d: any;
  patient_schedule = this.navParams.get('patient_schedule');

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public transfer: FileTransfer,
    public camera: Camera,
    public imagePicker: ImagePicker,
    public storage: Storage,
    public hmo: HmoProvider,
    public loader: LoadingController,
  ) {
    this.transactionID = navParams.get('transaction_id');
    this.diagnosis = navParams.get('diagnosis');
    console.log("this.diagnosis", this.diagnosis);
    console.log(this.patient_schedule);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PatientsPrescriptionsPage');
  }

  gotoHMOReport(){
    // if(this.findings == 0) {
    //   this.noFindings = true;
    // } else {
    //   this.noFindings = false;
    // }
    // if(this.remarks == '') {
    //   this.noRemarks = true;
    // } else {
    //   this.noRemarks = false;
    // }
    // if(this.imageURI.length == 0) {
    //   this.noAttachments = true;
    // } else {
    //   this.noAttachments = false;
    // }
    // if(this.findings != 0 && this.remarks != '' && this.imageURI.length > 0) {
      this.storage.set('findings', {
        'findings': this.findings,
        'remarks': this.remarks,
        'attachments': this.imageURI,
        'type': 'findings'
      });
      let params = { 'transaction_id': this.transactionID,
                     'patient_schedule': this.patient_schedule
       };
      this.navCtrl.push(PatientsHmoreportPage, params);
       
    // }
  }

  chooseImage() {
    // const options: CameraOptions = {
    //   quality: 100,
    //   destinationType: this.camera.DestinationType.FILE_URI,
    //   sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    //   correctOrientation: true,
    //   mediaType: this.camera.MediaType.PICTURE,
    // }
    // this.camera.getPicture(options).then((imageData) => {
    //   this.imageURI.push(imageData);
    //   console.log(this.imageURI);
    // },
    // (err) => {
    //   console.log(err);
    // }).then((path) => {

    // });
    let max;
    if(this.imageURI.length == 0) {
      max = 3
    } else {
      let len = this.imageURI.length;
      max = 3 - len;
    }
    const options: ImagePickerOptions = {
        quality: 100,
        maximumImagesCount: max,
        
      }
    this.imagePicker.getPictures(options).then((results) => {
      // this.imageURI = results;

      if(results !== 'OK'){
        for (var i = 0; i < results.length; i++) {
            // console.log('Image URI: ' + results[i]);
            this.imageURI.push({ 'path': results[i], 'name': results[i].substr(results[i].lastIndexOf('/') + 1)});
        }
        console.log(this.imageURI);
      }
    }, (err) => { });
  }

  removeImage(i) {
    this.imageURI.splice(i, 1);
  }
  
  padLeft(nr, n, str=''){
    return Array(n-String(nr).length+1).join(str||'0')+nr;
  }
  

}

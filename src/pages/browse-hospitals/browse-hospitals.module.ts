import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseHospitalsPage } from './browse-hospitals';
import { LongPressModule } from 'ionic-long-press';

@NgModule({
  declarations: [
    BrowseHospitalsPage,
  ],
  imports: [
    LongPressModule,
    IonicPageModule.forChild(BrowseHospitalsPage),
  ],
})
export class BrowseHospitalsPageModule {}

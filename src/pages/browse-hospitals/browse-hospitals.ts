import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams, Events, ActionSheetController, AlertController, LoadingController } from 'ionic-angular';
import { ProfileHospitalsPage } from './../profile-hospitals/profile-hospitals';
import { FormFilterHospitalsPage } from './../form-filter-hospitals/form-filter-hospitals';

import { Storage } from '@ionic/storage';

import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { AddHospitalPage } from '../../pages/add-hospital/add-hospital';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the BrowseHospitalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse-hospitals',
  templateUrl: 'browse-hospitals.html',
})
export class BrowseHospitalsPage {

  accessToken:any = "";
  all_hospitals: any;
  current_page = 1;
  total_count = 0;
  searched_keyword = (this.navParams.get('searched_keyword')) ? this.navParams.get('searched_keyword') : "";
  page_action = (this.navParams.get('page_action')) ? this.navParams.get('page_action') : null;

  specialties: any;
  selected_specialties: any;
  applied_filters: any;
  current_location: any;

  userData: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController,
              public hospitalsProvider: HospitalsProvider, private storage: Storage, public doctorProvider: DoctorProvider,
              public events: Events, public userProvider: UserProvider, public actionSheetCtrl: ActionSheetController, 
              public alertCtrl: AlertController, public loadingCtrl: LoadingController) {
      
      this.storage.get("sess_current_location").then(sess_current_location => {
          if(sess_current_location) {
              this.current_location = sess_current_location;
          } 

          if(this.page_action){
            this.getDoctorsHospitals(false, true, false);
          }else{
            this.loadAllData(false, true, false);
          }
      }); 

      this.storage.get('sess_user_login').then((val) => {
          this.userData = val;
      });

      events.subscribe('add_hospital', (hospital_data, time) => {
        this.all_hospitals = null;
        this.current_page = 1;
        this.getDoctorsHospitals(false, true, false);
      });
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BrowseHospitalsPage');
  }

  gotoProfileHospital(hospital_profile) {
    this.navCtrl.push(ProfileHospitalsPage, {
      "hospital_profile": hospital_profile,
      "page_action": this.page_action
    });
  }

  openFilterForm() {
    let filtermodal = this.modalCtrl.create(FormFilterHospitalsPage, {
        "specialties": this.specialties
    });

    filtermodal.onDidDismiss(data => { 
      if(data) {
          this.applied_filters = data;
          if(this.applied_filters.city) {
            this.all_hospitals = null;
            this.current_page = 1;
          }
          // this.searched_keyword = this.applied_filters.city;
          this.loadAllData(false, true, false);
      }
    });

    filtermodal.present();
    
  }

  loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
      
      console.log(this.current_page);

      let origin_coordinates = {};

      if(this.current_location) {
        origin_coordinates = {
          "lat": this.current_location['lat'],
          "lng": this.current_location['lng'],
        };
      } else {
        origin_coordinates = {
          "lat": 0,
          "lng": 0,
        };
      }

      let thisData = {
        "current_page": this.current_page,
        "searched_keyword": this.searched_keyword,
        "applied_filters": this.applied_filters,
        "origin_coordinates": origin_coordinates
      };

       this.hospitalsProvider.getAllHospitals(isLoadNew, thisData).then(data => {
        if(data['error'] == 0){

          this.total_count = data['result']['total_count'];

          if(!infiniteScroll && infiniteScroll === false) {
            if(!this.all_hospitals || isLoadNew === true) {
                 this.all_hospitals = []; 
              }
          }

          data['result']['items'].forEach((cat, idx) => {
                if(cat.id !== undefined) {
                    this.all_hospitals.push(cat);
                }
          });

          if(infiniteScroll) {
            infiniteScroll.complete();
          }
        }

        if(refresher) {
          refresher.complete();
        }
    });
  }

  doInfinite(infiniteScroll:any) {
      this.current_page += 1;
      if(this.page_action){
      
        if(this.all_hospitals.length < this.total_count) {
            this.getDoctorsHospitals(false, false, infiniteScroll);
        } else {
            infiniteScroll.enable(false);
        }

      }else{

        if(this.all_hospitals.length < this.total_count) {
            this.loadAllData(false, false, infiniteScroll);
        } else {
            infiniteScroll.enable(false);
        }
      }
  }

  searchNews(onCancel:any) {
    if(onCancel) {
          this.searched_keyword = "";
      }
      this.all_hospitals = null;
      this.current_page = 1; 
      this.loadAllData(false, true, false); 
  }

  onTyping(){
    this.searched_keyword = "";
  }
  
  reloadData(refresher?){
      if(this.page_action){
        this.all_hospitals = null;
        this.current_page = 1;
        this.getDoctorsHospitals(refresher, true, false);
      }else{
        this.all_hospitals = null;
        this.current_page = 1;
        this.loadAllData(refresher, true, false);
      }
  }


  getDoctorsHospitals(refresher?, isLoadNew?, infiniteScroll?){
    this.doctorProvider.getDoctorsHospitalList().then(data => {
      console.log('data',data);
      if(data['error'] == 0){
        this.total_count = data['hospitals']['total_count'];

        if(!infiniteScroll && infiniteScroll === false) {
          if(!this.all_hospitals || isLoadNew === true) {
            this.all_hospitals = []; 
          }
        }

        data['hospitals']['items'].forEach((cat, idx) => {
              if(cat.id !== undefined) {
                  this.all_hospitals.push(cat);
              }
        });
        
        if(infiniteScroll) {
          infiniteScroll.complete();
        }
      }

      if(refresher) {
        refresher.complete();
      }

    });
  }

  gotoAddHospitals(){

    let addHospitalModal;

    if(this.page_action){
      console.log('total_count',this.total_count);
      addHospitalModal = this.modalCtrl.create(AddHospitalPage, {
          hospital_data: this.all_hospitals,
          my_total_hospital: this.total_count
      });

    }else{

      addHospitalModal = this.modalCtrl.create(AddHospitalPage, {
          hospital_data: this.all_hospitals,
      });
    }

    addHospitalModal.onDidDismiss(data => { 
      if(data) {

      }
    });

    addHospitalModal.present();

  }

  addtoBookmark(temp_hospital, hospital_index){
    console.log('hospital',temp_hospital);
    console.log('index',hospital_index);

    let thisData = {
      "item_id": temp_hospital.id,
      "type": 'hospital',
    };

    if(temp_hospital.bookmark_id){
      thisData['id'] = temp_hospital.bookmark_id;
    }

    this.all_hospitals.forEach((cat, idx) => {
      if(hospital_index == idx){
        if(temp_hospital.bookmark_id){
          cat.bookmark_id = null;
        }else{
          cat.bookmark_id = 1;
        }
      }
    });

    this.userProvider.saveBookmarks(thisData).then((data) => {

      if(data['error'] === 0){
        this.all_hospitals.forEach((cat, idx) => {
          if(hospital_index == idx){
            if(thisData['id']){
              cat.bookmark_id = null;
            }else{
              cat.bookmark_id = data['result']['id'];
            }
          }
        });
      }

    }, err => {

      console.log(err);

    });
  }

  onPressStart(hospital, index){
    console.log('onPressStart',hospital);
      
    let setMain = {
        text: 'Set as main hospital',
        icon: 'md-star',
        cssClass: 'actionSheetCtrlSetMainHospital',
        handler: () => { 

            let alert = this.alertCtrl.create({
                title: 'Set as Main Hospital',
                cssClass: 'AcceptBookingAlert',
                buttons: [{
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        console.log('Stay clicked');
                    }
                  },{
                    text: 'Yes',
                    role: 'cancel',
                    handler: () => {
                      console.log('Leave clicked');

                      let loader = this.loadingCtrl.create();
                      loader.present();

                      let thisData = {
                        "hospital_id": hospital.id
                      };

                      let old_main_hospital_index;
                      this.all_hospitals.forEach((data, idx) => {
                          if(data.is_main == 1) {
                            old_main_hospital_index = idx;
                          }
                      });

                      this.doctorProvider.setDoctorHospitalMain(thisData).then((data) => {
                        if(data['error'] == 0){
                          this.all_hospitals[old_main_hospital_index]['is_main'] = 0;
                          this.all_hospitals[index]['is_main'] = 1;
                        }
                        loader.dismissAll();
                      });
                    }
                }]
            });
            alert.setMessage('Are you sure you want to set <strong>'+hospital.name+'<strong> as your main hospital?');
            alert.present();
        }
    };

    let buttons = [{
          text: 'Remove',
          icon: 'md-trash',
          cssClass: 'actionSheetCtrlDelete',
          handler: () => { 

            let alert = this.alertCtrl.create({
                title: 'Remove Hospital',
                cssClass: 'AcceptBookingAlert',
                buttons: [{
                    text: 'No',
                    role: 'cancel',
                    handler: () => {
                        console.log('Stay clicked');
                    }
                },{
                    text: 'Yes',
                    role: 'cancel',
                    handler: () => {
                      console.log('Leave clicked');

                      let loader = this.loadingCtrl.create();
                      loader.present();

                      let thisData = {
                        "hospital_id": hospital.id,
                        "user_id": this.userData['id'],
                      };

                      this.doctorProvider.removeDoctorsHospital(thisData).then((data) => {
                        if(data['error'] == 0){
                          this.all_hospitals.splice(index, 1);
                          this.total_count--;
                          this.events.publish('refresh-schedule', 'true', Date.now());
                        }
                        loader.dismissAll();
                      });
                    }
                }]
            });
            alert.setMessage('Are you sure you want to remove <strong>'+ hospital.name +'</strong> in your hospitals list?');
            alert.present();

          }
      },
      {
        text: 'Cancel',
        icon: 'md-close',
        role: 'cancel'
    }];

    if(hospital.is_main == 0){
      buttons.splice(0, 0, setMain);
    }

    if(this.page_action){
      let actionSheet = this.actionSheetCtrl.create({
        //title: 'Select Image Source',
        buttons: buttons
      });
      actionSheet.present();
    }
  }

}


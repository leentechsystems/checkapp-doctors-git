import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PatientsHmoreportPage } from './patients-hmoreport';
import { IonicSelectableModule } from "ionic-selectable";

@NgModule({
  declarations: [
    PatientsHmoreportPage,
  ],
  imports: [
    IonicSelectableModule,
    IonicPageModule.forChild(PatientsHmoreportPage),
  ],
})
export class PatientsHmoreportPageModule {}

import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { HomePage } from './../home/home';
import { Storage } from '@ionic/storage';
import { HmoProvider } from '../../providers/hmo/hmo';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { RequestOptions } from '@angular/http';
import { SiteProvider } from '../../providers/site/site';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import 'rxjs/add/observable/forkJoin';
import { PatientsTransactionSummaryPage } from '../patients-transaction-summary/patients-transaction-summary';
import { IonicSelectableComponent } from "ionic-selectable";

import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
/**
 * Generated class for the PatientsHmoreportPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-patients-hmoreport",
  templateUrl: "patients-hmoreport.html"
})
export class PatientsHmoreportPage {
  findings: any;
  remarks: any;
  noFindings: boolean = false;
  noRemarks: boolean = false;
  noAttachments: boolean = false;
  noRecommendation: Array<any> = [];
  recommendations: Array<any> = [];
  service_recommendations: Array<any> = [];
  transactionID: any;
  hmoTransactionID: any;
  r: any;
  reportDetails: any;
  diagnosisFiles: any;
  findingsFiles: any;
  diagattachment: Array<any> = [];
  fndgattachment: Array<any> = [];
  resdiag: any;
  resfndg: any;
  hmoForm: FormGroup;
  patient_schedule = this.navParams.get("patient_schedule");
  hospitalServices: any;
  is_submit: boolean = false;
recommendations_array: any;

  @ViewChild("recommendationComponent") recommendationComponent: IonicSelectableComponent;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public hmoService: HmoProvider,
    public appointment: AppointmentProvider,
    public hospitalsProvider: HospitalsProvider,
    public siteProvider: SiteProvider,
    public alert: AlertController,
    private form: FormBuilder
  ) {
    this.getServices();
    console.log(this.patient_schedule);
    this.transactionID = navParams.get("transaction_id");
    this.hmoForm = this.form.group({
      title: ["", Validators.required],
      remarks: ["", Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PatientsHmoreportPage");
  }

  gotoSummary() {
	this.is_submit = true;
	this.service_recommendations = [];
	this.recommendations_array = [];
	console.log(this.hmoForm.value);


	  if (this.hmoForm.valid && this.recommendations.length != 0) {
      this.findings = this.hmoForm.get("title").value;
	  this.remarks = this.hmoForm.get("remarks").value;
		  console.log(this.hospitalServices);

/*       this.recommendations.forEach((data, idx) => {
        this.hospitalServices.forEach((cat, idx) => {
          if (cat.id == data) {
			console.log(cat.title);
            this.service_recommendations.push(cat.title);
          }
        });
	  }); */


		this.recommendations.forEach((data, idx) => {
			this.service_recommendations.push(data.title);
			this.recommendations_array.push(data.id);
		});

      this.storage
        .set("hmoreport", {
          findings: this.findings,
          remarks: this.remarks,
			recommendations: this.recommendations_array,
			service_recommendations: this.service_recommendations,
          type: "hmoreport"
        })
        .then(
          () => {
            console.log("Item Stored");
            this.navCtrl.push(PatientsTransactionSummaryPage, {
              transaction_id: this.transactionID
            });
          },
          err => {
            console.error(err);
          }
        );
    }
  }

  padLeft(nr, n, str = "") {
    return Array(n - String(nr).length + 1).join(str || "0") + nr;
  }

  recommendationChange(event: {
    component: IonicSelectableComponent;
    value: any;
  }) { 
	  this.recommendations = event.value;
	  console.log(this.recommendations);
  }

  confirm() {
	  this.recommendationComponent.confirm();
	  this.recommendationComponent.close();
  }

  getServices() {
    this.hospitalsProvider.getHospitalServices().then(data => {
      this.hospitalServices = data["cmspage"];
      console.log(this.hospitalServices);
    });
  }
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormFilterDoctorsPage } from './form-filter-doctors';
import { IonicSelectableModule } from 'ionic-selectable';


@NgModule({
  declarations: [
    FormFilterDoctorsPage,
  ],
  imports: [
    IonicSelectableModule,
    IonicPageModule.forChild(FormFilterDoctorsPage),
  ],
})
export class FormFilterDoctorsPageModule {}

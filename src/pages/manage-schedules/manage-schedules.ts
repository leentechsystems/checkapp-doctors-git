import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, ActionSheetController, AlertController, LoadingController } from 'ionic-angular';

import { AddSchedulePage } from './../add-schedule/add-schedule';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { BrowseHospitalsPage } from './../browse-hospitals/browse-hospitals';

/**
 * Generated class for the ManageSchedulesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-manage-schedules',
  templateUrl: 'manage-schedules.html',
})
export class ManageSchedulesPage {


  finalSchedule = [];
  days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  storeScheds: any;
  storeSchedsCopy: any;
  storeScheds1: any;
  doctorSchedule = [];
  temp_schedule: any;
  temp_schedule_count = 0;
  sunday: any;
  monday: any;
  tuesday: any;
  wednesday: any;
  thursday: any;
  friday: any;
  saturday: any;
  sampleSchedule = [];
  sampleSchedule1 = [];
  test = [];
  test1 = [];
  x = 0;
  y = 0;
  sun_idx = 0;
  mon_idx = 0;
  tue_idx = 0;
  wed_idx = 0;
  thu_idx = 0;
  fri_idx = 0;
  sat_idx = 0;
  from_add_schedule: any;

  doctorsHospital: any;
  doctorsHospitalCount: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public doctorProvider: DoctorProvider, public events: Events,
              public modalCtrl: ModalController, public actionSheetCtrl: ActionSheetController,
              public alertCtrl: AlertController, public loadingCtrl: LoadingController) {

    events.subscribe('add_schedule', (add_schedule, time) => {
      console.log('add_schedule');

      this.temp_schedule = null;
      this.sunday = [];
      this.monday = [];
      this.tuesday = [];
      this.wednesday = [];
      this.thursday = [];
      this.friday = [];
      this.saturday = [];
      this.doctorSchedule = [];
      this.getDoctorsSchedules(false);
    });

    this.events.subscribe('refresh-schedule', (refresher, time) => {
      if(refresher){
        console.log(refresher);
        this.temp_schedule = null;
        this.sunday = [];
        this.monday = [];
        this.tuesday = [];
        this.wednesday = [];
        this.thursday = [];
        this.friday = [];
        this.saturday = [];
        this.doctorSchedule = [];
        this.getDoctorsSchedules(false);
      }
    });

    this.temp_schedule = null;
    this.sunday = [];
    this.monday = [];
    this.tuesday = [];
    this.wednesday = [];
    this.thursday = [];
    this.friday = [];
    this.saturday = [];
    this.doctorSchedule = [];
    this.getDoctorsSchedules(false);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManageSchedulesPage');
  }   

  ionViewDidEnter() {

  }

  reloadData(refresher?){
    this.sunday = [];
    this.monday = [];
    this.tuesday = [];
    this.wednesday = [];
    this.thursday = [];
    this.friday = [];
    this.saturday = [];
    this.doctorSchedule = [];

    this.getDoctorsSchedules(refresher);
  }

  gotoAddSchedules(temp_schedule){
    
    this.navCtrl.push(AddSchedulePage, {
      "temp_doctorsSchedule": temp_schedule,
      "doctorsHospital": this.doctorsHospital
    });
  }

  getDoctorsSchedules(refresher?){

    this.doctorProvider.getDoctorsHospitalList().then(data => {
      console.log('data',data);
      if(data['error'] == 0){
        this.doctorsHospitalCount = data['hospitals']['total_count']; 
        this.doctorsHospital = data['hospitals']['items']; 
      }
    });

    this.doctorProvider.getDoctorsSchedule().then(data => {
      if(data['error'] == 0){

        this.temp_schedule = data['hospitals']['items'];

        this.sun_idx = 0;
        this.mon_idx = 0;
        this.tue_idx = 0;
        this.wed_idx = 0;
        this.thu_idx = 0;
        this.fri_idx = 0;
        this.sat_idx = 0;

        data['hospitals']['items'].forEach((item, idx) => {
          if(item['doctor_schedule_day_id'] == '1'){
            this.sunday.push(item);
          }
          else if(item['doctor_schedule_day_id'] == '2'){
            this.monday.push(item);
          }
          else if(item['doctor_schedule_day_id'] == '3'){
            this.tuesday.push(item);
          }
          else if(item['doctor_schedule_day_id'] == '4'){
            this.wednesday.push(item);
          }
          else if(item['doctor_schedule_day_id'] == '5'){
            this.thursday.push(item);
          }
          else if(item['doctor_schedule_day_id'] == '6'){
            this.friday.push(item);
          }
          else if(item['doctor_schedule_day_id'] == '7'){
            this.saturday.push(item);
          }
        });

        /*data['hospitals']['items'].forEach((cat, idx) => {

          this.sampleSchedule = [];
          this.sampleSchedule1 = [];

          cat['schedules'].forEach((cat1, idx1) => {
            
            cat1['time_to'] = (parseFloat(cat1['time_to']) + 1)+":00";

            let timeslot = { 
                    "time_from" : cat1['time_from'],
                    "time_to" : cat1['time_to']
            };

            this.sampleSchedule.push(timeslot);
            this.sampleSchedule1.push(timeslot);
          });

          this.x = 0;
          this.test = [];
          this.test.push(this.sampleSchedule[0]);
          this.sampleSchedule.forEach((cat2, idx2) => {
            this.x++;
            if(this.x < this.sampleSchedule1.length){
              if((cat2['time_to']) !== this.sampleSchedule1[this.x]['time_from']){
                this.test.push(cat2);
                this.test.push(this.sampleSchedule1[this.x]);
              }
            }
          }); 

          this.test.push(this.sampleSchedule[this.sampleSchedule.length - 1]);
    
          this.storeScheds = null;
          this.storeSchedsCopy = [];
          this.test.forEach((time, didx) => {
            if(!this.storeScheds) {
                this.storeScheds = [];
            }
            if(didx % 2 == 0){
              this.storeScheds.push(time['time_from']);
              this.storeSchedsCopy.push(time['time_from']);
            }else{
              this.storeScheds.push(time['time_to']);
              this.storeSchedsCopy.push(time['time_to']);
            }
          }); 

          var temp_quantity = this.storeScheds.length / 2;
          this.storeSchedsCopy.splice(0, temp_quantity);
      
          this.storeScheds1 = null;
          this.y = 0; 
          this.storeSchedsCopy.forEach((day, dids) => {
              if(!this.storeScheds1) {
                  this.storeScheds1 = [];
              }

              let newSched = { 
                "time_from" : this.storeScheds[0 + this.y],
                "time_to" : this.storeScheds[1 + this.y]
              };
                     
              this.storeScheds1.push(newSched);
              this.y++;   
              this.y++;   
          }); 

          this.finalSchedule.push(this.storeScheds1);
          this.storeScheds1.forEach((sched, sidx) => {
            console.log(parseFloat(sched['time_from']));
            if(parseFloat(sched['time_from']) < 12){
              if(parseFloat(sched['time_from']) == 0){
                sched['time_from'] = '12:00';
              }
              sched['time_from_unit'] = 'AM';
            }else{
              sched['time_from_unit'] = 'PM';

              if(parseFloat(sched['time_from']) != 12){
                sched['time_from'] = (parseFloat(sched['time_from']) - 12)+':00';
              }

            }

            if(parseFloat(sched['time_to']) < 12){
              if(parseFloat(sched['time_to']) == 0){
                sched['time_from'] = '12:00';
              }
              sched['time_to_unit'] = 'AM';
            }else{
              sched['time_to_unit'] = 'PM';

              if(parseFloat(sched['time_to']) != 12){
                sched['time_to'] = (parseFloat(sched['time_to']) - 12)+':00';
              }                
            }
          }); 

          if(cat['doctor_schedule_day_id'] == '1'){
            this.sunday.push(cat);
            this.sunday[this.sun_idx]['timeslot'] = this.storeScheds1;
            this.sun_idx++;

          }else if(cat['doctor_schedule_day_id'] == '2'){
            this.monday.push(cat);
            this.monday[this.mon_idx]['timeslot']  = this.storeScheds1;
            this.mon_idx++;

          }else if(cat['doctor_schedule_day_id'] == '3'){
            this.tuesday.push(cat);
            this.tuesday[this.tue_idx]['timeslot'] = this.storeScheds1;
            this.tue_idx++;

          }else if(cat['doctor_schedule_day_id'] == '4'){
            this.wednesday.push(cat);
            this.wednesday[this.wed_idx]['timeslot'] = this.storeScheds1;
            this.wed_idx++;

          }else if(cat['doctor_schedule_day_id'] == '5'){
            this.thursday.push(cat);
            this.thursday[this.thu_idx]['timeslot'] = this.storeScheds1;
            this.thu_idx++;

          }else if(cat['doctor_schedule_day_id'] == '6'){
            this.friday.push(cat);
            this.friday[this.fri_idx]['timeslot'] = this.storeScheds1;
            this.fri_idx++;

          }else if(cat['doctor_schedule_day_id'] == '7'){
            this.saturday.push(cat);
            this.saturday[this.sat_idx]['timeslot'] = this.storeScheds1;
            this.sat_idx++;
          }
        });*/

        if(this.sunday.length != 0){
          this.doctorSchedule.push(this.sunday);

        }if(this.monday.length != 0){
          this.doctorSchedule.push(this.monday);

        }if(this.tuesday.length != 0){
          this.doctorSchedule.push(this.tuesday);
          
        }if(this.wednesday.length != 0){
          this.doctorSchedule.push(this.wednesday);
          
        }if(this.thursday.length != 0){
          this.doctorSchedule.push(this.thursday);
          
        }if(this.friday.length != 0){
          this.doctorSchedule.push(this.friday);
          
        }if(this.saturday.length != 0){
          this.doctorSchedule.push(this.saturday);
        }

        this.temp_schedule_count = this.doctorSchedule.length;
        console.log('doctorSchedule',this.doctorSchedule);
      }

      if(refresher) {
        refresher.complete();
      }

    });
  }

  removeDuplicates(arr){
    let unique_array = []
      for(let i = 0;i < arr.length; i++){
          if(unique_array.indexOf(arr[i]) == -1){
              unique_array.push(arr[i])
          }
      }
    return unique_array
  }

  gotoAddHospital(){
    this.navCtrl.push(BrowseHospitalsPage,{
      page_action: 'hospital_list',
    });
  }

  onPressStart(schedule, sIndex, index){
    console.log('sched',schedule);

    let actionSheet = this.actionSheetCtrl.create({
      //title: 'Select Image Source',
      buttons: [{
            text: 'Delete',
            icon: 'md-trash',
            cssClass: 'actionSheetCtrlDelete',
            handler: () => { 

              let alert = this.alertCtrl.create({
                  title: 'Delete Schedule',
                  message: 'Are you sure you want to delete this schedule?',
                  cssClass: 'AcceptBookingAlert',
                  buttons: [{
                      text: 'No',
                      role: 'cancel',
                      handler: () => {
                          console.log('Stay clicked');
                      }
                  },
                  {
                      text: 'Yes',
                      role: 'cancel',
                      handler: () => {
                        console.log('Leave clicked');

                        let loader = this.loadingCtrl.create();
                        loader.present();

                        let thisData = {
                          "day_id": schedule.doctor_schedule_day_id, 
                          "hospital_id": schedule.doctor_schedule_hospital_doctors_id,
                          "user_id": schedule.id,
                        };

                        this.doctorProvider.deleteScheduleTimeSlot(thisData).then((data) => {
                          if(data['error'] == 0){
                            this.doctorSchedule[sIndex].splice(index, 1);
                            if(this.doctorSchedule[sIndex].length == 0){
                              this.temp_schedule_count--;
                            }
                          }
                          loader.dismissAll();
                        });
                      }
                  }]
              });
              alert.present();

            }
        },
        {
          text: 'Cancel',
          icon: 'md-close',
          role: 'cancel'
      }]
    });
    actionSheet.present();
  }

}

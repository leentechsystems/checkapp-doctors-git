import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManageSchedulesPage } from './manage-schedules';
import { LongPressModule } from 'ionic-long-press';

@NgModule({
  declarations: [
    ManageSchedulesPage,
  ],
  imports: [
    LongPressModule,
    IonicPageModule.forChild(ManageSchedulesPage),
  ],
})
export class ManageSchedulesPageModule {}

import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController, LoadingController, Events, App } from 'ionic-angular';
import { ProfileDoctorPage } from '../../pages/profile-doctor/profile-doctor';
import { BrowseDoctorsPage } from '../../pages/browse-doctors/browse-doctors';
import { BrowseHospitalsPage } from '../../pages/browse-hospitals/browse-hospitals';
import { AddServicesPage } from '../../pages/add-services/add-services';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { GoogleMap, GoogleMaps } from '@ionic-native/google-maps';
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { PromptModalPage } from '../../pages/prompt-modal/prompt-modal';

/**
 * Generated class for the ProfileHospitalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 declare var google;

@IonicPage()
@Component({
	selector: 'page-profile-hospitals',
	templateUrl: 'profile-hospitals.html',
})
export class ProfileHospitalsPage {
	public hospital_tabs = "overview";
	profile = this.navParams.get('hospital_profile');
	doctors: any;
	doctor_schedules: any;
	hospital_location: any;
	marker: any;
	hospital_services: any;
	total_hospital_services = 0;
	total_count = 0;
	selected_service: any;
	user_profile: any;
	page_action = (this.navParams.get('page_action')) ? this.navParams.get('page_action') : null;
	my_total_hospital = (this.navParams.get('my_total_hospital')) ? this.navParams.get('my_total_hospital') : null;

	@ViewChild('map') mapElement: ElementRef;
	map: GoogleMap;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public hospitalsProvider: HospitalsProvider,
				public doctorProvider: DoctorProvider,
				private googleMaps: GoogleMaps,
				public siteProvider: SiteProvider,
				public modalCtrl: ModalController,
				public alertCtrl: AlertController,
				public loadingCtrl: LoadingController,
				public storage: Storage,
				public events: Events,
				private appCtrl: App,
				public userProvider: UserProvider) {

		this.loadDoctors(true);

		if(this.profile.description) {
			this.hospital_tabs = "overview";
		} else {
			this.hospital_tabs = "services";
		}

		console.log('page_action', this.page_action);
		console.log('my_total_hospital', this.my_total_hospital);

		this.storage.get("sess_user_login").then((val) => {
			this.user_profile = val;
		});

		events.subscribe('added_doctor_services', (services, time) => {
			this.getDoctorsServices();
		});

		if(this.page_action == 'hospital_list') {
			this.getDoctorsServices();
		} else {
			this.getHospitalServices();
		}
		console.log("profile hospital", this.profile);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ProfileHospitalsPage');
		if(this.profile.hospital_latitude && this.profile.hospital_longtitude) {
			 this.initializeMap();
		}
	}

	loadDoctors(isLoadNew) {
		let thisData = {
			"id": this.profile.id
		}
		this.hospitalsProvider.getAllHospitalDoctors(isLoadNew, thisData).then((res) => {
			if(res['hospital']) {
				this.doctors = res['hospital'];

				/* if doctor hospital */
				if(this.page_action == 'hospital_list'){
					this.doctors.forEach((doc, idx) => {
						if(doc.user_id == this.user_profile.id){
							this.doctors.splice(idx, 1);
						}
					});
				}
			}
		});
	}

	gotoDoctorProfile(thisDoctor) {
		this.navCtrl.push(ProfileDoctorPage, {
			"doctorProfile": thisDoctor
		});
	}

	goToBrowseDoctors(thisId) {
		this.navCtrl.push(BrowseDoctorsPage, {
			"hospital": this.profile
		});
	}


	initializeMap() {
		let element = this.mapElement.nativeElement;
		// this.map = this.googleMaps.create(element);
		this.map = new google.maps.Map(this.mapElement.nativeElement, {
			zoom: 13,
			center: {lat: parseFloat(this.profile['hospital_latitude']), lng: parseFloat(this.profile['hospital_longtitude'])},
			mapTypeControl: false,
			streetViewControl: false
		});

		this.marker = new google.maps.Marker({
			map: this.map,
			animation: google.maps.Animation.DROP,
			position: {lat: parseFloat(this.profile['hospital_latitude']), lng: parseFloat(this.profile['hospital_longtitude'])},
		});
	}

	getHospitalServices() {
		this.hospitalsProvider.getServices(this.profile.id).then(services => {
		 	if(services['error'] === 0) {
				this.total_hospital_services = services['services']['service_count'];
				this.hospital_services = services['services']['data'];
				this.total_count = services['services']['total_count'];
				console.log(this.hospital_services);
			}
		}, err => {
			console.log("getServices", err);
		});
	}

	getDoctorsServices() {

		this.storage.get("sess_user_login").then((val) => {
			this.user_profile = val;

			let thisData = {
				'hospital_id': this.profile.id,
				'doctor_id': this.user_profile.id
			};

			this.doctorProvider.getUserServices(thisData).then(services => {
				if(services['error'] === 0) {
					this.total_hospital_services = services['services']['service_count'];
					this.hospital_services = services['services']['data'];
					this.total_count = services['services']['total_count'];
					console.log(this.hospital_services);
				}
			}, err => {
				console.log("getServices", err);
			});
		});
	}

	addHospital(){
		let alert = this.alertCtrl.create({
			title: 'Add Hospital',
			message: 'Are you sure you want to add '+this.profile.name+'?',
			cssClass: 'AcceptBookingAlert',
			buttons: [{
				text: 'No',
				role: 'cancel',
				handler: () => {
					console.log('Cancel clicked');
				}
			},{
				text: 'Yes',
				handler: () => {
					console.log('Open clicked');

					let main = 1;

					if(this.my_total_hospital){
						main = 0;
					}

					let thisData = {
						"hospital_id": this.profile.id,
						"user_id": this.user_profile.id,
						"main": main
					};

					let loader = this.loadingCtrl.create({content: "Processing..."});
					loader.present();

					this.doctorProvider.addDoctorsHospital(thisData).then((data) => {
						loader.dismiss();
						if(data['error'] === 0){

							let alert = this.alertCtrl.create({
								title: 'Add Hospital',
								message: 'You have successfully added '+this.profile.name+' in you Hospital List',
								cssClass: 'AcceptBookingAlert',
								buttons: [{
									text: 'Okay',
									role: 'cancel',
									handler: () => {
										console.log('Cancel clicked');
										this.events.publish('add_hospital', data['result'], Date.now());
										this.events.publish('refresh-schedule', 'true', Date.now());
										this.navCtrl.pop();
									}
								}]
							});
							alert.present();
						}
					}, err => {
						console.log(err);
					});
				}
			}]
		});
		alert.present();
	}


	addtoBookmark(temp_hospital){
		console.log('hospital',temp_hospital);
		let thisData = {
			"item_id": temp_hospital.id,
			"type": 'hospital',
		};

		if(temp_hospital.bookmark_id){
			thisData['id'] = temp_hospital.bookmark_id;
		}

		if(temp_hospital.bookmark_id){
			this.profile.bookmark_id = null;
		}else{
			this.profile.bookmark_id = 1;
		}

		this.userProvider.saveBookmarks(thisData).then((data) => {
			if(data['error'] === 0){
				if(thisData['id']){
					this.profile.bookmark_id = null;
				}else{
					this.profile.bookmark_id = data['result']['id'];
				}
			}
		}, err => {
			console.log(err);
		});
	}

	seeAllServices(serviceKey, services){
		let doneModal = this.modalCtrl.create(PromptModalPage, {
			message_type: "see_all_services",
			service_type: serviceKey,
			services: services,
		});
		doneModal.onDidDismiss(data => {

		});

		doneModal.present();
	}

	addSevices(){
		if(this.profile){
			this.navCtrl.push(AddServicesPage, {
				'hospital': this.profile
			});
		}
	}
}

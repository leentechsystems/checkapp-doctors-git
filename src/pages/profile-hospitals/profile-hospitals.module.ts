import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileHospitalsPage } from './profile-hospitals';
import { IonicImageLoader } from 'ionic-image-loader';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    ProfileHospitalsPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(ProfileHospitalsPage),
    IonicImageLoader,
  ],
})
export class ProfileHospitalsPageModule {}

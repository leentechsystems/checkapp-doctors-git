import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, NavParams } from 'ionic-angular';
import { FormFilterAmbulancePage } from './../form-filter-ambulance/form-filter-ambulance';
import { AmbulanceProvider } from '../../providers/ambulance/ambulance';
import { UserProvider } from '../../providers/user/user';
import { CallNumber } from '@ionic-native/call-number';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the BrowseAmbulancePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse-ambulance',
  templateUrl: 'browse-ambulance.html',
})
export class BrowseAmbulancePage {

  specialties: any;
  selected_specialties: any;
  applied_filters: any;
  current_location: any;

  accessToken:any = "";
  all_ambulance: any;
  current_page = 1;
  total_count = 0;
  searched_keyword = (this.navParams.get('searched_keyword')) ? this.navParams.get('searched_keyword') : "";

  constructor(public navCtrl: NavController, public storage: Storage,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public ambulanceProvider: AmbulanceProvider,
              private callNumber: CallNumber,
              public userProvider: UserProvider) {

      this.storage.get("sess_current_location").then(sess_current_location => {
          if(sess_current_location) {
              this.current_location = sess_current_location;
          }
          this.loadAllData(false, true, false);
      }); 
  }

  ionViewDidLoad() {
      console.log('ionViewDidLoad BrowseAmbulancePage');
  }

  openFilterForm() {
    let filtermodal = this.modalCtrl.create(FormFilterAmbulancePage, {
        "specialties": this.specialties
    });

    filtermodal.onDidDismiss(data => { 
        if(data) {
          this.applied_filters = data;
          if(this.applied_filters.city) {
            this.all_ambulance = null;
            this.current_page = 1;
          }
          this.loadAllData(false, true, false);
        }
    });

    filtermodal.present();
    
  }


  loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
      let origin_coordinates = {};

      if(this.current_location) {
        origin_coordinates = {
          "lat": this.current_location['lat'],
          "lng": this.current_location['lng'],
        };
      } else {
        origin_coordinates = {
          "lat": 0,
          "lng": 0,
        };
      }

      let thisData = {
        "current_page": this.current_page,
        "searched_keyword": this.searched_keyword,
        "applied_filters": this.applied_filters,
        "origin_coordinates": origin_coordinates,
      };

       this.ambulanceProvider.getAllAmbulance(isLoadNew, thisData).then(data => {
        if(data['error'] == 0){
          this.total_count = data['result']['total_count'];

          if(!infiniteScroll && infiniteScroll === false) {
            if(!this.all_ambulance || isLoadNew === true || refresher) {
                 this.all_ambulance = []; 
                 console.log(this.all_ambulance);
              }
          }

          data['result']['items'].forEach((cat, idx) => {
                if(cat.id !== undefined) {
                    this.all_ambulance.push(cat);
                }
          });
          
          if(infiniteScroll) {
            infiniteScroll.complete();
          }
        }
        
        if(refresher) {
          refresher.complete();
        }
    });
  }

  doInfinite(infiniteScroll:any) {
         this.current_page += 1;
         if(this.all_ambulance.length < this.total_count) {
            this.loadAllData(false, false, infiniteScroll);
         } else {
            infiniteScroll.enable(false);
         }
  }

  searchNews(onCancel:any) {
    if(onCancel) {
          this.searched_keyword = "";
    }
    this.all_ambulance = null;
      this.current_page = 1; 
    this.loadAllData(false, true, false);   
  }

  callNow(thisNumber) {
    if(thisNumber['contact']) {
        let tocall = "";
        if(thisNumber.contact.type === "mobile") {
          tocall += thisNumber.contact.country_code;
        } else {
          tocall += thisNumber.contact.area_code;
        }
        tocall += " "+ thisNumber.contact.number;
       
        this.callNumber.callNumber(tocall, true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
    }
  }


  reloadData(refresher?){
      this.all_ambulance = null;
      this.current_page = 1;
      this.loadAllData(refresher, true, false);
  }

  addtoBookmark(temp_ambulance, ambulance_index){
    console.log('hospital',temp_ambulance);
    console.log('index',ambulance_index);

    let thisData = {
      "item_id": temp_ambulance.id,
      "type": 'ambulance',
    };

    if(temp_ambulance.bookmark_id){
      thisData['id'] = temp_ambulance.bookmark_id;
    }

    this.all_ambulance.forEach((cat, idx) => {
      if(ambulance_index == idx){
        if(temp_ambulance.bookmark_id){
          cat.bookmark_id = null;
        }else{
          cat.bookmark_id = 1;
        }
      }
    });

    this.userProvider.saveBookmarks(thisData).then((data) => {

      if(data['error'] === 0){
        this.all_ambulance.forEach((cat, idx) => {
          if(ambulance_index == idx){
            if(thisData['id']){
              cat.bookmark_id = null;
            }else{
              cat.bookmark_id = data['result']['id'];
            }
          }
        });
      }

    }, err => {

      console.log(err);

    });
  }

}

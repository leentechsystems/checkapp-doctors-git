import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileUserPage } from './profile-user';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    ProfileUserPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(ProfileUserPage),
    IonicImageLoader,
  ],
})
export class ProfileUserPageModule {}

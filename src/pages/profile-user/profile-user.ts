import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, ActionSheetController, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { HmoProvider } from '../../providers/hmo/hmo';
import { ImagePicker } from '@ionic-native/image-picker';
import { Camera } from '@ionic-native/camera';
import { ChangePasswordPage } from './../change-password/change-password'; 
import { BrowseSpecialtiesPage } from './../browse-specialties/browse-specialties';
import { EditProfilePage } from './../edit-profile/edit-profile';
import { SiteProvider } from '../../providers/site/site';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { BrowseHospitalsPage } from './../browse-hospitals/browse-hospitals';

/**
 * Generated class for the ProfileUserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-user',
  templateUrl: 'profile-user.html',
})
export class ProfileUserPage {

  user_profile: any;
  userid: any;
  user_name: any;
  hmo_account: any;
  photo: any;
  is_uploading = false;
  editing_section: any;
  medical_degrees: any;
  specialties: any;

  serviceType: any;
  total_service_type: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage,
    public hmoProvider: HmoProvider, public toastCtrl: ToastController, public siteProvider: SiteProvider,
    public camera: Camera, public imagePicker: ImagePicker, public actionSheetCtrl: ActionSheetController, 
    public doctorProvider: DoctorProvider, public modalCtrl: ModalController ) {

    this.getServiceType();

  }

  getServiceType(){
      this.doctorProvider.getServiceType().then(data => {
        console.log(data);
        if(data['error'] == 0){
          this.total_service_type = data['data']['total_count'];
          this.storage.set("sess_service_type", data['data']['data']).then(sess_service_type => {
              this.serviceType = sess_service_type;
          });
        }
      });
  }

  presentActionSheet() {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Select Image Source',
        buttons: [{
              text: 'Load from Library',
              handler: () => { this.openImagePicker(); }
          },
          {
              text: 'Use Camera',
              handler: () => { this.takePicture(); }
          },
          {
            text: 'Cancel',
            role: 'cancel'
        }]
      });
      actionSheet.present();
  }

   openImagePicker(){
      let options= {
        maximumImagesCount: 1,
        quality: 70,
      }

      this.imagePicker.getPictures(options).then((results) => {

        if(results !== 'OK'){
          results.forEach((pp, pidx) => {
            let pp_pieces = pp.split("/");
            let new_pp = {
              "name": pp_pieces[pp_pieces.length - 1],
              "path": pp
            };

            let thisPhoto = [];
            thisPhoto.push(new_pp);
            this.uploadPhoto(thisPhoto);
          });
        }
        
      }, (err) => { console.log(err) });
  }

  takePicture(){
      let options = {
        quality: 70,
        correctOrientation: true
      };

      this.camera.getPicture(options).then((data) => {
          console.log('picture',data);
          let pp_pieces = data.split("/");
          console.log('picture123',pp_pieces);
          let new_pp = {
            "name": pp_pieces[pp_pieces.length - 1],
            "path": data
          };

          let thisPhoto = [];
          thisPhoto.push(new_pp);
          this.uploadPhoto(thisPhoto);

      }, function(error) {
          console.log(error);
      });
  }

  uploadPhoto(thisPhoto) {
        this.is_uploading = true;

        this.siteProvider.uploadPhoto(thisPhoto).then(res => {
          if(res['error'] === 0) {
                let attachments = [];
                res['items'].forEach((items, iidx) => {
                    attachments.push({"filename": items.files[0].filename, "filepath": items.files[0].filepath})
                });

                let attachmentsData = {
                  "attachment_type": "user",
                  "item_id": this.userid,
                  "attachment": attachments
                };

                this.siteProvider.saveAttachment(attachmentsData).then(saveAttachmentResult => {
                      if(saveAttachmentResult['error'] === 0) {
                          let new_user_profile = this.user_profile;
                          new_user_profile['attachments_filename'] = saveAttachmentResult['attachments'][0]['filename'];
                          new_user_profile['attachments_filepath'] = saveAttachmentResult['attachments'][0]['filepath'];
                          this.photo = this.siteProvider.base_url + saveAttachmentResult['attachments'][0]['filepath'] + "/" + saveAttachmentResult['attachments'][0]['filename'];
                          console.log('this.photo',this.photo);
                          this.storage.set("sess_user_login", new_user_profile).then(sess_user_login => {
                              this.user_profile = new_user_profile;
                          });

                      } else {
                         this.presentToast("Something went wrong uploading files please try again.");
                      }
                      /* Finish this process */
                      this.is_uploading = false;
                });
                
          /* If moving of files fails*/     
          } else {
              this.presentToast("Something went wrong uploading files please try again.");
              /* Finish this process */
              this.is_uploading = false;
          }
      });
  }  

  presentToast(msg) {
      this.toastCtrl.create({
          message: msg,
          duration: 2000,
          position: "top"
      }).present();
  }

  /* 04-20-2018 */
  changePass(){
      this.navCtrl.push(ChangePasswordPage);
  }

  editSection(thisSection) {
    if(thisSection == 'specialty') {
        this.navCtrl.push(BrowseSpecialtiesPage, {
            "user_profile": this.user_profile
        });

    } 
    else if(thisSection == 'services'){
        this.navCtrl.push(BrowseHospitalsPage,{
          page_action: 'hospital_list',
        });
    }
    else {
        this.navCtrl.push(EditProfilePage, {
            "editing_section": thisSection,
            "user_profile": this.user_profile
        });
    } 
    this.editing_section = null;
  }

  saveSection() {
    this.editing_section = "";
  }


  ionViewDidEnter() {
      this.storage.get('sess_user_login').then((user) => {
        this.userid = user.id;
        this.user_name = user.firstname;
        this.user_profile = user;
        console.log(this.user_profile);

         if(user.attachments_filename && user.attachments_filepath) {
            this.photo = this.hmoProvider.base_url + user.attachments_filepath +"/"+ user.attachments_filename;
        }
    });
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseHmoTransactionsPage } from './browse-hmo-transactions';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    BrowseHmoTransactionsPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(BrowseHmoTransactionsPage),
  ],
})
export class BrowseHmoTransactionsPageModule {}

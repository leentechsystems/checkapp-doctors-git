import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { HmoProvider } from '../../providers/hmo/hmo';
import { ProfileAppointmentPage } from '../../pages/profile-appointment/profile-appointment';
import { FormFilterPatientsPage } from './../form-filter-patients/form-filter-patients';

/**
 * Generated class for the BrowseHmoTransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-browse-hmo-transactions',
	templateUrl: 'browse-hmo-transactions.html',
})
export class BrowseHmoTransactionsPage {
	all_previous: any;
	all_today: any;
	current_today_page = 1;
	current_prev_page = 1;
	total_today_count = 0;
	total_prev_count = 0;
	today_is_loading = false;
    applied_filters: any;
	pageType = this.navParams.get('pageType') ? this.navParams.get('pageType') : null;
	patient_id = this.navParams.get('patient_id') ? this.navParams.get('patient_id') : null;
	patient_name = this.navParams.get('patient_name') ? this.navParams.get('patient_name') : null;

	constructor(public navCtrl: NavController, public navParams: NavParams, public hmoProvider: HmoProvider, 
				public modalCtrl: ModalController, public viewCtrl: ViewController) {

		this.loadAllData(false, true, false); 
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad BrowseHmoTransactionsPage');
	}

	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
		if(isLoadNew = true) { /* if refreshed */
			this.current_today_page = 1; /* reset the current page to default */
			this.current_prev_page = 1; /* reset the current page to default */
		}

		if(this.pageType){
			this.applied_filters = {
				'patient_id' : this.patient_id,
			};
		}

		/* - load functions responsible on getting and processing the data 
		- we'll pass refresher so we will stop the refresher event after processing the data NOT promptly
		- we'll pass infiniteScroll so we will stop the infiniteScroll event after processing the data NOT promptly */
		this.loadAllToday(refresher, isLoadNew); 
		this.loadAllPrevious(refresher, isLoadNew, infiniteScroll); 
	}

	loadAllToday(refresher?, isLoadNew?) {
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.current_today_page, /* separate each parameters bty comma*/
            "applied_filters": this.applied_filters
		};

		this.hmoProvider.getAllTodayTransactions(isLoadNew, thisData).then(data => {
			if(data['error'] == 0) {
				this.total_today_count = data['today']['total_count'];

				if(!this.all_today || isLoadNew === true) {
					this.all_today = []; 
				}

				data['today']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {                  

						let wordDate = cat['appointment_appointment_date'].split(" ");
						let wordCount = wordDate[1].split(":");

						if(wordCount[0] < 12){
							cat['appointment_time'] = wordCount[0]+':'+wordCount[1]+' AM';

						}else{
							if(wordCount[0] == 12){
								cat['appointment_time'] = '12'+':'+wordCount[1]+' PM';

							}else{
								cat['appointment_time'] = (wordCount[0]-12) +':'+wordCount[1]+' PM';
							}
						}

						this.all_today.push(cat);
					}
				});
			}

			if(refresher) {
				refresher.complete();
			}
		});
	}

	loadAllPrevious(refresher?, isLoadNew?, infiniteScroll?) {

		console.log(this.current_prev_page);
		
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.current_prev_page, /* separate each parameters bty comma*/
            "applied_filters": this.applied_filters
		};

		this.hmoProvider.getAllPreviousTransactions(isLoadNew, thisData).then(data => {
			if(data['error'] == 0) {
				this.total_prev_count = data['previous']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.all_previous || isLoadNew === true) {
						this.all_previous = []; 
					}
				}

				data['previous']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {                  
						let wordDate = cat['appointment_appointment_date'].split(" ");
						let wordCount = wordDate[1].split(":");

						if(wordCount[0] < 12){
							cat['appointment_time'] = wordCount[0]+':'+wordCount[1]+' AM';

						}else{
							if(wordCount[0] == 12){
								cat['appointment_time'] = '12'+':'+wordCount[1]+' PM';

							}else{
								cat['appointment_time'] = (wordCount[0]-12) +':'+wordCount[1]+' PM';
							}
						}

						this.all_previous.push(cat);
					}
				});
				
				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}

			if(refresher) {
				refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
			}
		});
	}

	doInfinite(infiniteScroll:any) {
		this.current_prev_page += 1;
		if(this.all_previous.length < this.total_prev_count) {
			this.loadAllPrevious(false, false, infiniteScroll);

		} else {
			infiniteScroll.enable(false);
		}
	}


	loadMoreToday() {
		this.current_today_page += 1;
		if(this.all_today.length < this.total_today_count) {
			this.loadAllToday(false, false);
		} else {
			this.today_is_loading = false;
		}
	}

	reloadData(refresher?){
		this.all_previous = null;
		this.current_prev_page = 1;
		this.loadAllData(refresher, true, false);
	}

	goToDetails(thisDetails) {
		thisDetails['description'] = thisDetails['appointment_description'];
		thisDetails['id'] = thisDetails['appointment_id'];
		thisDetails['appointment_date'] = thisDetails['appointment_appointment_date'];

		let pieces = thisDetails.appointment_appointment_date.split(" "); 
		let wordCount = pieces[0].split("-");
		let timeCount = pieces[1].split(":");
		let date_target_year = wordCount[0];
		let date_target_month = wordCount[1];
		let date_target_day = wordCount[2];
		let date_target_hour = timeCount[0];
		let date_target_minutes = timeCount[1];

		let mlist = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
		let convert_appointment_date = new Date(date_target_year, parseInt(date_target_month)-1, date_target_day, date_target_hour, date_target_minutes);
			
		/* Convertion to 12-hour time */
		let new_selected_hour = "";
		let new_selected_time_unit = "";
		if(parseInt(date_target_hour) > 12) {
			new_selected_time_unit = "PM";
			let new_selected_hour1 = parseInt(date_target_hour) - 12;
			if(new_selected_hour1 < 10) {
				new_selected_hour = "0" + new_selected_hour1.toString();
			} else {
				new_selected_hour = new_selected_hour1.toString();
			}
		} else {
			new_selected_time_unit = "AM";
			new_selected_hour = date_target_hour;
		}
			
		thisDetails['appointment_date_day'] =  date_target_day;
		thisDetails['appointment_date_month'] =  mlist[date_target_month - 1];
		thisDetails['appointment_date_year'] =  parseInt(date_target_year);
		thisDetails['appointment_date_time'] =  new_selected_hour +":"+ date_target_minutes + "" + new_selected_time_unit;

		let modal = this.modalCtrl.create(ProfileAppointmentPage, {
			"appointment_details": thisDetails
		});
		modal.present();
	} 

	gotofilter(){
        let filterModal = this.modalCtrl.create(FormFilterPatientsPage,{
           	filter_type: 'hmo_transaction',
            data: this.applied_filters
        });

        filterModal.onDidDismiss(data => { 
            if(data){
            	console.log(data);
                if(data == 'reset'){
					this.all_previous = null;
					this.all_today = null;
					this.current_today_page = 1;
					this.current_prev_page = 1;
					this.total_today_count = 0;
					this.total_prev_count = 0;
                    this.applied_filters = null;
                    this.loadAllData(false,true,false);
                }else{
					this.all_previous = null;
					this.all_today = null;
					this.current_today_page = 1;
					this.current_prev_page = 1;
					this.total_today_count = 0;
					this.total_prev_count = 0;
                    this.applied_filters = data;
                    this.loadAllData(false,true,false);
                }
            }
        });

        filterModal.present();
    }  

    goBack(){
		this.viewCtrl.dismiss();
    }
}

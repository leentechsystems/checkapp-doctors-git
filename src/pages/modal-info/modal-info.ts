import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ModalInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal-info',
  templateUrl: 'modal-info.html',
})
export class ModalInfoPage {

	public contactForm: FormGroup;
	modal_type: any;
  	modal_contact: any;
  	contact_idx: any;
  	hide_area_code = false;
  	formErrorNumber = 'Mobile Number must be at least 10 digit';
  	is_submit = false;

  	focus_number = false;

	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public viewCtrl: ViewController,
				public formBuilder: FormBuilder,
				public toastCtrl: ToastController,
				public loadingCtrl: LoadingController,
				public userProvider: UserProvider,
				public storage: Storage,
				public alertCtrl: AlertController) {

		this.modal_type = this.navParams.get('modal_type');
		this.modal_contact = (this.navParams.get('modal_contact')) ? this.navParams.get('modal_contact') : null;
		this.contact_idx = (this.navParams.get('contact_idx') >= 0) ? this.navParams.get('contact_idx') : null;

  		this.contactForm = formBuilder.group({
          	country_code: ['+63'],
		  	area_code: [this.modal_contact ? this.modal_contact.area_code : '', Validators.compose([Validators.minLength(4), Validators.maxLength(4)])],
          	number: [this.modal_contact ? this.modal_contact.number : '', Validators.required],
          	type: [this.modal_contact ? this.modal_contact.type : '', Validators.required],
          	id: [this.modal_contact ? this.modal_contact.id : ''],
          	item_id: [this.modal_contact ? this.modal_contact.item_id : '']
      	});

	  	if(this.modal_contact){
			this.contactTypeChange('', this.modal_contact);
	  	}
	  	console.log(this.contact_idx);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ModalInfoPage');
	}

	shareSocailMedia(socialMedia){
		this.viewCtrl.dismiss(socialMedia);
	}

  	contactTypeChange(reset = null, contact = null){
		console.log(contact);
		console.log(this.contactForm.value['type']);

        setTimeout( () => {
        	if(reset){
		  		this.contactForm.controls['number'].setValue('');
        	}

          	if(contact){
			  	this.contactForm.controls['type'].setValue(contact.type ? contact.type : '');
			  	this.contactForm.controls['number'].setValue(contact.number ? contact.number : '');
			  	this.contactForm.controls['area_code'].setValue(contact.area_code ? contact.area_code : '');
			}
			if(this.contactForm.value['type'] == 'mobile'){
			  	this.contactForm.controls.number.setValidators([Validators.required, Validators.minLength(10), Validators.maxLength(10)]);
			  	this.contactForm.controls.area_code.setValidators([]);
		  		this.contactForm.controls['area_code'].setValue('');
			  	this.formErrorNumber = 'Mobile Number must be at least 10 digit only';
			  	this.hide_area_code = true;

			}else{
			  	this.contactForm.controls.area_code.setValidators([Validators.required, Validators.minLength(4), Validators.maxLength(4)]);
			  	this.contactForm.controls.number.setValidators([Validators.required, Validators.minLength(7), Validators.maxLength(7)]);
			  	this.formErrorNumber = 'Mobile Number must be at least 7 digit only';
				this.hide_area_code = false;
			}
        }, 700);

  	}

  	modalAction(action){
  		console.log('action',action);

	 	if(action == 'clear'){
		  	this.contactForm.controls['type'].setValue('');
		  	this.contactForm.controls['number'].setValue('');
		  	this.contactForm.controls['area_code'].setValue('');

	 	}else{

  			this.is_submit = true;
		  	console.log(this.contactForm);
		 	if(!this.contactForm.valid) {
				this.presentToast("Please fill-up all necessary fields");

		 	}else{

		 		if(action == 'remove'){
		 			this.contactForm.value['action'] = 'remove';
		 		}
				let loader = this.loadingCtrl.create();
				loader.present();

				this.userProvider.contactInformation(this.contactForm.value).then(res => {
					console.log('res',res);
					if(res['error'] === 0) {
						this.storage.get("sess_user_login").then(data => {
							if(data){
								console.log('old_sess_user_login',data);
								if(action == 'remove'){
									data['contact'].splice(this.contact_idx, 1);

								}else{
									if(this.contact_idx != null){
										console.log('contact splice ',this.contact_idx);
										data['contact'].splice(this.contact_idx, 1, res['data']);
									}else{
										if(data['contact']){
											console.log('have contact');
											data['contact'] = res['data'];
											//data['contact'].push(res['data']);
										}else{
											console.log('no contact');
											data['contact'] = [];
											data['contact'] = res['data'];
											//data['contact'].push(res['data']);
										}
									}
								}
								console.log('data',data);
								this.storage.set("sess_user_login", data).then(sess_user_login => {
									console.log('sess_user_login',sess_user_login);
									this.viewCtrl.dismiss(sess_user_login);
								});
							}
						});
					}
					loader.dismiss();
				});
		 	}
	 	}
  	}

  	deleteContact(){
    	let alert = this.alertCtrl.create({
          	title: 'Remove Contact',
          	message: 'Are you sure you want to remove this contact?',
          	cssClass: 'AcceptBookingAlert',
          	buttons: [{
              	text: 'No',
              	role: 'cancel',
              	handler: () => {
                 	console.log('Stay clicked');
              	}
          	},{
              	text: 'Yes',
              	role: 'cancel',
              	handler: () => {
                	console.log('Leave clicked');
                	this.modalAction('remove');
              	}
          	}]
      	});
      	alert.present();
  	}

  	checkFocus(){
  		this.focus_number = true;
  	}

  	checkBlur(){
  		this.focus_number = false;
  	}

    escapeModal() {
      this.viewCtrl.dismiss();
    }

    stopPropagation(e) {
      e.stopPropagation();
    }

  	presentToast(msg) {
	  	this.toastCtrl.create({
		  	message: msg,
		  	duration: 2000,
		  	position: "top"
	  	}).present();
  	}
}

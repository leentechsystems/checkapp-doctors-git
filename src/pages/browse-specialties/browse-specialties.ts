import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, AlertController } from 'ionic-angular';
import { BrowseDoctorsPage } from '../../pages/browse-doctors/browse-doctors';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the BrowseSpecialtiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse-specialties',
  templateUrl: 'browse-specialties.html',
})
export class BrowseSpecialtiesPage {

  /* Declare a variable where you'll be storing the data on this page */
  all_results: any;
  current_page = 1; /* Step 1: Declare default page */
  total_count = 0;  /* Total count from the server */
  searched_keyword = "";
  user_profile: any;
  categoriesChks:boolean[];
  tempSelectedChecks: any;
  user_specialties: any;
  counter: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public doctorProvider: DoctorProvider,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController,
    public storage: Storage, public toastCtrl: ToastController,) {
       /* Step 2: Call the function to load the data. First parameter is a refresher (boolean) this is the pull-to-refresh event, 
      Second is isLoadNew (boolean) an indicator if we need to get new data from database */
      this.loadAllData(false, true, false); 
      this.user_profile = this.navParams.get('user_profile');
      this.user_specialties = this.user_profile.specialties;
  }

  loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
      /* getting of data from the provider, we'll passing set of data the function is requiring */
      let thisData = {
        "current_page": this.current_page, /* separate each parameters bty comma*/
        "searched_keyword": this.searched_keyword
      };

      this.doctorProvider.getSpecializations(isLoadNew, thisData).then(data => {
          if(data['result']) {
              this.total_count = data['result']['total_count'];

              if(!infiniteScroll && infiniteScroll === false) {
                   /* converting into array if variable is empty */
                  /* emptying the variable when refreshed */
                  if(!this.all_results || isLoadNew === true) {
                     this.all_results = []; 
                  }
              }

              if(isLoadNew === true) {
                this.tempSelectedChecks = [];
              }else{
                this.tempSelectedChecks = this.categoriesChks;
              }

              data['result']['items'].forEach((cat, idx) => {
                    if(cat.id !== undefined) {
                        this.all_results.push(cat);
                    } 

                    if(this.user_specialties){
                      this.checkIfExistingSpeacilities(cat);
                      if(this.counter != 0){
                        this.tempSelectedChecks.push(true);
                      }else{
                        this.tempSelectedChecks.push(false);
                      }
                    }else{
                      this.tempSelectedChecks.push(false);
                    }

                    if(infiniteScroll) {
                        infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
                    }
              });

              this.categoriesChks = this.tempSelectedChecks;

              if(refresher) {
                refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
              }
          }
      });
  }

  checkIfExistingSpeacilities(cat){
    this.counter = 0;
    this.user_specialties.forEach((spe, ids) => {
      if(cat.id == spe.specialties_id){
        this.counter ++;
      }
    });
  }

  doInfinite(infiniteScroll:any) {
     this.current_page += 1;
     if(this.all_results.length < this.total_count) {
        this.loadAllData(false, false, infiniteScroll);
     } else {
        infiniteScroll.enable(false);
     }
  }

  searchKeyword(onCancel:any) {
    if(onCancel) {
        this.searched_keyword = "";
    }
    this.all_results = null;
    this.current_page = 1;
    this.loadAllData(false, true, false); 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BrowseSpecialtiesPage');
  }

  presentToast(msg) {
    this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: "top"
    }).present();
  }

  saveSpecialty(){
    console.log('categoriesChks',this.categoriesChks);
    console.log('all_results',this.all_results);

    let selected_specialties = [];
    let temp_selected_specialties = [];

    if(this.categoriesChks && this.all_results){

      this.categoriesChks.forEach((cat, ids) => {
        if(cat == true){
          this.all_results.forEach((all, idx) => {
            if(ids == idx){
              selected_specialties.push({
                "id": idx,
                "specialties_description": all['description'],
                "specialties_id": all['id'],
                "specialties_title": all['title'].slice(0, -1),
                "user_id": this.user_profile['user_id'],
              });
              temp_selected_specialties.push({
                "specialties_id": all['id']
              });
            }
          });
        }
      });

      console.log(selected_specialties);
    }

    if(temp_selected_specialties.length != 0){

      let loader = this.loadingCtrl.create();
      loader.present();

      this.doctorProvider.editDoctorSpecialty({"specialty" : temp_selected_specialties}).then(res => {
          if(res['error'] === 0) {

              let new_sess_user_login = this.user_profile;
              new_sess_user_login['total_specialties'] = res['total_specialties'];
              new_sess_user_login['specialties'] = selected_specialties;

              this.storage.set("sess_user_login", new_sess_user_login).then(sess_user_login => {
                  let alert = this.alertCtrl.create({
                      title: 'Edit Profile',
                      message: 'Your profile has been updated successfully!',
                      cssClass: 'AcceptBookingAlert',
                      buttons: [{
                        text: 'Back to Profile',
                        role: 'cancel',
                        handler: () => {
                            this.navCtrl.pop();
                            console.log('Cancel clicked');
                        }
                      }]
                  });
                  alert.present();
              });
          } else {
              this.presentToast("Something went wrong. Please try again.");
          }

          loader.dismiss();
      });
    }
  }
}

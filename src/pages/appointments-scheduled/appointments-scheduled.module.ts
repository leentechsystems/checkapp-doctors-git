import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentsScheduledPage } from './appointments-scheduled';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    AppointmentsScheduledPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(AppointmentsScheduledPage),
    IonicImageLoader,
  ],
})
export class AppointmentsScheduledPageModule {}

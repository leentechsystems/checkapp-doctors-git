import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { ViewModalPage } from './../view-modal/view-modal';
import { ProfileHospitalsPage } from './../profile-hospitals/profile-hospitals';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the AppointmentsScheduledPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-appointments-scheduled',
  templateUrl: 'appointments-scheduled.html',
})
export class AppointmentsScheduledPage {

  scheduled_appointment = this.navParams.get('temp_appointment');
  approval_date = this.navParams.get('temp_date');
  scheduled_appointments: any;
  hospital: any;

  total_scheduled_appointment: 0;
  current_page = 1;
  from_notif = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public modalCtrl : ModalController,
              public siteProvider : SiteProvider,
              public hospitalsProvider: HospitalsProvider,
              public appointmentProvider: AppointmentProvider) {

    this.hospitalProfile();

  }

  ionViewWillEnter() {
    this.reloadScheduledAppointment(false, true, false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppointmentsScheduledPage');
  }

  reloadData(refresher?){
      this.scheduled_appointment = null;
      this.current_page = 1;
      this.reloadScheduledAppointment(refresher, true, false);
  }

  hospitalProfile(){
      var hospital_id;
      if(this.from_notif){
        hospital_id = this.scheduled_appointment.hospital_id;
      }else{
        hospital_id = this.scheduled_appointment[0]['hospital_id'];
      }
      this.hospitalsProvider.getSpecificHospital(hospital_id).then((data) => {
          this.hospital = data['hospital'][0];
          console.log(this.hospital);
        }, err => {

        console.log(err);
      });
  }

  gotoHospitalProfile(){

    this.navCtrl.push(ProfileHospitalsPage, {
      "hospital_profile": this.hospital
    });
  }

  gotoViewModal(patient_schedule_index, temp_patient_schedule){
    let doneModal = this.modalCtrl.create(ViewModalPage, {
          temp_patient_schedule: temp_patient_schedule,
          temp_date: this.approval_date
    });

      doneModal.onDidDismiss(data => {

        if(data){
            console.log('modal galing',data);

            this.reloadScheduledAppointment(false, true, false);
        }else{
            console.log('modal galing',this.approval_date);

            this.reloadScheduledAppointment(false, true, false);
        }
    });

    doneModal.present();
  }

  reloadScheduledAppointment(refresher?, isLoadNew?, infiniteScroll?){

    console.log(this.approval_date);
    let thisData = {
        "current_page": this.current_page,
        "day": 'today',
        "date": this.approval_date,
        "category": 'scheduled'
    };

    this.appointmentProvider.getDoctorsAppointments(true, thisData).then((data) => {
        this.total_scheduled_appointment = data['appointments']['total_count'];

        if(!infiniteScroll && infiniteScroll === false) {
          if(!this.scheduled_appointments || isLoadNew === true) {
             this.scheduled_appointments = [];
          }
        }

        data['appointments']['items'].forEach((cat, idx) => {
              if(cat.id !== undefined) {

                  let wordDate = cat['appointment_date'].split(" ");
                  let wordCount = wordDate[1].split(":");

                  if(wordCount[0] < 12){
                    cat['appointment_time'] = wordCount[0]+':'+wordCount[1]+' AM';

                  }else{
                    if(wordCount[0] == 12){
                      cat['appointment_time'] = '12'+':'+wordCount[1]+' PM';
                    }else{
                      cat['appointment_time'] = (wordCount[0]-12) +':'+wordCount[1]+' PM';
                    }
                  }

                  this.scheduled_appointments.push(cat);
              }

              if(infiniteScroll) {
                infiniteScroll.complete();
              }
        });

        if(refresher) {
          refresher.complete();
        }
    });
  }

  doInfinite(infiniteScroll:any) {
    this.current_page += 1;
      if(this.scheduled_appointments.length < this.total_scheduled_appointment) {
          this.reloadScheduledAppointment(false, false, infiniteScroll);
      } else {
          infiniteScroll.enable(false);
      }
  }

}

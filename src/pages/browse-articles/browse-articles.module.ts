import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseArticlesPage } from './browse-articles';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    BrowseArticlesPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(BrowseArticlesPage),
    IonicImageLoader,
  ],
})
export class BrowseArticlesPageModule {}

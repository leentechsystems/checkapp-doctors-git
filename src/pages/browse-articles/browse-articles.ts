import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfileArticlePage } from '../../pages/profile-article/profile-article';
import { Storage } from '@ionic/storage';

import { ArticleProvider } from '../../providers/article/article';
import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the BrowseArticlesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-browse-articles',
  templateUrl: 'browse-articles.html',
})
export class BrowseArticlesPage {

  	/* Declare a variable where you'll be storing the data on this page */
  	featured_articles: any;
  	all_articles: any;
  	current_page = 1; /* Step 1: Declare default page */
  	total_count = 0;  /* Total count from the server */
  	total_featured_count = 0;  /* Total count from the server */
  	searched_keyword = (this.navParams.get('searched_keyword')) ? this.navParams.get('searched_keyword') : "";

  	constructor(public navCtrl: NavController,
  				public navParams: NavParams,
			  	public articleProvider: ArticleProvider,
			  	private storage: Storage,
			  	public userProvider: UserProvider,
                public siteProvider: SiteProvider ) {

	  	/* Step 2: Call the function to load the data. First parameter is a refresher (boolean) this is the pull-to-refresh event,
	  	Second is isLoadNew (boolean) an indicator if we need to get new data from database */
	  	if(this.navParams.get('searched_keyword')){
			this.searched_keyword = this.navParams.get('searched_keyword');
	  	}
	  	this.loadAllData(false, true, false);
 	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad BrowseArticlesPage');
  	}

  	loadAllData(refresher?, isLoadNew?, infiniteScroll?) {
	  	if(isLoadNew = true) { /* if refreshed */
		  	this.current_page = 1; /* reset the current page to default */
	  	}

	  	/* - load functions responsible on getting and processing the data
	  	- we'll pass refresher so we will stop the refresher event after processing the data NOT promptly
	  	- we'll pass infiniteScroll so we will stop the infiniteScroll event after processing the data NOT promptly */

	  	this.loadFeatureArticles(isLoadNew);
	  	this.loadAllArticles(refresher, isLoadNew, infiniteScroll);
  	}

  	loadFeatureArticles(isLoadNew?) {
	  	this.articleProvider.getFeaturedArticles(isLoadNew).then((data) => {
		  	if(data['error'] == 0) {
				this.featured_articles = data['articles'];
		  	}
	  	});
  	}

  	loadAllArticles(refresher?, isLoadNew?, infiniteScroll?) {
		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.current_page, /* separate each parameters bty comma*/
			"searched_keyword": this.searched_keyword,
			"featured": 0
		};

		this.articleProvider.getAllArticles(isLoadNew, thisData).then(data => {
			if(data['error'] == 0) {
				this.total_count = data['result']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.all_articles || isLoadNew === true) {
						this.all_articles = [];
					}
				}

				data['result']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						this.all_articles.push(cat);
					}
				});

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}

			if(refresher) {
			  refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
			}
		});
  	}

  	addtoBookmark(temp_article, article_index){
		console.log('article',temp_article);

		let thisData = {
		  	"item_id": temp_article.id,
		  	"type": 'article',
		};

		if(temp_article.bookmark_id){
		  	thisData['id'] = temp_article.bookmark_id;
		}

		this.all_articles.forEach((cat, idx) => {
		  	if(article_index == idx){
				if(temp_article.bookmark_id){
				  	cat.bookmark_id = null;
				}else{
				  	cat.bookmark_id = 1;
				}
		  	}
		});

		this.userProvider.saveBookmarks(thisData).then((data) => {

		  	if(data['error'] === 0){
				this.all_articles.forEach((cat, idx) => {
				  	if(article_index == idx){
						if(thisData['id']){
						  	cat.bookmark_id = null;
						}else{
						  	cat.bookmark_id = data['result']['id'];
						}
				  	}
				});
		 	}
		}, err => {
	  		console.log(err);
		});
  	}

  	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		if(this.all_articles.length < this.total_count) {
			this.loadAllArticles(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
  	}

  	goToArticleInside(articleProfile) {
		this.navCtrl.push(ProfileArticlePage,{
		  	articleProfile : articleProfile
		});
  	}

  	reloadData(refresher?){
	  	this.all_articles = null;
	  	this.current_page = 1;
	  	this.loadAllData(refresher, true, false);
  	}
}

import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController, Events, Nav, Tabs} from 'ionic-angular';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { PatientsListPage } from '../patients-list/patients-list';
import { AppointmentsPage } from '../appointments/appointments';
import { NotificationPage } from '../notification/notification';
import { MorePage } from '../more/more';
import { UserProvider } from '../../providers/user/user';
import { FCM } from '@ionic-native/fcm';
import { Network } from '@ionic-native/network';
import { Storage } from '@ionic/storage';
import { Badge } from '@ionic-native/badge';
import { SiteProvider } from '../../providers/site/site';


@Component({
  	templateUrl: 'tabs.html'
})
export class TabsPage {
  	@ViewChild(Nav) nav: Nav;
  	@ViewChild('myTabs') tabRef: Tabs;

  	tab1Root = HomePage;
  	tab2Root = PatientsListPage;
  	tab3Root = AppointmentsPage;
  	tab4Root = NotificationPage;
  	tab5Root = MorePage;
  	total_unread_notifications: any;
  	//from_notification = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;
    toast_counter = 0;

  	constructor(public toastCtrl: ToastController, private network: Network, public userProvider: UserProvider,
				public events: Events, public fcm: FCM, public navParams: NavParams, public navCtrl: NavController,
				public storage: Storage, public badge: Badge, public siteProvider: SiteProvider) {

	    events.subscribe('total_unread_notification:created', (action, time) => {
	      	storage.get('sess_total_unread_notification').then((val) => {
		        if(action == 'summation'){
		          this.total_unread_notifications = val - 1;
		          this.badge.decrease(1);
		          this.storage.set("sess_total_unread_notification", this.total_unread_notifications);
		        }else if(action == 'addition'){
		          this.total_unread_notifications = val + 1;
		          this.badge.increase(1);
		          this.storage.set("sess_total_unread_notification", this.total_unread_notifications);
		        }
	      	});
	    });

	    // this.fcm.onNotification().subscribe(data => {
      	// console.log(data);
      	// if(data.wasTapped == true){
	    //     console.log("Received in background");
	    //     this.tabRef.select(3);
      	// } else {
	    //     console.log("Received in foreground");
	    //     events.publish('total_unread_notification:created', 'addition', Date.now());
	    //     this.showToast('1 New Unread Notification');
      	// };
	    // });

  	}

  	ionViewDidEnter() {
      	this.network.onchange().subscribe(data => {
        	this.displayNetworkUpdate(data.type);
      	});

      	this.totalUnreadNotification();
  	}


  	displayNetworkUpdate(connectionState: string) {
      	let options = {};

      	if(connectionState === "offline") {
        	options = {
              	message: "You are offline",
              	position: "top",
              	showCloseButton: true,
              	duration: 5000,
              	closeButtonText: "x"
          	};
      	}  else {
        	options = {
              	message: "You are now connected",
              	position: "top",
              	duration: 5000,
              	cssClass: "success"
          	};
      	}

        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();

        let current_counter = this.siteProvider.timeToDecimal(h +':'+ m);

        if(this.toast_counter){
          if(current_counter != this.toast_counter){
            console.log(current_counter+" "+this.toast_counter);
            this.toastCtrl.create(options).present();
            this.toast_counter = current_counter;
          }

        }else{

          this.toastCtrl.create(options).present();
          this.toast_counter = current_counter;
        }
  	}

  	totalUnreadNotification(){

	    let thisData = {
	        "current_page": '1',
	        "action": 'unread'
	    };

      	this.userProvider.getNotification(true, thisData).then(data => {
          if(data['error'] == 0){
            this.total_unread_notifications = data['result']['total_unread'];
            console.log('total_unread_count',this.total_unread_notifications);
            this.storage.set("sess_total_unread_notification", this.total_unread_notifications);
            this.badge.set(this.total_unread_notifications);
          }
      	});
  	}


  	showToast(msg) {
		const toast = this.toastCtrl.create({
			message: msg,
			showCloseButton: true,
			duration: 8000,
			dismissOnPageChange: true,
			position: 'top',
			closeButtonText: "View",
			cssClass: 'toastNotification'
		});

		toast.onDidDismiss((data, role) => {
			if(role == 'close'){
			  this.tabRef.select(3);
			}
		});
		toast.present();
  	}
}

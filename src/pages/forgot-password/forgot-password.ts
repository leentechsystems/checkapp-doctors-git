import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, AlertController, NavParams, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { UserProvider } from '../../providers/user/user';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  email_address: any;
  verification_code: any;
  new_password: any;
  title_text = "";
  lbl_text = "";
  current_step: any;
  btn_text = "";

   passwordType = "password";
   passwordIcon = "ios-eye";
   passwordStatus = "hide";

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, 
    public toastCtrl: ToastController, public userProvider: UserProvider, private alertCtrl: AlertController,
    public loadingCtrl: LoadingController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  ionViewDidEnter() {
    console.log('ionionViewDidEnterViewDidLoad ForgotPasswordPage');

      /*
        step1 - enter email address
        step 2 - validate code
        step 3 - change password
      */
      this.storage.get("sess_forgot_pass_email").then(sess_forgot_pass_email => {
          if(sess_forgot_pass_email) {
              this.current_step = "step2";
              this.title_text = "Validate Code";
              this.lbl_text = "We have sent you an email to your email address with the verification code.";
              this.btn_text = "Verify";
              this.email_address = sess_forgot_pass_email.email;
          } else {
              this.current_step = "step1";
              this.title_text = "Forgot Password";
              this.lbl_text = "To reset your password, please enter your email address";
              this.btn_text = "Send Request";
          }
      });
  }

  send() {
    if(this.current_step === "step1") {
        this.requestCode();

    } else if(this.current_step === "step2") {
        this.verifyCode();
    } else {
        this.resetPassword();
    }
  }

  presentToast(msg) {
      this.toastCtrl.create({
          message: msg,
          duration: 2000,
          position: "top"
      }).present();
  }

  requestCode() {
      if(!this.email_address) {
          this.presentToast("Please fill-up all necessary fields");

      } else {
          let formData = {
              "email": this.email_address
           };

           this.userProvider.requestValidationCode(formData).then(res => {
                if(res['error'] === 0) {
                    console.log("res", res);

                    let sess_data = {
                      "token": res['token'],
                      "email": formData['email']
                    };

                    this.storage.set("sess_forgot_pass_email", sess_data).then(sess_forgot_pass_email => {
                        this.current_step = "step2";
                        this.title_text = "Validate Code";
                        this.lbl_text = "We have sent you an email to your email address with the verification code.";
                        this.btn_text = "Verify";

                          let alert = this.alertCtrl.create({
                            title: 'Validate Code',
                            message: 'We have sent you an email to your email address with the verification code.',
                            buttons: [{
                                text: "Okay"
                            }]
                          });
                          alert.present(); 
                    });
                } else {
                  this.presentToast(res['message']);
                }
           });
      }
  }

  verifyCode() {
      if(!this.verification_code) {
          this.presentToast("Please fill-up all necessary fields");

      } else {
          this.storage.get("sess_forgot_pass_email").then(sess_forgot_pass_email => {
              let formData = {
                "code": this.verification_code,
                "token": sess_forgot_pass_email['token']
              };

              this.userProvider.validateValidationCode(formData).then(res => {
                  if(res['error'] === 0) {
                      this.current_step = "step3";
                      this.title_text = "Reset Password";
                      this.lbl_text = "Enter your new password below.";
                      this.btn_text = "Save Password";
                  } else {
                      this.presentToast(res['message']);
                  }
              });
          });
      }
  }


  resetPassword() {
      if(!this.new_password) {
          this.presentToast("Please fill-up all necessary fields");

      } else {
          if(this.new_password.length > 9) {
              this.storage.get("sess_forgot_pass_email").then(sess_forgot_pass_email => {
                  let formData = {
                    "code": this.verification_code,
                    "email": this.email_address,
                    "password": this.new_password,
                    "token": sess_forgot_pass_email['token']
                  };

                  this.userProvider.resetPasswordWithCode(formData).then(res => {
                      if(res['error'] === 0) {
                          let alert = this.alertCtrl.create({
                            title: 'Success',
                            message: 'Your password has been changed successfully.',
                            buttons: [{
                                text: "Continue to Login",
                                handler: () => {
                                    this.storage.get("sess_forgot_pass_email").then(resetres => {
                                        this.navCtrl.pop(); 
                                      });
                                  }
                            }]
                          });
                          alert.present(); 
                      } else {
                          this.presentToast(res['message']);
                      }
                  });
              });

          } else {
              this.presentToast("Password must be atleast ten (10) characters");
          }
      }
  }

  togglePassword() {
    if(this.passwordStatus == "hide") {
      this.passwordStatus = "show";
      this.passwordType = "text";
      this.passwordIcon = "ios-eye-off-outline";
    } else {
      this.passwordStatus = "hide";
      this.passwordType = "password";
      this.passwordIcon = "ios-eye";
    }
  }

  changeEmailAddress() {
    let loader = this.loadingCtrl.create();
    loader.present();
    
    this.storage.remove("sess_forgot_pass_email").then(res => {
        this.current_step = "step1";
        this.title_text = "Forgot Password";
        this.lbl_text = "To reset your password, please enter your email address";
        this.btn_text = "Send Request";
        loader.dismiss();
    });
  }

}

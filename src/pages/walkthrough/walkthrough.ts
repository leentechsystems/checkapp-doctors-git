import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';

/**
 * Generated class for the WalkthroughPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {
  @ViewChild(Slides) slides: Slides;
  btn_label = "Next";
  currentIndex = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalkthroughPage');
  }

  slideChanged() {
	    this.currentIndex = this.slides.getActiveIndex();

	   	if(this.currentIndex >= 3) {
	   		  this.btn_label = "Let's get started";
	   	} else {
	   		  this.btn_label = "Next";
	   	}
  }

  nextSlide() {
  	if(this.currentIndex < 3) {
  		this.slides.slideNext();
  	} else {
  		this.navCtrl.push(LoginPage);
  	}
  }

}

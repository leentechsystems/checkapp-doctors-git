import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SiteProvider } from '../../providers/site/site';
import { UserProvider } from '../../providers/user/user';

import { Storage } from '@ionic/storage';

/**
 * Generated class for the PromptModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-prompt-modal',
  templateUrl: 'prompt-modal.html',
})
export class PromptModalPage {

	public invitationForm: FormGroup;
	userData: any;
	submitAttempt: boolean = false;
	public base_url = "";
	message_type: any;
	email: any;

	service_type: any;
	services: any;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
				public viewCtrl: ViewController, public toastCtrl: ToastController, public siteProvider: SiteProvider,
				private storage: Storage, public userProvider: UserProvider, public loadingCtrl: LoadingController) {

		this.message_type = this.navParams.get('message_type');

		if(this.message_type == 'message_support'){
			this.email = this.navParams.get('support_email');
			this.userData = this.navParams.get('userData');

			this.invitationForm = formBuilder.group({
				user_id: this.userData.id,
				subject: ['', Validators.compose([Validators.required])],
				message: ['', Validators.compose([Validators.required])]
			});
		}else if(this.message_type == 'see_all_services'){
			this.service_type = this.navParams.get('service_type');
			this.services = this.navParams.get('services');

		}
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad PromptModalPage');
  	}

	escapeModal() {
		this.viewCtrl.dismiss();
	}

    stopPropagation(e) {
      e.stopPropagation();
    }

	sendMessage(){
		console.log(this.invitationForm.value);
		let loader = this.loadingCtrl.create({
			content: 'Sending...'
		});
  		if(!this.invitationForm.valid){
			this.showToast("Please check all input fields");

		}else{
			loader.present();

			this.userProvider.sendSupportMessage(this.invitationForm.value).then(data => {
				console.log(data);
				if(data['error'] === 0){
					loader.dismiss();
					this.viewCtrl.dismiss('message');
				}
				else{
					loader.dismiss();
					this.siteProvider.showToast(data['message']);
				}
			});
		}
	}

 	showToast(msg) {
	    const toast = this.toastCtrl.create({
	      	message: msg,
	      	showCloseButton: false,
    	  	duration: 3000,
	      	position: 'top'
	    });
	    toast.present();
	}
}

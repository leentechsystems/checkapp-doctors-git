import { Component } from '@angular/core';
import { NavController, ModalController, AlertController } from 'ionic-angular';

import { PromptModalPage } from '../../pages/prompt-modal/prompt-modal';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';
import { CallNumber } from '@ionic-native/call-number';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/take'
import 'rxjs/add/observable/forkJoin';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

	contact_mobile: any;
	contact_landline: any;
	contact_email: any;
	userData: any;

  	constructor(public navCtrl: NavController, public siteProvider: SiteProvider, private storage: Storage,
  				public callNumber: CallNumber, public modalCtrl: ModalController, public alertCtrl: AlertController) {
  		
		this.storage.get("sess_user_login").then((data) => {
			this.userData = data;
		});
  	}

  	ionViewDidEnter(){

  		this.storage.get("sess_contact").then(contact => {
  			console.log(contact);
  			if(contact){
				this.contact_mobile = contact.mobile;
				this.contact_landline = contact.landline;
				this.contact_email = contact.email;
  			}else{
  				this.getContact();
  			}
  		});
  	}

	getContact(){
		Observable.forkJoin([
		  	this.siteProvider.getContact('mobile'),
		  	this.siteProvider.getContact('landline'),
		  	this.siteProvider.getContact('email'),
		]).subscribe(res => {
		  	console.log(res);
		  	if(res[0]["error"] != 1 && res[1]["error"] != 1 && res[2]["error"] != 1){
				let contact = {
					mobile : res[0]['items']['value'],
					landline: res[1]['items']['value'],
					email: res[2]['items']['value'],
				};

		  		console.log(contact);
	            this.storage.set("sess_contact", contact).then(contact => {
					this.contact_mobile = contact.mobile;
					this.contact_landline = contact.landline;
					this.contact_email = contact.email;
	            });
		  	}
		});
	}

	makeCall(thisnumber){
		this.callNumber.callNumber(thisnumber, true)
		.then(res => console.log('Launched dialer!', res))
		.catch(err => console.log('Error launching dialer', err));
	}

  	sendMessage(){

    	let doneModal = this.modalCtrl.create(PromptModalPage, {
            message_type: "message_support",
            support_email: this.contact_email,
            userData: this.userData,
        });
    	doneModal.onDidDismiss(data => {
	        if(data){
	          	this.showAlert("Successfully Sent!", "Check later your email inbox if the checkApp support reply to your message");                      
	       	}
	    });

    	doneModal.present();
  	}

  	showAlert(title, msg) {

    	let alert = this.alertCtrl.create({
     	 	title: title,
      		subTitle: msg,
        	buttons: [{ 
          		text: "Okay", 
          		handler: () => {
              		this.navCtrl.pop();
          		}
        	}]
    	});
    	alert.present();
  	}

}

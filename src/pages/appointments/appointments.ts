import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Calendar } from '@ionic-native/calendar';

import { AppointmentsScheduledPage } from './../appointments-scheduled/appointments-scheduled';
import { AppointmentsApprovalPage } from './../appointments-approval/appointments-approval';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { DoctorProvider } from '../../providers/doctor/doctor';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the AppoinmentsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-appointments',
	templateUrl: 'appointments.html',
})
export class AppointmentsPage {

	date: any;
	daysInThisMonth: any;
	daysInLastMonth: any;
	daysInNextMonth: any;
	monthNames: string[];
	currentMonth: any;
	currentYear: any;
	currentDate: any;
	currentMonthIdx: any;
	eventList: any;
	selectedEvent: any;
	isSelected: any;

	hospital_tabs: string = '';
	dayshortNames = [];
	date_selected: any;
	selected_day: any;
	selected_month: any;
	selected_year: any;
	today_year: any;
	today_month: any;
	today_day: any;
	currentDateSchedule: any;

	scheduledAppointment_page = 1;
	scheduledAppointment: any;
	total_scheduledAppointment = 0;

	forApprovalAppointment_page = 1;
	forApprovalAppointment: any;
	total_forApprovalAppointment = 0;

	appointmentDate: any;
	temp_date: any;
	hospital_id= [];
	temp_hospital_id= [];
	today: any;
	all_upcoming_scheduled = [];
	all_upcoming_approve = [];
	from_appointment = (this.navParams.get('appointment')) ? this.navParams.get('appointment') : null;

	hospitalList: any;
	approval_appointment_hospitals_id = [];

	constructor(public navCtrl: NavController, public navParams: NavParams, private calendar: Calendar, public appointmentProvider: AppointmentProvider,
				public doctorProvider: DoctorProvider, public siteProvider: SiteProvider) {

		this.today = new Date();
		this.today_month = this.today.getMonth() + 1;
		this.today_day = this.today.getDate();
		this.today_year = this.today.getFullYear();
		if(this.today_month < 10){
			this.today_month = '0'+this.today_month;
		}
		if(parseInt(this.today_day) < 10){
			this.today_day = '0'+this.today_day.toString();
		}

		if(this.from_appointment == 'approval'){
			this.hospital_tabs = 'approval';
		}else{
			this.hospital_tabs = 'scheduled';
		}

		this.appointmentDate = null;/*
		this.all_upcoming_scheduled = [];
		this.all_upcoming_approve = [];*/
		this.selected_day = null;
		this.date = new Date();
		this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		this.dayshortNames = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
		this.getDoctorsHospitals();
		this.loadDoctorsSchedule();
		this.getDaysOfMonth();
		this.loadEventThisMonth();
		this.loadAllData(false, true);
		this.selectDate(this.selected_day, 'fromEnter');
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AppointmentsPage');
	}

	ionViewWillEnter() {
		// this.date_selected = [];
		// this.appointmentDate = [];/*
		// this.all_upcoming_scheduled = [];
		// this.all_upcoming_approve = [];*/
		// this.selected_day = null;
		// this.date = new Date();
		// this.monthNames = ["January","February","March","April","May","June","July","August","September","October","November","December"];
		// this.dayshortNames = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"];
		// this.getDoctorsHospitals();
		// this.loadDoctorsSchedule();
		// this.getDaysOfMonth();
		// this.loadEventThisMonth();
		// this.loadAllData(false, true);
		// this.selectDate(this.selected_day, 'fromEnter');
	}

	getDaysOfMonth() {
		this.daysInThisMonth = new Array();
		this.daysInLastMonth = new Array();
		this.daysInNextMonth = new Array();
		this.currentMonth = this.monthNames[this.date.getMonth()];
		this.currentYear = this.date.getFullYear();
		this.currentMonthIdx = this.date.getMonth()+1;
		if(this.date.getMonth() === new Date().getMonth()) {
			this.currentDate = new Date().getDate();
		} else {
			this.currentDate = 999;
		}

		if(this.currentDate < 10){
			this.selected_day = '0'+this.currentDate;
		}else{
			this.selected_day = this.currentDate;
		}

		if(this.currentMonthIdx < 10){
			this.selected_month = '0'+this.currentMonthIdx;
		}else{
			this.selected_month = this.currentMonthIdx;
		}

		this.currentDateSchedule = this.currentYear+'-'+this.selected_month+'-'+this.selected_day;

		var firstDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth(), 1).getDay();

		var prevNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth(), 0).getDate();
		for(var i = prevNumOfDays-(firstDayThisMonth-1); i <= prevNumOfDays; i++) {
			this.daysInLastMonth.push(i);
		}

		var thisNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDate();
		for (var j = 0; j < thisNumOfDays; j++) {
			this.daysInThisMonth.push(j+1);
		}

		var lastDayThisMonth = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0).getDay();
		// var nextNumOfDays = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0).getDate();
		for (var k = 0; k < (6-lastDayThisMonth); k++) {
			this.daysInNextMonth.push(k+1);
		}
		var totalDays = this.daysInLastMonth.length+this.daysInThisMonth.length+this.daysInNextMonth.length;
		if(totalDays<36) {
			for(var l = (7-lastDayThisMonth); l < ((7-lastDayThisMonth)+7); l++) {
				this.daysInNextMonth.push(l);
			}
		}
	}

	goToLastMonth() {
		this.date_selected = [];
		this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
		this.getDaysOfMonth();
	}

	goToNextMonth() {
		this.date_selected = [];
		this.date = new Date(this.date.getFullYear(), this.date.getMonth()+2, 0);
		this.getDaysOfMonth();
	}

	/*addEvent() {
		this.navCtrl.push(AddEventPage);
	}*/

	loadEventThisMonth() {
		this.eventList = new Array();
		var startDate = new Date(this.date.getFullYear(), this.date.getMonth(), 1);
		var endDate = new Date(this.date.getFullYear(), this.date.getMonth()+1, 0);
		this.calendar.listEventsInRange(startDate, endDate).then(
			(msg) => {
				msg.forEach(item => {
					this.eventList.push(item);
				});
			},
			(err) => {
				console.log(err);
			}
		);
	}

	loadDoctorsSchedule(){

		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"day": 'upcomming'
		};

		this.appointmentProvider.getDoctorsAppointments(true, thisData).then(data => {
			if(data['error'] == 0){
				data['appointments']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						let pieces = cat.appointment_date.split(" ");
						let wordCount = pieces[0].split("-");
						let timeCount = pieces[1].split(":");
						let date_target_year = wordCount[0];
						let date_target_month = wordCount[1];
						let date_target_day = wordCount[2];
						let date_target_hour = timeCount[0];
						let date_target_minutes = timeCount[1];

						let mlist = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
						let convert_appointment_date = new Date(date_target_year, parseInt(date_target_month)-1, date_target_day, date_target_hour, date_target_minutes);

						/* Convertion to 12-hour time */
						let new_selected_hour = "";
						let new_selected_time_unit = "";
						if(parseInt(date_target_hour) > 12) {
							new_selected_time_unit = "PM";
							let new_selected_hour1 = parseInt(date_target_hour) - 12;

							if(new_selected_hour1 < 10) {
								new_selected_hour = "0" + new_selected_hour1.toString();
							} else {
								new_selected_hour = new_selected_hour1.toString();
							}

						} else {
								new_selected_time_unit = "AM";
								new_selected_hour = date_target_hour;
						}

						cat['appointment_date_day'] =  date_target_day;
						cat['appointment_date_month'] =  mlist[date_target_month - 1];
						cat['appointment_date_year'] =  parseInt(date_target_year);
						cat['appointment_date_time'] =  new_selected_hour +":"+ date_target_minutes + "" + new_selected_time_unit;


						let wordDate1 =  cat['appointment_date'].split(" ");

						if(wordDate1[0] >= this.currentDateSchedule) {

							if((cat['status'] == '1' || cat['status'] == '3' || cat['status'] == '4') && (cat['hmo_transactions_status'] == '1' || cat['hmo_transactions_status'] == '3')){
									this.all_upcoming_scheduled.push(date_target_year+"-"+date_target_month+"-"+date_target_day);
							}
							else if((cat['status'] == '2' || cat['hmo_transactions_status'] == '2') || (cat['status'] == '0' || cat['hmo_transactions_status'] == '0')){
									this.all_upcoming_approve.push(date_target_year+"-"+date_target_month+"-"+date_target_day);
							}
						}
					}
				});
			}
		});
	}

	checkEvent(day) {
		var hasEvent = false;
		var thisDate1 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 00:00:00";
		var thisDate2 = this.date.getFullYear()+"-"+(this.date.getMonth()+1)+"-"+day+" 23:59:59";
		this.eventList.forEach(event => {
			if(((event.startDate >= thisDate1) && (event.startDate <= thisDate2)) || ((event.endDate >= thisDate1) && (event.endDate <= thisDate2))) {
				hasEvent = true;
			}
		});
		return hasEvent;
	}

	getDoctorsHospitals(){
		this.doctorProvider.getDoctorsHospitalList().then(data => {
			if(data['error'] == 0){
				this.hospitalList = data['hospitals']['items'];
				console.log('hospital_list', this.hospitalList);
			}
			console.log(data);
		});

	}


	loadAllData(infiniteScroll?, isLoadNew?) {
		if(isLoadNew = true) { /* if refreshed */
			this.scheduledAppointment_page = 1; /* reset the current page to default */
			this.forApprovalAppointment_page = 1; /* reset the current page to default */
			this.scheduledAppointment = null;
			this.forApprovalAppointment = null;
		}

		/* - load functions responsible on getting and processing the data
		- we'll pass refresher so we will stop the refresher event after processing the data NOT promptly
		- we'll pass infiniteScroll so we will stop the infiniteScroll event after processing the data NOT promptly */

		this.loadDoctorsScheduledAppointment(isLoadNew, infiniteScroll);
		this.loadDoctorsApprovalAppointment(isLoadNew, infiniteScroll);
	}

	loadDoctorsApprovalAppointment(isLoadNew?, infiniteScroll?){
		if(this.appointmentDate){
			this.temp_date = this.appointmentDate;
		}else{
			this.temp_date = this.currentDateSchedule;
		}

		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.forApprovalAppointment_page,
			"day": 'today',
			"date": this.temp_date,
			"category": 'approval'
		};

		this.appointmentProvider.getDoctorsAppointments(isLoadNew, thisData).then(data => {
			if(data['error'] === 0) {
				this.total_forApprovalAppointment = data['appointments']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.forApprovalAppointment || isLoadNew === true) {
					 	this.forApprovalAppointment = [];
						this.forApprovalAppointment_page = 1;
					}
				}

				data['appointments']['items'].forEach((cat, idx) => {
					if(cat['status'] == '2' || cat['hmo_transactions_status'] == '2' || cat['status'] == '0' || cat['hmo_transactions_status'] == '0') {
						this.forApprovalAppointment.push(cat);
						this.approval_appointment_hospitals_id.push(cat['hospital_id']);
					}
				});

				if(this.total_scheduledAppointment != 0){
					this.hospital_tabs = 'scheduled';
				}else{
					this.hospital_tabs = 'approval';
				}

				console.log(this.forApprovalAppointment);

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}
		});
	}

	loadDoctorsScheduledAppointment(isLoadNew?, infiniteScroll?){
		if(this.appointmentDate){
			this.temp_date = this.appointmentDate;
		}else{
			this.temp_date = this.currentDateSchedule;
		}

		/* getting of data from the provider, we'll passing set of data the function is requiring */
		let thisData = {
			"current_page": this.scheduledAppointment_page,
			"day": 'today',
			"date": this.temp_date,
			"category": 'scheduled'
		};

		this.appointmentProvider.getDoctorsAppointments(isLoadNew, thisData).then(data => {
			if(data['error'] === 0) {
				this.total_scheduledAppointment = data['appointments']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					/* converting into array if variable is empty */
					/* emptying the variable when refreshed */
					if(!this.scheduledAppointment || isLoadNew === true) {
						this.scheduledAppointment = [];
						this.scheduledAppointment_page = 1;
					}
				}

				data['appointments']['items'].forEach((cat, idx) => {
					if(((cat['status'] == '1' || cat['status'] == '4') && cat['hmo_transactions_status'] == '1') || (cat['status'] == '3' && cat['hmo_transactions_status'] == '3')){
						this.scheduledAppointment.push(cat);
					}
				});

				if(this.total_scheduledAppointment != 0){
					this.hospital_tabs = 'scheduled';
				}

				if(infiniteScroll) {
					infiniteScroll.complete();  /* after getting and assigning data to variables, remove infiniteScroll loader */
				}
			}
		});
	}

	doInfinite(infiniteScroll:any, thisType) {

		if(thisType === "approval") {
			this.forApprovalAppointment_page += 1;
			if(this.forApprovalAppointment.length < this.total_forApprovalAppointment) {
				this.loadDoctorsApprovalAppointment(false, infiniteScroll);
			} else {
				infiniteScroll.enable(false);
			}
		} else if(thisType === "scheduled") {
			this.scheduledAppointment_page += 1;
			if(this.scheduledAppointment.length < this.total_scheduledAppointment) {
				this.loadDoctorsScheduledAppointment(false, infiniteScroll);
			} else {
				infiniteScroll.enable(false);
			}
		}
	}

	removeDuplicates(arr){
		let unique_array = []
		for(let i = 0;i < arr.length; i++){
			if(unique_array.indexOf(arr[i]) == -1){
					unique_array.push(arr[i])
			}
		}
		return unique_array
	}

	gotoScheduled(temp_appointment){
		this.navCtrl.push(AppointmentsScheduledPage, {
			"temp_appointment": temp_appointment,
			"temp_date": this.temp_date
		});
	}

	gotoApproval(temp_appointment, temp_hospital){
	 	console.log('temp_hospital', temp_hospital);
	 	console.log('temp_appointment', temp_appointment);

	 	let appointments = [];
	 	temp_appointment.forEach((cat, idx) => {
		 	if(temp_hospital.id == cat.hospital_id){
		 		appointments.push(cat);
		 	}
	 	});

		if(this.appointmentDate){
			this.temp_date = this.appointmentDate;
		}else{
			this.temp_date = this.currentDateSchedule;
		}

		this.navCtrl.push(AppointmentsApprovalPage, {
			"temp_appointment": appointments,
			"temp_hospital_id": temp_hospital.id,
			"temp_date": this.temp_date
		});
	}

	selectDate(day, action){
		if(action == 'fromEnter'){
			this.selected_day = day;
		}else{
			if(day < 10){
				this.selected_day = '0'+day;
			} else {
				this.selected_day = day;
			}
		}

		if(this.currentMonthIdx < 10){
			this.selected_month = '0'+this.currentMonthIdx;
		}else{
			this.selected_month = this.currentMonthIdx;
		}

		this.selected_year = this.currentYear;
		this.appointmentDate = this.selected_year+'-'+this.selected_month+'-'+this.selected_day;

		let thisDay123 = this.today_year +"-"+this.today_month+"-"+this.today_day;
		console.log(this.appointmentDate +'>='+ thisDay123);
		if(this.appointmentDate >= thisDay123) {
			this.scheduledAppointment = null;
			this.forApprovalAppointment = null;
			this.approval_appointment_hospitals_id = [];
			this.loadAllData(false, true);

		}else{

			this.scheduledAppointment = [];
			this.forApprovalAppointment = [];
			this.approval_appointment_hospitals_id = [];
			this.total_scheduledAppointment = 0;
			this.total_forApprovalAppointment = 0;
		}
		this.date_selected = day;
	}

	checkIfAppointment(day, month, year, action) {
		month = parseInt(month) + 1;

		if(month < 10) {
			month = "0" + month.toString();
		}

		if(day < 10) {
			day = "0" + day.toString();
		}

		let assemble_date = year + "-" + month + "-" + day;
		if(action == 'approve'){
			if(this.all_upcoming_approve.indexOf(assemble_date) > -1){
				return "yes";
			} else {
				return "none";
			}
		}else{
			if(this.all_upcoming_scheduled.indexOf(assemble_date) > -1){
				return "yes";
			} else {
				return "none";
			}
		}
	}

	checkIfHospital(hospital_id) {

		if(this.approval_appointment_hospitals_id.indexOf(hospital_id) > -1){
			return "yes";
		} else {
			return "none";
		}
	}

	checkAvatarQuantity(){

	}
}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BrowseActivityLogsPage } from './browse-activity-logs';

@NgModule({
  declarations: [
    BrowseActivityLogsPage,
  ],
  imports: [
    IonicPageModule.forChild(BrowseActivityLogsPage),
  ],
})
export class BrowseActivityLogsPageModule {}

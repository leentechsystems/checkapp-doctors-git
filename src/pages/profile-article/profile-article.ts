import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ToastController, AlertController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { ModalInfoPage } from '../../pages/modal-info/modal-info'
import { ArticleProvider } from '../../providers/article/article';
import { UserProvider } from '../../providers/user/user';
import { SiteProvider } from '../../providers/site/site';
import { SocialSharing } from '@ionic-native/social-sharing';
import { LinkedIn } from '@ionic-native/linkedin';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { AppAvailability } from '@ionic-native/app-availability';

/**
 * Generated class for the ProfileArticlePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-article',
  templateUrl: 'profile-article.html',
})
export class ProfileArticlePage {

  	related_article: any;
  	article_id: any;
  	articleProfile = this.navParams.get('articleProfile');
  	total_count = 0;
  	current_page = 1;
  	all_articles: any;
  	unregisterBackButtonAction: any;
  	facebook_app: any;
  	facebook_url: any;
  	twitter_app: any;
  	twittern_url: any;
  	linkedin_app: any;
  	linkedin_url: any;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public articleProvider: ArticleProvider,
		  		private storage: Storage, public modalCtrl: ModalController, public socialSharing: SocialSharing,
		  		public loadingCtrl: LoadingController, public toastCtrl: ToastController, public linkedin: LinkedIn,
		  		public alertCtrl: AlertController, public inAppBrowser: InAppBrowser, public platform: Platform,
		  		private appAvailability: AppAvailability, public userProvider: UserProvider, public siteProvider: SiteProvider) {

	  	this.related_article = this.articleProfile.category.id;
	  	this.article_id = this.articleProfile.id;

	  	this.loadAllArticles(false,true,false);

	  	if (this.platform.is('ios')) {
		  	this.twitter_app = 'twitter://';
		  	this.twittern_url = 'https://itunes.apple.com/ph/app/twitter/id333903271?mt=8';
		  	this.facebook_app = 'facebook://';
		  	this.facebook_url = 'https://itunes.apple.com/us/app/facebook/id284882215?mt=8';
		  	this.linkedin_app = 'linkedin://';
		  	this.linkedin_url = 'https://itunes.apple.com/ph/app/linkedin/id288429040?mt=8';
		} else if (this.platform.is('android')) {
		  	this.twitter_app = 'com.twitter.android';
		  	this.twittern_url = 'https://play.google.com/store/apps/details?id=com.twitter.android';
		  	this.facebook_app = 'com.facebook.katana';
		  	this.facebook_url = 'https://play.google.com/store/apps/details?id=com.facebook.katana&hl=en';
		  	this.linkedin_app = 'com.linkedin.android';
		  	this.linkedin_url = 'https://play.google.com/store/apps/details?id=com.linkedin.android';
		}

		console.log('articleProfile',this.articleProfile);
  	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad ProfileArticlePage');
	    this.initializeBackButtonCustomHandler();
  	}

	initializeBackButtonCustomHandler(){
	    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(function(event){
	        console.log('Prevent Back Button Page Change', event);
	    }, 101);
	}

   	loadAllArticles(refresher?, isLoadNew?, infiniteScroll?) {

	  	let thisData = {
			"current_page": this.current_page,
			"related_article": this.related_article
	  	};

	  	this.articleProvider.getAllRelatedArticles(isLoadNew, thisData).then(data => {
		  	console.log(data);
		  	this.total_count = data['articles']['total_count'];

			  	if(!this.all_articles || isLoadNew === true) {
				 	this.all_articles = [];
			  	}

		  	data['articles']['items'].forEach((cat, idx) => {
				if(cat.id !== undefined) {
					this.all_articles.push(cat);
				}
		  	});

		  	console.log('this.all_articles',this.all_articles);

		  	if(refresher) {
				refresher.complete(); /* after getting and assigning data to variables, remove refresher loader */
		  	}
	  	});
  	}

  	goToArticleInside(articleProfile){
		this.navCtrl.push(ProfileArticlePage,{
			articleProfile : articleProfile
		});
  	}

  	shareSocialMedia(){
		let shareModal = this.modalCtrl.create(ModalInfoPage,{
			modal_type: 'socialmedia_share'
		});

		shareModal.onDidDismiss(data => {

		  	if(data){
		  		console.log(data);
				if(data == "facebook") {

			  		this.appAvailability.check(this.facebook_app).then(
					    (yes) => { this.shareFacebook();
					    },
					    (no) => { this.downloadApp(data, this.facebook_url);
						}
					);

				}else if(data == "linkedin"){

			        /*let loader = this.loadingCtrl.create({content: "Linkedin Sharing ..."});
			            loader.present();
						this.socialSharing.shareVia('com.linkedin.android', this.articleProfile.title, this.articleProfile.content, this.articleProvider.base_url + this.articleProfile.attachments.filepath +'/'+ this.articleProfile.attachments.filename).then(() => {
							console.log("success");
			        		loader.dismiss();
						}).catch((err) => {

						this.presentToast('Something went wrong. Download LinkedinApp');
			        	loader.dismiss();
					});*/
			  		this.appAvailability.check(this.linkedin_app).then(
					    (yes) => { this.shareLinkedin();
					    },
					    (no) => { this.downloadApp(data, this.linkedin_url);
					    }
					);

				}else if(data == "twitter"){

			  		this.appAvailability.check(this.twitter_app).then(
					    (yes) => { this.shareTwitter();
					    },
					    (no) => { this.downloadApp(data, this.twittern_url);
					    }
					);
				}
		  	}
		});
		shareModal.present();
  	}

  	shareFacebook(){
		let loader = this.loadingCtrl.create({content: "Facebook Sharing..."});
		loader.present();

		this.socialSharing.shareViaFacebook(this.articleProfile.title, this.siteProvider.base_url + this.articleProfile.attachments.filepath +'/'+ this.articleProfile.attachments.filename, this.siteProvider.url).then(() => {
			console.log("success");
			loader.dismiss();
		}).catch((err) => {
			loader.present();
			this.presentToast('Something went wrong. Please try again.');
		});
  	}

  	shareTwitter(){
  		let loader = this.loadingCtrl.create({content: "Twitter Sharing ..."});
  		loader.present();

  		this.socialSharing.shareViaTwitter(this.articleProfile.title, this.siteProvider.base_url + this.articleProfile.attachments.filepath +'/'+ this.articleProfile.attachments.filename, this.siteProvider.url).then(() => {
			console.log("success");
			loader.dismiss();
  		}).catch((err) => {

			loader.dismiss();
			this.presentToast('Something went wrong. Please try again.');
  		});
  	}

  	shareLinkedin(){
  		let loader = this.loadingCtrl.create({content: "Linkedin Sharing ..."});
  		loader.present();

  		// check if there is an active session
  		this.linkedin.hasActiveSession().then((active) => {
			console.log('has active session?', active);
			if(active == false){

	  			// login
	  			var scopes:any = ['r_basicprofile', 'r_emailaddress', 'rw_company_admin', 'w_share'];
	  			this.linkedin.login(scopes, true).then((res) => {
		  			console.log('Logged in!', res);

		  			// get connections
		  			this.linkedin.getRequest('people/~').then((log) =>{
						console.log('Logged data!',log);

						// share something on profile
						const body = {
						  	//comment: "Check out developer.linkedin.com!",
							content: {
							    title: this.articleProfile.title,
							    description: this.articleProfile.content,
							    "submitted-url": this.siteProvider.url,
								"submitted-image-url": this.siteProvider.base_url + this.articleProfile.attachments.filepath +'/'+ this.articleProfile.attachments.filename
							},
							visibility: {
							    code: "anyone"
							}
						};

						this.linkedin.postRequest('people/~/shares', body).then(res => {
			  				console.log(res);
  							loader.dismiss();
		  					this.showAlert(res.updateUrl);

		  				}).catch((e) => {
  							loader.dismiss();
			  				this.presentToast('Something went wrong. Please Try Again')
			  			});

		  			}).catch((e) => {
  						loader.dismiss();
		  				console.log('error data', e);
		  			});

				}).catch((e) => {
  					loader.dismiss();
				});

			}else{

	  			// get connections
	  			this.linkedin.getRequest('people/~').then((log) => {
					console.log('Logged data!',log);
					console.log(log.siteStandardProfileRequest.url);
					// share something on profile
					const body = {
						comment: this.articleProfile.content,
						content: {
							title: this.articleProfile.title,
							description: this.articleProfile.content,
							"submitted-url": this.siteProvider.base_url + this.articleProfile.attachments.filepath +'/'+ this.articleProfile.attachments.filename,
							"submitted-image-url": this.siteProvider.base_url + this.articleProfile.attachments.filepath +'/'+ this.articleProfile.attachments.filename
						},
						visibility: {
							code: "anyone"
						}
					};

					this.linkedin.postRequest('people/~/shares', body).then((res) => {
		  				console.log(res);
  						loader.dismiss();
		  				this.showAlert(res.updateUrl);

		  			}).catch((e) => {
  						loader.dismiss();
		  				this.presentToast('Something went wrong. Please Try Again')
		  			});

	  			}).catch((e) => {
  					loader.dismiss();
	  				this.presentToast('Something went wrong. Please try again.')
	  			});
			}
  		});
  	}

  	showAlert(url) {
    	let alert = this.alertCtrl.create({
			title: 'Success',
			subTitle: this.articleProfile.title +' has been shared successfully!',
			cssClass: 'AcceptBookingAlert',
			buttons: [{
				text: 'Skip',
				role: 'cancel',
				  	handler: () => {
				        console.log('Cancel clicked');

				    }
				},
				{
				text: 'Open',
					handler: () => {
				        console.log('Open clicked');
  						// let url = 'https://'+this.socialMedia.facebook;
				  		console.log(url);
				        this.inAppBrowser.create(url, "_system", "location=true");
				    }
			}]
		});
    	alert.present();
  	}

  	presentToast(msg) {
		this.toastCtrl.create({
	  		message: msg,
	  		duration: 2000,
	  		position: "top"
		}).present();
  	}

	downloadApp(socialMedia, socialMediaUrl){

    	let alert = this.alertCtrl.create({
			title: 'Oh no!',
			subTitle: 'Something went terribly wrong, Make sure you have '+socialMedia+' App',
			cssClass: 'AcceptBookingAlert',
			buttons: [{
				text: 'Not now',
				role: 'cancel',
				  	handler: () => {
				        console.log('Cancel clicked');

				    }
				},
				{
				text: 'Download now',
					handler: () => {
				        console.log('Open clicked');
				        this.inAppBrowser.create(socialMediaUrl, "_system", "location=true");
				    }
			}]
		});
    	alert.present();
	}

  	addtoBookmark(temp_article, article_action, article_index = null){
	    console.log('article',temp_article);
	    console.log('action',article_action);
	    console.log('index',article_index);

	    let thisData = {
	      "item_id": temp_article.id,
	      "type": 'article',
	    };

	    if(temp_article.bookmark_id){
	      thisData['id'] = temp_article.bookmark_id;
	    }

	    //pandaya sa pag update ng bookmark
	    if(article_action == 'profile'){
	        if(temp_article.bookmark_id){
	    		this.articleProfile.bookmark_id = null;
	        }else{
	    		this.articleProfile.bookmark_id = 1;
	        }

	    }else{
		    this.all_articles.forEach((cat, idx) => {
		      if(article_index == idx){
		        if(temp_article.bookmark_id){
		          cat.bookmark_id = null;
		        }else{
		          cat.bookmark_id = 1;
		        }
		      }
		    });
	    }

	    this.userProvider.saveBookmarks(thisData).then((data) => {

	      	if(data['error'] === 0){
			    if(article_action == 'profile'){
			        if(thisData['id']){
			    		this.articleProfile.bookmark_id = null;
			        }else{
			    		this.articleProfile.bookmark_id = data['result']['id'];
			        }

			    }else{

				    this.all_articles.forEach((cat, idx) => {
				      	if(article_index == idx){
				            if(thisData['id']){
				              cat.bookmark_id = null;
				            }else{
				              cat.bookmark_id = data['result']['id'];
				            }
				      	}
				    });
			    }
	      	}

	    }, err => {

	      console.log(err);

	    });
  	}
}

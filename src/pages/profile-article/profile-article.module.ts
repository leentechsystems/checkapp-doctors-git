import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfileArticlePage } from './profile-article';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    ProfileArticlePage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(ProfileArticlePage),
  ],
})
export class ProfileArticlePageModule {}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController} from 'ionic-angular';

import { PatientsProfilePage } from './../patients-profile/patients-profile';

import { UserProvider } from '../../providers/user/user';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the AcceptApprovalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-accept-approval',
  templateUrl: 'accept-approval.html',
})
export class AcceptApprovalPage {

  patient_schedule1 = this.navParams.get('temp_patient_schedule');
  patient_schedule : any;
  schedule_date = this.navParams.get('temp_date');
  origin_page = (this.navParams.get('origin_page')) ? this.navParams.get('origin_page') : null;
  from_notif = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;
  patient_profile: any;
  patient_id: any;
  appointment_id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public userProvider: UserProvider,
              public siteProvider: SiteProvider,
              public appointmentProvider: AppointmentProvider,
              public viewCtrl: ViewController,
              public loadingCtrl: LoadingController,
              public photoViewer: PhotoViewer) {

    if(this.from_notif){
      this.appointment_id = this.patient_schedule1['appointment_id'];
      this.patient_id = this.patient_schedule1['patient_user_id']
    }else{
      this.appointment_id = this.patient_schedule1['id'];
      this.patient_id = this.patient_schedule1['patient_id']
    }

    this.getPatientAppoinment();
    this.getPatientProfile();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AcceptApprovalPage');
  }

  getPatientProfile(){
    console.log('patient_id',this.patient_id);
    this.userProvider.getPatientProfile(this.patient_id).then((data) => {
      this.patient_profile = data['user'];
    }, err => {
        console.log(err);
    });
  }


  getPatientAppoinment(){

    let thisData = {
      "day": 'today',
      "date": this.schedule_date
    };

    this.appointmentProvider.getDoctorsAppointments(true, thisData).then((data) => {
        console.log(data['appointments']['items']);

        data['appointments']['items'].forEach((cat, idx) => {

            if(this.appointment_id == cat['id']){

                this.patient_schedule = cat;
                this.patient_schedule['attachments_filename'] = this.patient_schedule1['attachments_filename'];
                this.patient_schedule['attachments_filepath'] = this.patient_schedule1['attachments_filepath'];

                let wordDate1 = cat['appointment_date'].split(" ");
                let wordCount1 = wordDate1[1].split(":");

                if(wordCount1[0] < 12){
                  cat['appointment_time'] = wordCount1[0]+':'+wordCount1[1]+' AM';

                }else{
                  if(wordCount1[0] == 12){
                    cat['appointment_time'] = '12'+':'+wordCount1[1]+' PM';
                  }else{
                    cat['appointment_time'] = (wordCount1[0]-12) +':'+wordCount1[1]+' PM';
                  }
                }

                let wordDate = new Date(this.patient_schedule['appointment_date']).toString();
                let wordCount = wordDate.split(" ");
                if(wordCount[0] == 'Sun'){
                    cat['appointment_week'] = 'Sunday';
                }
                else if(wordCount[0] == 'Mon'){
                    cat['appointment_week'] = 'Monday';
                }
                else if(wordCount[0] == 'Tue'){
                    cat['appointment_week'] = 'Thuesday';
                }
                else if(wordCount[0] == 'Wed'){
                    cat['appointment_week'] = 'Wednesday';
                }
                else if(wordCount[0] == 'Thu'){
                    cat['appointment_week'] = 'Thursday';
                }
                else if(wordCount[0] == 'Fri'){
                    cat['appointment_week'] = 'Friday';
                }
                else if(wordCount[0] == 'Sat'){
                    cat['appointment_week'] = 'Saturday';
                }

                this.patient_schedule = cat;
            }
        });

    }, err => {
       console.log(err);
    });
  }

  ViewProfile(){
    this.patient_schedule['appointment_status'] = this.patient_schedule['status'];

    console.log("temp_patient_appointment", this.patient_schedule);

    this.navCtrl.push(PatientsProfilePage, {
      "temp_patient_profile": this.patient_profile,
      "temp_patient_appointment" : this.patient_schedule,
      "origin_page": this.origin_page
    });
  }

  actionAppointment(thisAction){

    let thisData = {
        "id": this.appointment_id,
        "action": thisAction
    };

    let loader = this.loadingCtrl.create({content: "Processing..."});
    loader.present();

    this.appointmentProvider.approveAppointment(thisData).then((data) => {
        loader.dismiss();
        this.viewCtrl.dismiss(data['result']);

      }, err => {
        console.log(err);
    });
  }

  escapeModal() {
    this.viewCtrl.dismiss();
  }

  stopPropagation(e) {
    e.stopPropagation();
  }

  viewPhoto(attachment){
    console.log(attachment);
    this.photoViewer.show(attachment);
  }

}

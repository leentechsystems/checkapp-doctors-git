import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcceptApprovalPage } from './accept-approval';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    AcceptApprovalPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(AcceptApprovalPage),
    IonicImageLoader,
  ],
})
export class AcceptApprovalPageModule {}

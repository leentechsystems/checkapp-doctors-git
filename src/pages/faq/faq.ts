import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {

  	faqData: any;

  	constructor(public navCtrl: NavController, public navParams: NavParams, public siteProvider: SiteProvider, private storage: Storage) {

  		this.storage.get("sess_faq_doctor").then(sess_faq => {
	        if(sess_faq) {
	            this.faqData = sess_faq;
	        } else {
	            this.siteProvider.getCmsPageContent("faq_doctor").then((data) => {
	                if(data['error'] === 0) {
	                  this.storage.set("sess_faq_doctor", data['cmspage']['content']).then(cms => {
	                    this.faqData = data['cmspage']['content'];
	                  });
	                }
	            });
	        }
      	});
  	}

  	ionViewDidLoad() {
		console.log('ionViewDidLoad FaqPage');
  	}
}

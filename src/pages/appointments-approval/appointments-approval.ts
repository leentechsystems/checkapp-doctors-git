import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, ViewController} from 'ionic-angular';

import { ProfileHospitalsPage } from './../profile-hospitals/profile-hospitals';
import { AcceptApprovalPage } from './../accept-approval/accept-approval';
import { HospitalsProvider } from '../../providers/hospitals/hospitals';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { SiteProvider } from '../../providers/site/site';

/**
 * Generated class for the AppointmentsApprovalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-appointments-approval',
	templateUrl: 'appointments-approval.html',
})
export class AppointmentsApprovalPage {

	approval_appointment = this.navParams.get('temp_appointment');
	hospital_id = this.navParams.get('temp_hospital_id');
	approval_date = this.navParams.get('temp_date');
	from_notif = (this.navParams.get('from_notif')) ? this.navParams.get('from_notif') : null;
	approval_appointments: any;
	hospital: any;

	total_approval_appointment = 0;
	temp_total_count = 0;
	temp_idx = 0;
	current_page = 1;
	limit = 20;

	reActiveInfinite: any;


	constructor(public navCtrl: NavController,
				public navParams: NavParams,
				public modalCtrl : ModalController,
				public viewCtrl: ViewController,
				public loadingCtrl: LoadingController,
				public hospitalsProvider: HospitalsProvider,
				public appointmentProvider: AppointmentProvider,
				public siteProvider: SiteProvider) {

		this.hospitalProfile();
		console.log("approval_appointment", this.approval_appointment);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad AppointmentsApprovalPage');
	}

	ionViewWillEnter() {
		this.reloadApprovalAppointment(false, true, false);
	}

	reloadData(refresher?){
		this.approval_appointments = null;
		this.total_approval_appointment = 0;
		this.temp_total_count = 0;
		this.temp_idx = 0;
		this.current_page = 1;
		this.reloadApprovalAppointment(refresher, true, false);
		if(this.approval_appointment.length > 20){
			this.reActiveInfinite.enable(true);
		}
	}

	hospitalProfile(){
		this.hospitalsProvider.getSpecificHospital(this.hospital_id).then((data) => {
			if(data['error'] == 0){
				this.hospital = data['hospital'][0];
				console.log(this.hospital);
			}
		}, err => {
			console.log(err);
		});
	}

	gotoHospitalProfile(){

		if(this.hospital['hospital_attachments_filename'] || this.hospital['hospital_attachments_filepath']){
			this.hospital['attachments'] = {
				"filename": this.hospital['hospital_attachments_filename'],
				"filepath": this.hospital['hospital_attachments_filepath'],
			};
		}else{
			this.hospital['attachments'] = null;
		}
		console.log("this.approval_appointment", this.hospital);

		this.navCtrl.push(ProfileHospitalsPage, {
			"hospital_profile": this.hospital
		});
	}

	gotoModal(patient_schedule_index, temp_patient_schedule){

		let doneModal = this.modalCtrl.create(AcceptApprovalPage, {
			temp_patient_schedule: temp_patient_schedule,
			temp_date: this.approval_date,
			origin_page: "appointments",
			from_notif: this.from_notif,
		});

		doneModal.onDidDismiss(data => {
			if(data){
				console.log('data',data);
				temp_patient_schedule['approved_at'] = data.appointment_approved_at;
				temp_patient_schedule['approved_by'] = data.appointment_approved_by;
				if(this.from_notif){
					temp_patient_schedule['appointment_status'] = data.appointment_status;
				}else{
					temp_patient_schedule['status'] = data.appointment_status;
				}
				this.approval_appointments.splice(patient_schedule_index, 1, temp_patient_schedule);

					//this.reloadApprovalAppointment(false, true, false);
			}else{
					//this.reloadApprovalAppointment(false, true, false);
			}
		});

		doneModal.present();
	}

	/*reloadApprovalAppointment(refresher?, isLoadNew?, infiniteScroll?){

		if(!infiniteScroll && infiniteScroll === false) {
			if(!this.approval_appointments || isLoadNew === true) {
				this.approval_appointments = [];
			}
		}

		if(this.from_notif){
			this.approval_appointment['appointment_status'] =  this.approval_appointment['appointment_status'];
			this.approval_appointment['hmo_transactions_status'] = this.approval_appointment.status; //hmo transaction status
			this.approval_appointments.push(this.approval_appointment);
			this.total_approval_appointment += 1;

			console.log('this.approval_appointments',this.approval_appointments);


		}else{
			this.temp_total_count = 0;
			this.approval_appointment.forEach((cat, idx) => {
					if(cat.id !== undefined) {
						console.log(this.limit,'>',this.temp_total_count);
						if(this.limit > this.temp_total_count){
							console.log(this.temp_idx,'<',(idx+1));
							if(this.temp_idx < (idx+1)){
								this.total_approval_appointment += 1;
								this.temp_total_count +=1;
								this.approval_appointments.push(cat);
							}
						}
					}
			});

			this.temp_idx += this.temp_total_count;
		}

		if(refresher) {
			refresher.complete();
		}

		if(infiniteScroll) {
			infiniteScroll.complete();
		}
	}*/

	/*doInfinite(infiniteScroll:any) {
		this.reActiveInfinite = infiniteScroll;
		this.current_page += 1;
		console.log('this.temp_idx',this.temp_idx);
		console.log(this.approval_appointment.length,'>',this.total_approval_appointment);
		if(this.approval_appointment.length > this.total_approval_appointment) {
			this.reloadApprovalAppointment(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}*/

	goBack(){
		this.viewCtrl.dismiss(this.approval_appointments);
	}

	reloadApprovalAppointment(refresher?, isLoadNew?, infiniteScroll?){

		if(this.from_notif){

			if(!infiniteScroll && infiniteScroll === false) {
				if(!this.approval_appointments || isLoadNew === true) {
					this.approval_appointments = [];
				}
			}

			this.approval_appointment['appointment_status'] =  this.approval_appointment['appointment_status'];
			this.approval_appointment['hmo_transactions_status'] = this.approval_appointment.status; //hmo transaction status
			this.approval_appointments.push(this.approval_appointment);
			this.total_approval_appointment += 1;

			console.log('this.approval_appointments',this.approval_appointments);


		}else{

			console.log(this.approval_date);
			let thisData = {
				"current_page": this.current_page,
				"day": 'today',
				"date": this.approval_date,
				"category": 'approval'
			};

			this.appointmentProvider.getDoctorsAppointments(true, thisData).then((data) => {
				this.total_approval_appointment = data['appointments']['total_count'];

				if(!infiniteScroll && infiniteScroll === false) {
					if(!this.approval_appointments || isLoadNew === true) {
						 this.approval_appointments = [];
					}
				}

				data['appointments']['items'].forEach((cat, idx) => {
					if(cat.id !== undefined) {
						let wordDate = cat['appointment_date'].split(" ");
						let wordCount = wordDate[1].split(":");

						if(wordCount[0] < 12){
							cat['appointment_time'] = wordCount[0]+':'+wordCount[1]+' AM';

						}else{
							if(wordCount[0] == 12){
								cat['appointment_time'] = '12'+':'+wordCount[1]+' PM';
							}else{
								cat['appointment_time'] = (wordCount[0]-12) +':'+wordCount[1]+' PM';
							}
						}

						this.approval_appointments.push(cat);
					}
				});

				if(refresher) {
					refresher.complete();
				}

				if(infiniteScroll) {
					infiniteScroll.complete();
				}
			});
		}
	}

	doInfinite(infiniteScroll:any) {
		this.current_page += 1;
		if(this.approval_appointments.length < this.total_approval_appointment) {
			this.reloadApprovalAppointment(false, false, infiniteScroll);
		} else {
			infiniteScroll.enable(false);
		}
	}
}

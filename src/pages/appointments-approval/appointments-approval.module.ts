import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppointmentsApprovalPage } from './appointments-approval';
import { PipesModule } from './../../pipes/pipes.module';
import { IonicImageLoader } from 'ionic-image-loader';

@NgModule({
  declarations: [
    AppointmentsApprovalPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(AppointmentsApprovalPage),
    IonicImageLoader,
  ],
})
export class AppointmentsApprovalPageModule {}

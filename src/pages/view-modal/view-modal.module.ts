import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewModalPage } from './view-modal';
import { PipesModule } from './../../pipes/pipes.module';

@NgModule({
  declarations: [
    ViewModalPage,
  ],
  imports: [
  PipesModule,
    IonicPageModule.forChild(ViewModalPage),
  ],
})
export class ViewModalPageModule {}

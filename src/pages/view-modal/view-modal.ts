import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';

import { PatientScheduledProfilePage } from './../patient-scheduled-profile/patient-scheduled-profile';

import { UserProvider } from '../../providers/user/user';
import { AppointmentProvider } from '../../providers/appointment/appointment';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SiteProvider } from '../../providers/site/site';
/**
 * Generated class for the ViewModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-view-modal',
	templateUrl: 'view-modal.html',
})
export class ViewModalPage {

	patient_schedule = this.navParams.get('temp_patient_schedule');
	patient_profile: any;

	constructor(public navCtrl: NavController, public navParams: NavParams,
				public userProvider: UserProvider,
				public siteProvider: SiteProvider,
				public appointmentProvider: AppointmentProvider,
				public viewCtrl: ViewController,
				public loadingCtrl: LoadingController,
				public photoViewer: PhotoViewer) {

		this.getPatientProfile();
		if(this.patient_schedule){
			let wordDate = new Date(this.patient_schedule['appointment_date']).toString();
			console.log(wordDate);
			let wordCount = wordDate.split(" ");
			console.log(wordCount[0]);
			if(wordCount[0] == 'Sun'){
				this.patient_schedule['appointment_week'] = 'Sunday';
			}
			else if(wordCount[0] == 'Mon'){
				this.patient_schedule['appointment_week'] = 'Monday';
			}
			else if(wordCount[0] == 'Tue'){
				this.patient_schedule['appointment_week'] = 'Thuesday';
			}
			else if(wordCount[0] == 'Wed'){
				this.patient_schedule['appointment_week'] = 'Wednesday';
			}
			else if(wordCount[0] == 'Thu'){
				this.patient_schedule['appointment_week'] = 'Thursday';
			}
			else if(wordCount[0] == 'Fri'){
				this.patient_schedule['appointment_week'] = 'Friday';
			}
			else if(wordCount[0] == 'Sat'){
				this.patient_schedule['appointment_week'] = 'Saturday';
			}
		}
		console.log(this.patient_schedule);
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad ViewModalPage');
	}

	getPatientProfile(){
		this.userProvider.getPatientProfile(this.patient_schedule['patient_id']).then((data) => {
			this.patient_profile = data['user'];
			console.log(this.patient_profile);
		}, err => {
			console.log(err);
		});
	}

	ViewProfile(){
		this.navCtrl.push(PatientScheduledProfilePage, {
			"temp_patient_profile": this.patient_profile,
			"transaction_num": this.patient_schedule['id'],
			"patient_schedule": this.patient_schedule,
		});
	}

	actionAppointment(thisAction){
		let thisData = {
			"id": this.patient_schedule['id'],
			"action": thisAction
		};

		let loader = this.loadingCtrl.create({content: "Processing..."});
		loader.present();

		this.appointmentProvider.approveAppointment(thisData).then((data) => {
			console.log(data);

			this.viewCtrl.dismiss(data['results']);
			loader.dismiss();

			}, err => {
			console.log(err);
		});
	}

	escapeModal() {
		this.viewCtrl.dismiss();
	}

	stopPropagation(e) {
	  e.stopPropagation();
	}

	viewPhoto(attachment){
		console.log(attachment);
		this.photoViewer.show(attachment);
	}
}
